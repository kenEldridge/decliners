from os import listdir
from os.path import isfile, join
import numpy as np

path = 'data/decline/'

files = [f for f in listdir(path) if isfile(join(path, f))]
ind = [x for x in files if x.find('decline') == -1 
       and x.find('student') == -1 
       and x.find('assigned_group') == -1]
ind = [x for x in ind if x.find('csv') == -1]
ind = [x.replace('.txt', '') for x in ind]
ind = [x.replace('_train', '') for x in ind]
ind = [x.replace('_validation', '') for x in ind]
ind.sort()
ind = np.unique(np.array(ind)).tolist()
# dep = [x for x in 
#        [z for z in files if z.find('decline') != -1]
#         if x.find('csv') == -1]

for x in ind:
    print '"' + x + '", '
    
for x in ind:
    print 'm.' + x + ', '
    
# print dep

base_pred = ['m.add predicate: "', '",        types: [ArgumentType.UniqueID]']
pred = [base_pred[0] + x + base_pred[1] for x in ind]
pred = '\n'.join(pred)
print pred

base_logic = ['m.add rule : ( student(A) & ', '(A) ) >> decline(A) , weight : 10', '(A) ) >> not_decline(A) , weight : 10']
# logic = [base_logic[0] + x + base_logic[1] + '\n' + base_logic[0] + x + base_logic[2] for x in ind]
logic = [base_logic[0] + x + base_logic[1] for x in ind]
logic = '\n'.join(logic)
print logic
