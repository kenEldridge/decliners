import pandas as pd
import numpy as np

df = pd.read_csv('data/decline/behavior_large_train.csv')
 
for c in df:
    print c
    if c != 'person_uid':
        dfc = df[c]
        df[c + '_gtm'] = df[c].apply(lambda x: x >= df[c].median())
        d = df[df[c + '_gtm'] == True]
        (d.person_uid).to_csv('data/decline/' + c + '_train.txt', sep='\t', index=False, header=False)
     
df = pd.read_csv('data/decline/behavior_large_validation.csv')
 
df = pd.read_csv('data/decline/prior_purdue_gpa_train.csv')
df.prior_purdue_gpa = df.prior_purdue_gpa.apply(lambda x: x >= df.prior_purdue_gpa.median())
d = df[df.prior_purdue_gpa == True]
(d.person_uid).to_csv('data/decline/prior_purdue_gpa_train.txt', sep='\t', index=False, header=False)


for c in df:
    print c
    if c != 'person_uid':
        df[c + '_gtm'] = df[c].apply(lambda x: x >= df[c].median())
        d = df[df[c + '_gtm'] == True]
        (d.person_uid).to_csv('data/decline/' + c + '_validation.txt', sep='\t', index=False, header=False)

df = pd.read_csv('data/decline/prior_purdue_gpa_validation.csv')
df.prior_purdue_gpa = df.prior_purdue_gpa.apply(lambda x: x >= df.prior_purdue_gpa.median())
d = df[df.prior_purdue_gpa == True]
(d.person_uid).to_csv('data/decline/prior_purdue_gpa_validation.txt', sep='\t', index=False, header=False)


print 'done.'