﻿-- HAWQ
DROP TABLE IF EXISTS ke.colocation_not_class_res_academic_period;
CREATE TABLE ke.colocation_not_class_res_academic_period
AS
(
	SELECT
		ac.academic_period
		, c.person_uid
		, c.match_person_uid
		, sum(min) as min_count
	FROM
		networklog.colocation c	
	INNER JOIN
		oir.academic_calendar ac
	ON
		(c.week >= ac.start_date
		AND c.week <= ac.end_date)
	INNER JOIN	
		sis.student_profile_pu p
	ON
		(c.person_uid = p.person_uid)
	WHERE
		ac.academic_period = '201610'
		AND p.profile_academic_period = '201610'
		AND principle_use_mode != 'Res'
		AND in_class = 0
	GROUP BY 1,2,3
) DISTRIBUTED BY (person_uid)
;


SELECT 
-- 	count(*)
	person_uid,
	match_person_uid
FROM
	ke.colocation_not_class_res_academic_period
WHERE
	min_count > 200
;



--GPDB
drop table if exists board_plan_buddies;
create temp table board_plan_buddies
as
(
    select
        profit_center_name,
        dt,
        pair[1] as student_x,
        pair[2] as student_y
    from
    (
        select
            profit_center_name,
            dt,
            features_card_service.extract_buddies(customer_seq_arr, transact_epoch_arr, 20) as pair
        from
        (
            select
                profit_center_name,
                board_tran_actual_datetime::date as dt,     
                array_agg(customer_number order by board_tran_actual_datetime) as customer_seq_arr,
                array_agg(extract(epoch from board_tran_actual_datetime) order by board_tran_actual_datetime) as transact_epoch_arr
            from
                card.transact_board_plan_transactions t1,
                sis.banner_lookup t2
            where 
                t1.customer_number = t2.puid 
            group by 1, 2
        )q1
    )q2
) distributed randomly;


DROP TABLE IF EXISTS ke.colocation_not_class_res_academic_period;
CREATE TABLE ke.colocation_not_class_res_academic_period
AS
(
	WITH coswipe AS
	(
		select
			ac.academic_period,
			p.person_uid, 
			pm.person_uid as match_person_uid,
			count(*) as pair_cooccurrence_freq
		from 
			board_plan_buddies b
		INNER JOIN
			oir.academic_calendar ac
		ON
			(b.dt >= ac.start_date
			AND b.dt <= ac.end_date)
		INNER JOIN	
			sis.student_profile_pu p
		ON
			(b.student_x = p.id)
		INNER JOIN
			sis.student_profile_pu pm
		ON
			(b.student_y = pm.id)
		WHERE
			ac.academic_period = '201610'
			AND p.profile_academic_period = '201610'
		group by 1,2,3
	)
	SELECT
		*
	FROM
		coswipe
	WHERE 
		pair_cooccurrence_freq > 10
)DISTRIBUTED BY (person_uid)
;


SELECT 
	person_uid,
	match_person_uid
FROM
	ke.colocation_not_class_res_academic_period
;	



