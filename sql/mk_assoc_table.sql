﻿DROP TABLE IF EXISTS ke.associated;
CREATE TABLE ke.associated
(
  academic_period text,
  person_uid numeric,
  match_person_uid numeric,
  strength numeric
)
WITH (
  OIDS=FALSE
)
DISTRIBUTED BY (person_uid);
ALTER TABLE ke.associated
  OWNER TO gpadmin;
