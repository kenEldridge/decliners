﻿select ke.decline_input_table_build(array['netlog','card','lms', 'sis'], 'ke', 90);
--select ke.decline_input_table_build(array['netlog'], 'ke', 90);

-----------------------------------------------------------------------------------------------------
-- XGBoost grid search pipeline decline
-----------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--                                  DECLINE MODEL
-------------------------------------------------------------------------------------

select
    ke.xgboost_grid_search(
        'ke',--training table_schema
        'decline_mod_input_table_netlog_card_lms_sis_06_sep_2016',--training table_name
        'person_uid', -- id column
        'gt_std_decline', -- class label column
        -- Columns to exclude from features (independent variables)
        ARRAY[
		'person_uid',
		'id',
		'academic_period',
		'dropped',
		'gt_median_decline',
		'gt_std_decline',
		'gt_2std_decline',
		'row_number'
		--'colo_positivity',
		--, 'associates_positivity'
        ],
        --XGBoost grid search parameters
        $$
        {
            'learning_rate': [0.001, 0.01, 0.1], #Regularization on weights (eta). For smaller values, increase n_estimators
            'max_depth': [1,2,4,8],#Larger values could lead to overfitting
            'subsample': [0.8],#introduce randomness in samples picked to prevent overfitting
            'colsample_bytree': [0.65],#introduce randomness in features picked to prevent overfitting
            'min_child_weight': [0.5,1,2,4],#larger values will prevent over-fitting
            'n_estimators':[200,600,1000] #More estimators, lesser variance (better fit on test set)
        }
        $$,
        --Grid search parameters temp table (will be dropped when session ends)
        'xgb_params_temp_tbl',
        --Grid search results table.
        'ke.decline_grid_search_mdl_results_90_perc_train',
        --class weights (set it to empty string '' if you want it to be automatic)
        --$$
        --    {
        --        '0':0.2,
        --        '1':0.8                                                            #IF CHANGE WEIGHTS HERE, CHANGE RESULTING TABLE NAMES ('_80wt') ACCORDINGLY
        --    }
        --$$,
        '',
        0.9
    );


-- select metrics, features, params, f_importances, neg_metrics, pos_metrics from ke.decline_grid_search_mdl_results_90_perc_train order by pos_metrics[4] desc;

ALTER TABLE ke.decline_grid_search_mdl_results_90_perc_train
  OWNER TO kjeldrid;
GRANT ALL ON TABLE ke.decline_grid_search_mdl_results_90_perc_train TO kjeldrid;