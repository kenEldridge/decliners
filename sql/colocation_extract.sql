﻿DROP TABLE IF EXISTS ke.colo_business_hours_w_quarter_201610cohort_temp;
CREATE TABLE ke.colo_business_hours_w_quarter_201610cohort_temp AS (				
-- EXPLAIN
SELECT 
	c.week
	, case when c.day_of_week = 0 or c.day_of_week = 6 then
		0
	when c.quarter_of_day = 1 or c.quarter_of_day = 2 then
		1
	else
		0
	end as business_time
	, c.person_uid
	, c.match_person_uid
	, c.pair
	, sum(c.min) as min
	, count(*) as contacts
  FROM 
	networklog.colocation c 
  LEFT JOIN 
	sis.student_profile_pu p 
  ON 
	(p.person_uid = c.person_uid) 
  WHERE p.profile_academic_period = '201610'
		AND p.profile_level = 'UG'
		AND p.profile_reporting_campus = 'PWL'
		AND profile_firstime_fulltime_ind = 'Y'
	AND c.in_class = 0
	AND min > 5
  GROUP BY 1,2,3,4,5
) DISTRIBUTED BY (person_uid)
;

DROP TABLE IF EXISTS ke.colo_business_hours_w_quarter_201610cohort;
CREATE TABLE ke.colo_business_hours_w_quarter_201610cohort AS (				
-- EXPLAIN
select
	c.person_uid
	, c.match_person_uid
	, c.pair
	, c.business_time
	, wt.quarter_of_term
	, wt.academic_period
	, sum(c.min) as min
	, sum(c.contacts) as contacts
from
	ke.colo_business_hours_w_quarter_201610cohort_temp c
left join
	oir.week_of_term wt
on
	(wt.week = c.week)
where wt.academic_period in ('201610', '201620')
group by 1,2,3,4,5,6
order by person_uid
) DISTRIBUTED BY (person_uid)
;


SELECT 
	count(*) 
	--*
FROM ke.colo_business_hours_w_quarter_201610cohort 
where 
contacts > 5
limit 10000

\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 0 and academic_period = '201610' and business_time = 0 order by 1,2) to '/data/exports/colo_201610_bh_q0.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 1 and academic_period = '201610' and business_time = 0 order by 1,2) to '/data/exports/colo_201610_bh_q1.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 2 and academic_period = '201610' and business_time = 0 order by 1,2) to '/data/exports/colo_201610_bh_q2.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 3 and academic_period = '201610' and business_time = 0 order by 1,2) to '/data/exports/colo_201610_bh_q3.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 0 and academic_period = '201620' and business_time = 0 order by 1,2) to '/data/exports/colo_201620_bh_q0.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 1 and academic_period = '201620' and business_time = 0 order by 1,2) to '/data/exports/colo_201620_bh_q1.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 2 and academic_period = '201620' and business_time = 0 order by 1,2) to '/data/exports/colo_201620_bh_q2.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 3 and academic_period = '201620' and business_time = 0 order by 1,2) to '/data/exports/colo_201620_bh_q3.csv' DELIMITER E'\t' CSV HEADER

\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 0 and academic_period = '201610' and business_time = 1 order by 1,2) to '/data/exports/colo_201610_nbh_q0.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 1 and academic_period = '201610' and business_time = 1 order by 1,2) to '/data/exports/colo_201610_nbh_q1.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 2 and academic_period = '201610' and business_time = 1 order by 1,2) to '/data/exports/colo_201610_nbh_q2.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 3 and academic_period = '201610' and business_time = 1 order by 1,2) to '/data/exports/colo_201610_nbh_q3.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 0 and academic_period = '201620' and business_time = 1 order by 1,2) to '/data/exports/colo_201620_nbh_q0.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 1 and academic_period = '201620' and business_time = 1 order by 1,2) to '/data/exports/colo_201620_nbh_q1.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 2 and academic_period = '201620' and business_time = 1 order by 1,2) to '/data/exports/colo_201620_nbh_q2.csv' DELIMITER E'\t' CSV HEADER
\copy (select person_uid , match_person_uid from ke.colo_business_hours_w_quarter_201610cohort where  contacts > 2 and quarter_of_term = 3 and academic_period = '201620' and business_time = 1 order by 1,2) to '/data/exports/colo_201620_nbh_q3.csv' DELIMITER E'\t' CSV HEADER



select 
	count( person_uid)
-- 	, match_person_uid 
from 
	ke.colo_business_hours_w_quarter_201610cohort 
where  
	contacts > 5 
	and quarter_of_term = 0 
	and academic_period = '201610' 
	and business_time = 0 
order by 1,2
;

select * from oir.var_range_histogram( 'ke.colo_business_hours_w_quarter_201610cohort' , 'contacts', 40)

select 
	academic_period
	, quarter_of_term
	, count(*) 
from 
	ke.colo_business_hours_w_quarter_201610cohort
where 
	contacts > 5
	and quarter_of_term is not null
group by 1,2
order by 1,2
;