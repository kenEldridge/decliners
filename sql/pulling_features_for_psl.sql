﻿-- ke.student_features build in export_for_neville.sql
select count(*) from ke.student_features;
select * from ke.student_features limit 100;

-- Persons in training that have
DROP TABLE IF EXISTS stu_m;
CREATE TEMP TABLE stu_m AS
SELECT
	min(prior_term_gpa) as prior_term_gpa_min,
	max(prior_term_gpa) as prior_term_gpa_max,
	median(prior_term_gpa) as prior_term_gpa_median,
	min(aleks) as aleks_min,
	max(aleks) as aleks_max,
	median(aleks) as aleks_median
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
;

-- Prior term GPA train
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND prior_term_gpa >= (SELECT prior_term_gpa_median FROM stu_m)
;

-- Aleks train
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND aleks >= (SELECT aleks_median FROM stu_m)
;

-- Prior term GPA validation
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND prior_term_gpa >= (SELECT prior_term_gpa_median FROM stu_m)
;

-- Aleks validation
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND aleks >= (SELECT aleks_median FROM stu_m)
;

DROP TABLE IF EXISTS hs_core_gpa_stat;
CREATE TEMP TABLE hs_core_gpa_stat AS
SELECT
	median(hs_core_gpa) as hs_core_gpa_median
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016
;

-- High School Core GPA training
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND hs_core_gpa >= (SELECT hs_core_gpa_median FROM hs_core_gpa_stat)
;

-- High School Core GPA validation
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND hs_core_gpa >= (SELECT hs_core_gpa_median FROM hs_core_gpa_stat)
;

DROP TABLE IF EXISTS perc_attend_stat;
CREATE TEMP TABLE perc_attend_stat AS
SELECT
	median(perc_attendance_all_avg) as perc_attendance_all_avg_median
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016
;
-- select perc_attendance_all_avg from ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 limit 100;
-- perc_attendance_all_avg training
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND perc_attendance_all_avg >= (SELECT perc_attendance_all_avg_median FROM perc_attend_stat)
;

-- perc_attendance_all_avg training
SELECT
	*
-- 	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s limit 199
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND perc_attendance_all_avg >= (SELECT perc_attendance_all_avg_median FROM perc_attend_stat)
;


-- let's do some features in batch
SELECT
	s.person_uid
	, avg_arrival_prompt_rec_avg
	, perc_attendance_all_avg
	, avg_arrival_prompt_all_avg
	, perc_attendance_rec_avg
	, door_access_prob_hr_of_day_19
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
-- limit 199
;


-- let's do a bunch of features in batch
SELECT
	s.person_uid
	, avg_arrival_prompt_rec_avg 
	,perc_attendance_all_avg 
	,avg_arrival_prompt_all_avg 
	,perc_attendance_rec_avg 
	,door_access_prob_hr_of_day_19 
	,brd_pln_dining_usage_prob_hr_of_day_16 
	,perc_attendance_lab_avg 
	,perc_attendance_lec_avg 
	,avg_arrival_prompt_sd_avg 
	,avg_arrival_prompt_lec_avg 
	,door_access_prob_hr_of_day_20 
	,door_access_prob_hr_of_day_12 
	,door_access_prob_hr_of_day_14 
	,brd_pln_dining_usage_prob_hr_of_day_12 
	,brd_pln_dining_usage_prob_hr_of_day_11 
	,brd_pln_dining_usage_prob_hr_of_day_20 
	,perc_attendance_pso_avg 
	,door_access_prob_hr_of_day_13 
	,brd_pln_dining_usage_prob_hr_of_day_13 
	,brd_pln_dining_usage_prob_hr_of_day_17 
	,perc_attendance_sd_avg 
	,strd_val_tran_laundry_usage_prob_hr_of_day_21 
	,strd_val_tran_laundry_usage_prob_hr_of_day_16 
	,door_access_prob_hr_of_day_0 
	,door_access_prob_hr_of_day_11 
	,brd_pln_dining_usage_prob_hr_of_day_19 
	,avg_arrival_prompt_lab_avg 
	,door_access_prob_hr_of_day_8 
	,avg_arrival_prompt_pso_avg 
	,door_access_prob_hr_of_day_4 
	,door_access_prob_hr_of_day_23 
	,door_access_prob_hr_of_day_1 
	,avg_weekly_rectrac_swipes 
	,associates_positivity 
	,strd_val_tran_laundry_usage_prob_hr_of_day_15 
	,strd_val_tran_laundry_usage_prob_hr_of_day_23 
	,strd_val_tran_laundry_usage_prob_hr_of_day_18 
	,colo_positivity 
	,door_access_prob_hr_of_day_10 
	,door_access_prob_hr_of_day_16 
	,strd_val_tran_laundry_usage_prob_hr_of_day_9 
	,door_access_prob_hr_of_day_3 
	,brd_pln_dining_usage_prob_hr_of_day_14 
	,brd_pln_dining_usage_prob_hr_of_day_15 
	,brd_pln_dining_usage_prob_hr_of_day_18 
	,rectrac_login_dow_thu 
	,rectrac_login_dow_tue 
	,rectrac_login_dow_wed 
	,door_access_prob_hr_of_day_9 
	,door_access_prob_hr_of_day_5 
	,door_access_prob_hr_of_day_21 
	,door_access_prob_hr_of_day_2 
	,door_access_prob_hr_of_day_7 
	,strd_val_tran_laundry_usage_prob_hr_of_day_19 
	,brd_pln_dining_usage_prob_hr_of_day_9 
	,brd_pln_dining_usage_prob_hr_of_day_8 
	,door_access_prob_hr_of_day_17 
	,strd_val_tran_laundry_usage_prob_hr_of_day_10 
	,brd_pln_dining_usage_prob_hr_of_day_10 
	,avg_arrival_prompt_lbp_avg 
	,strd_val_tran_dining_usage_prob_hr_of_day_11 
	,strd_val_tran_dining_usage_prob_hr_of_day_10 
	,strd_val_tran_laundry_usage_prob_hr_of_day_22 
	,strd_val_tran_laundry_usage_prob_hr_of_day_14 
	,strd_val_tran_laundry_usage_prob_hr_of_day_13 
	,brd_pln_dining_usage_prob_hr_of_day_7 
	,door_access_prob_hr_of_day_15 
	,door_access_prob_hr_of_day_18 
	,strd_val_tran_dining_usage_prob_hr_of_day_9 
	,strd_val_tran_laundry_usage_prob_hr_of_day_11 
	,strd_val_tran_dining_usage_prob_hr_of_day_18 
	,strd_val_tran_dining_usage_prob_hr_of_day_19 
	,rectrac_login_dow_mon 
	,rectrac_login_dow_fri
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_training)
-- limit 199
;


-- Persons in training set
SELECT person_uid FROM ke.gpa_decline_201610_train;

-- Persons in validation set
SELECT person_uid FROM ke.gpa_decline_201610_validation;

SELECT person_uid, case when gt_std_decline = 0 then 1 else 0 end as gt_std_decline FROM ke.gpa_decline_201610_train;
SELECT person_uid, case when gt_std_decline = 0 then 1 else 0 end as gt_std_decline FROM ke.gpa_decline_201610_validation;

SELECT person_uid,gt_std_decline FROM ke.gpa_decline_201610_train;
SELECT person_uid, gt_std_decline FROM ke.gpa_decline_201610_validation;

-- Behavioral Feats by importance score
'avg_arrival_prompt_rec_avg', 
'perc_attendance_all_avg', 
'avg_arrival_prompt_all_avg', 
'perc_attendance_rec_avg', 
'door_access_prob_hr_of_day_19', 
'brd_pln_dining_usage_prob_hr_of_day_16', 
'perc_attendance_lab_avg', 
'perc_attendance_lec_avg', 
'avg_arrival_prompt_sd_avg', 
'avg_arrival_prompt_lec_avg', 
'door_access_prob_hr_of_day_20', 
'door_access_prob_hr_of_day_12', 
'door_access_prob_hr_of_day_14', 
'brd_pln_dining_usage_prob_hr_of_day_12', 
'brd_pln_dining_usage_prob_hr_of_day_11', 
'brd_pln_dining_usage_prob_hr_of_day_20', 
'perc_attendance_pso_avg', 
'door_access_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_17', 
'perc_attendance_sd_avg', 
'strd_val_tran_laundry_usage_prob_hr_of_day_21', 
'strd_val_tran_laundry_usage_prob_hr_of_day_16', 
'door_access_prob_hr_of_day_0', 
'door_access_prob_hr_of_day_11', 
'brd_pln_dining_usage_prob_hr_of_day_19', 
'avg_arrival_prompt_lab_avg', 
'door_access_prob_hr_of_day_8', 
'avg_arrival_prompt_pso_avg', 
'door_access_prob_hr_of_day_4', 
'door_access_prob_hr_of_day_23', 
'door_access_prob_hr_of_day_1', 
'avg_weekly_rectrac_swipes', 
'associates_positivity', 
'strd_val_tran_laundry_usage_prob_hr_of_day_15', 
'strd_val_tran_laundry_usage_prob_hr_of_day_23', 
'strd_val_tran_laundry_usage_prob_hr_of_day_18', 
'colo_positivity', 
'door_access_prob_hr_of_day_10', 
'door_access_prob_hr_of_day_16', 
'strd_val_tran_laundry_usage_prob_hr_of_day_9', 
'door_access_prob_hr_of_day_3', 
'brd_pln_dining_usage_prob_hr_of_day_14', 
'brd_pln_dining_usage_prob_hr_of_day_15', 
'brd_pln_dining_usage_prob_hr_of_day_18', 
'rectrac_login_dow_thu', 
'rectrac_login_dow_tue', 
'rectrac_login_dow_wed', 
'door_access_prob_hr_of_day_9', 
'door_access_prob_hr_of_day_5', 
'door_access_prob_hr_of_day_21', 
'door_access_prob_hr_of_day_2', 
'door_access_prob_hr_of_day_7', 
'strd_val_tran_laundry_usage_prob_hr_of_day_19', 
'brd_pln_dining_usage_prob_hr_of_day_9', 
'brd_pln_dining_usage_prob_hr_of_day_8', 
'door_access_prob_hr_of_day_17', 
'strd_val_tran_laundry_usage_prob_hr_of_day_10', 
'brd_pln_dining_usage_prob_hr_of_day_10', 
'avg_arrival_prompt_lbp_avg', 
'strd_val_tran_dining_usage_prob_hr_of_day_11', 
'strd_val_tran_dining_usage_prob_hr_of_day_10', 
'strd_val_tran_laundry_usage_prob_hr_of_day_22', 
'strd_val_tran_laundry_usage_prob_hr_of_day_14', 
'strd_val_tran_laundry_usage_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_7', 
'door_access_prob_hr_of_day_15', 
'door_access_prob_hr_of_day_18', 
'strd_val_tran_dining_usage_prob_hr_of_day_9', 
'strd_val_tran_laundry_usage_prob_hr_of_day_11', 
'strd_val_tran_dining_usage_prob_hr_of_day_18', 
'strd_val_tran_dining_usage_prob_hr_of_day_19', 
'rectrac_login_dow_mon', 
'rectrac_login_dow_fri'
