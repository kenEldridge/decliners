﻿--------------------------------------------------------------------------------------------------------------------------
--                                  Modeling dataset building for course grade model                                    --
--                                       Xi Zhang <zhan2037@purdue.edu>                                                 --
--                                                 April 2016                                                           --
--                                       Edited by Ken Eldridge <kjeldrid@purdue.edu>                                   --
--                                       Adapded from course grade model                                                --
--                                                 June 2016                                                             --
--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- Parameter setting for add late model input table building pipeline
-- feat_sources: enter any combination of sis, lms, card, netlog
-- output_schema: where to save the output table.
-- daily_table_waited_days: for features fed by daily refreshed raw tables, how many days you want to wait from last feature refreshed date
--------------------------------------------------------------------------------------------------------------------------

create or replace function ke.decline_input_table_build(feat_sources text[], output_schema text, daily_table_waited_days int) returns void as
$BODY$

DECLARE 
last_update_date date;
last_update_term text;
current_date_str text;
current_term text;
daily_refresh_tables text[];
census_refresh_tables text[];
netlog_feat_tables text[];
sis_feat_tables text[];
lms_feat_tables text[];
card_feat_tables text[];
selected_feat_tables text[];
feature_list text[];
join_condition text;

BEGIN
current_date_str = to_char(current_date, 'DD_Mon_YYYY');

select academic_period 
from oir.academic_calendar
where current_date >= start_date and current_date <= end_date
into current_term;

daily_refresh_tables = array[
    --'features_dependent_variables.grade_mod_dep_var',    --not a feature table!!
    'features_card_service.board_plan_rectrac_buddies',
    'features_card_service.board_plan_tran_dining_usage',
    'features_card_service.board_plan_tran_mfu_plan',
    'features_card_service.door_access',
    'features_card_service.rectrac_household',
    'features_card_service.rectrac_logins',
    'features_card_service.weekly_rectrac_swipes',
    'features_card_service.stored_val_tran_dining_usage',
    'features_card_service.stored_val_tran_laundry_usage',
    'features_course_schedule.class_time_schedule_types',
    'features_course_schedule.class_hr_of_day',
    'features_course_schedule.feats_from_reg_audit_table',
    'features_course_schedule.course_size',
    'features_demographics.application_demos',
    'features_demographics.admission_attributes',
    'features_demographics.admission_decision',
    'features_demographics.application_tests',
    'features_demographics.high_school',
    'features_demographics.high_school_inst_gpa',
    'features_demographics.high_school_core_gpa',
    'features_demographics.high_school_driving_time',
    --'features_demographics.high_school_to_regional_rep_dist',
    'features_demographics.acs_2015_hs_zip',
    'features_demographics.decision_count',
    'features_demographics.same_high_school',
    'features_demographics.feats_from_person_detail_table',
    'features_grade.cume_ap_credits_earned',
    'features_grade.student_course_dfw',
    'features_grade.first_term_num_fails',
    'features_grade.in_subject_gpa',
    'features_grade.hs_vs_purdue_gpa_diff',
    'features_grade.repeat_course_ind',
    'features_learning_management.bblearn_student_activity',    --not a feature table!!
    'features_learning_management.lms_num_sessions_dow',
    'features_learning_management.lms_session_duration',
    'features_learning_management.lms_num_courses_per_week'--,
   -- 'features_learning_management.lms_num_msg_posted_per_course',
    --'features_learning_management.lms_msg_participation_ratio_per_course'
    ];

census_refresh_tables = array[
    'features_academic_study.class_boap_enroll_college',
    'features_academic_study.major_college_num_changes',
    'features_demographics.feats_from_profile_table',
    'features_demographics.feats_from_demos_table',
    'features_financial_aid.feats_from_fin_aid_table_person_level',
    'features_financial_aid.feats_from_fin_aid_table_year_level',
    'features_grade.prior_purdue_gpa',
    'features_course_schedule.instructor_difficulty_experience'
    ];

netlog_feat_tables = array[
    'ke.attendance_by_person_course_agg'
    , 'ke.attendance_quartile'
    , 'ke.usage_by_person'
 	];

sis_feat_tables = array[
	--'features_course_schedule.class_time_schedule_types',
    --'features_course_schedule.class_hr_of_day',
    --'features_course_schedule.feats_from_reg_audit_table',
    --'features_course_schedule.course_size',
    'features_demographics.application_demos',
    'features_demographics.admission_attributes',
    'features_demographics.admission_decision',
    'features_demographics.application_tests',
    'features_demographics.high_school',
    'features_demographics.high_school_inst_gpa',
    'features_demographics.high_school_core_gpa',
    'features_demographics.high_school_driving_time',
    --'features_demographics.high_school_to_regional_rep_dist',
    'features_demographics.acs_2015_hs_zip',
    'features_demographics.decision_count',
    --'features_demographics.same_high_school',
    'features_demographics.feats_from_person_detail_table',
    'features_grade.cume_ap_credits_earned',
    --'features_grade.student_course_dfw',
    'features_grade.first_term_num_fails',
    --'features_grade.in_subject_gpa',
    'features_grade.hs_vs_purdue_gpa_diff',
    --'features_grade.repeat_course_ind',
    'features_academic_study.class_boap_enroll_college',
    'features_academic_study.major_college_num_changes',
    'features_demographics.feats_from_profile_table',
    'features_demographics.feats_from_demos_table',
    'features_financial_aid.feats_from_fin_aid_table_person_level',
    'features_financial_aid.feats_from_fin_aid_table_year_level',
    'features_grade.prior_purdue_gpa'--,
    --'features_course_schedule.instructor_difficulty_experience'
	];

card_feat_tables = array[
	--'features_card_service.board_plan_rectrac_buddies',
    'features_card_service.board_plan_tran_dining_usage',
    --'features_card_service.board_plan_tran_mfu_plan',
    'features_card_service.door_access',
    'features_card_service.rectrac_household',
    'features_card_service.rectrac_logins',
    'features_card_service.weekly_rectrac_swipes',
    'features_card_service.stored_val_tran_dining_usage',
    'features_card_service.stored_val_tran_laundry_usage'
	];

lms_feat_tables = array[
	'features_learning_management.lms_num_sessions_dow',
    'features_learning_management.lms_session_duration',
    'features_learning_management.lms_num_courses_per_week'--,
    --'features_learning_management.lms_num_msg_posted_per_course',
    --'features_learning_management.lms_msg_participation_ratio_per_course'
	];

RAISE NOTICE 'About to loop through feat_source, line 153';

--Combine selected feature tables
selected_feat_tables = null::text[];
FOR i in 1..array_upper(feat_sources, 1) LOOP
	if feat_sources[i] = 'sis' then selected_feat_tables = selected_feat_tables || sis_feat_tables;
	elseif feat_sources[i] = 'lms' then selected_feat_tables = selected_feat_tables || lms_feat_tables;
	elseif feat_sources[i] = 'card' then selected_feat_tables = selected_feat_tables || card_feat_tables;
	elseif feat_sources[i] = 'netlog' then selected_feat_tables = selected_feat_tables || netlog_feat_tables;
	end if;
end loop;

RAISE NOTICE 'About to loop through daily_refresh_tables, line 165';

--Loop through the daily refresh tables, pull the last time they were updated, and update if needed
FOR i IN 1..array_upper(daily_refresh_tables, 1) LOOP
    select statime::date 
    from 
    (
        select t1.schemaname, t1.relname, t2.statime, row_number() over (partition by relname order by statime desc) 
        from  pg_stat_all_tables t1
        join pg_stat_last_operation t2 
        on t1.relid = t2.objid and
        t1.schemaname = substring(daily_refresh_tables[i] from '#"%#".%' for '#') and
        t1.relname = substring(daily_refresh_tables[i] from '%.#"%#"' for '#') and
        t2.staactionname = 'CREATE' and 
        t2.stasubtype = 'TABLE'
    )t
    where row_number = 1 
    into last_update_date;

    --can be changed to higher refresh frequency if needed
    --make sure update functions share the name of the table exactly!
    IF current_date - last_update_date > daily_table_waited_days THEN 
        execute $$select $$ || daily_refresh_tables[i] || $$()$$;
    ELSE CONTINUE;
    END IF;
END LOOP;

RAISE NOTICE 'About to loop through census_refresh_tables, line 192';

FOR i IN 1..array_upper(census_refresh_tables, 1) LOOP
    select statime::date 
    from 
    (
        select t1.schemaname, t1.relname, t2.statime, row_number() over (partition by relname order by statime desc) 
        from  pg_stat_all_tables t1
        join pg_stat_last_operation t2 
        on t1.relid = t2.objid and
        t1.schemaname = substring(census_refresh_tables[i] from '#"%#".%' for '#') and
        t1.relname = substring(census_refresh_tables[i] from '%.#"%#"' for '#') and
        t2.staactionname = 'CREATE' and 
        t2.stasubtype = 'TABLE'
    )t
    where row_number = 1 
    into last_update_date;

    select academic_period 
    from oir.academic_calendar
    where last_update_date >= start_date and last_update_date <= end_date
    into last_update_term;

    --if not the same term, refresh
    IF current_term != last_update_term THEN 
        execute $$select $$ || census_refresh_tables[i] || $$()$$;
    ELSE CONTINUE;
    END IF;
END LOOP;

RAISE NOTICE 'About to loop through add_late_mod_temp_table, line 222';

--create a 'base' table
drop table if exists add_late_mod_temp_table cascade;
create temp table add_late_mod_temp_table as
(
	select 
        *
    from 
        ke.gpa_decline_201610
) distributed by (person_uid);

RAISE NOTICE 'About to loop through selected_feat_tables, line 234, (%)', selected_feat_tables;

FOR i in 1..array_upper(selected_feat_tables, 1) LOOP
		
	IF selected_feat_tables[i] not in (
		'features_dependent_variables.grade_mod_dep_var',	
		'features_learning_management.bblearn_student_activity'
	) THEN

		feature_list = null::text[];
		
        --select the names of all feature names into a list, excluding non-feature variables
		select array_agg(column_name::text) as column_name_arr 
		from information_schema.columns
		where 
		    table_schema || '.' || table_name = selected_feat_tables[i] and
		    column_name not in (
		        'person_uid', 
		        'id',
		        'puid',
		        'career_account',
		        'academic_period', 
		        'aid_year',
		        'course_identification',
		        'hs_zip',    --hs_zip in high_school table is not pivoted
                'hs_percentile',    --exclude hs_percentile in high_school table, use profile_high_school_percentile in feats_from_profile_table instead
                'profile_academic_period',   --profile_academic_period in feats_from_profile_table is not pivoted
		        'bytes_transferred',   --non-feature column in netlog table
		        'time_in_class') and    --non-feature column in netlog table
            column_name !~ E'hs\\_nation\\_.*' and  --hs_nation_... columns in high_school table
            column_name !~ E'hs\\_state\\_(?!in|il)[a-z]+' and  --exclude hs_state_... columns in high_school table, except _in and _il states
            column_name !~ E'app\\_race.+' and  --exclude app_race... in application_demos table
            column_name !~ E'app\\_residency\\_.+' and  --exclude app_residency_... columns in application_demos table
            column_name !~ E'profile\\_highest\\_sat\\_.+' and   --exclude profile_highest_sat_... columns in feats_from_profile_table, use test scores in application_tests table instead
            column_name !~ E'person\\_detail\\_.+'   --exclude person_detail_male and person_detail_urm columns in feats_from_person_detail_table
		into feature_list;

        --detect which type of join condition is required for this table based on the available data, and execute the join
        --make sure to add 'custom' ifs to the top!
		join_condition = null::text;

		IF selected_feat_tables[i] = 'features_financial_aid.feats_from_fin_aid_table_year_level' THEN
            RAISE NOTICE 'Condition 1, line 276';
			join_condition = 't1.person_uid = t2.person_uid and substring(t1.academic_period::text from 3 for 2) = substring(t2.aid_year::text from 3 for 2)';
		ELSIF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='person_uid') = TRUE THEN
			RAISE NOTICE 'Condition 2, line 279';
            IF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='academic_period') = TRUE THEN
                IF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='course_identification') = TRUE THEN
                    RAISE NOTICE 'Condition 2a, line 282, (%: %)', i, selected_feat_tables[i];
					join_condition = 't1.person_uid = t2.person_uid and t1.academic_period = t2.academic_period and t1.course_identification = t2.course_identification';
				ELSE 
					join_condition = 't1.person_uid = t2.person_uid and t1.academic_period = t2.academic_period';
				END IF;
			ELSIF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='course_identification') = TRUE THEN
                RAISE NOTICE 'Condition 2b, line 288, (%)', i;
                join_condition = 't1.person_uid = t2.person_uid and t1.course_identification = t2.course_identification';
			ELSE
				join_condition = 't1.person_uid = t2.person_uid';
			END IF;
		ELSIF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='academic_period') = TRUE THEN
			RAISE NOTICE 'Condition 3, line 292';
            IF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='course_identification') = TRUE THEN
                RAISE NOTICE 'Condition 3a, line 296, (%)', i;
				join_condition = 't1.academic_period = t2.academic_period and t1.course_identification = t2.course_identification';
			ELSE 
				join_condition = 't1.academic_period = t2.academic_period';
			END IF;
		ELSIF exists (select 1 from information_schema.columns where table_schema || '.' || table_name = selected_feat_tables[i] AND column_name='course_identification') = TRUE THEN
            RAISE NOTICE 'Condition 4, line 302, (%)', i;
			join_condition = 't1.course_identification = t2.course_identification';
        END IF;

		drop table if exists new_add_late_mod_temp_table cascade;
		EXECUTE $$create temp table new_add_late_mod_temp_table as
		(
			select 
				t1.*,
				$$ || array_to_string(feature_list, $a$, $a$) || $$
			from
				add_late_mod_temp_table t1
			left join
				$$ || selected_feat_tables[i] || $$ t2
			on $$ || join_condition || $$
		) distributed by (person_uid)$$;

		drop table if exists add_late_mod_temp_table cascade;
		alter table new_add_late_mod_temp_table rename to add_late_mod_temp_table;

	END IF;
END LOOP;

RAISE NOTICE 'About to generate output table, line 318';

EXECUTE $$drop table if exists $$ || output_schema || $$.$$ || $$decline_mod_input_table_$$ || array_to_string(feat_sources, $$_$$) || $$_$$ || current_date_str || $$ cascade$$;
EXECUTE $$create table $$ || output_schema || $$.$$ || $$decline_mod_input_table_$$ || array_to_string(feat_sources, $$_$$) || $$_$$ || current_date_str || $$ as
(
	select * from add_late_mod_temp_table
) distributed by (person_uid)$$;

END;
$BODY$
LANGUAGE PLPGSQL;

-- select ke.decline_input_table_build(array['netlog','card','lms', 'sis'], 'ke', 90);
-- select ke.decline_input_table_build(array['netlog'], 'ke', 90);
-- select count(*) from ke.decline_mod_input_table_netlog_card_lms_sis_09_Nov_2016;
-- select * from ke.decline_mod_input_table_netlog_card_lms_sis_09_Nov_2016 limit 1000;
-- This was to create a table from which randomized train/test/val sets can be pulled
-- DROP TABLE IF EXISTS ke.student_profile_random_201610;
-- CREATE TABLE ke.student_profile_random_201610
-- as
-- WITH stu_pro AS
-- (
-- SELECT
--     *
-- FROM
--     sis.student_profile_pu p
-- WHERE
--     p.profile_academic_period = '201610'
--     AND profile_firstime_fulltime_ind = 'Y'
-- ORDER BY
--     random()
-- )
-- SELECT
--     row_number() over(),
--     *
-- FROM
--     stu_pro
-- ;
