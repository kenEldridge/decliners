-- Term GPA (no duplicates with ap, inst type and event filters)
SELECT 
	person_uid, 
	id, 
	gpa_type, 
	gpa_type_desc, 
	academic_study_value, 
	academic_study_value_desc,
	academic_period, 
	academic_period_desc, 
	gpa, 
	gpa_credits, 
	credits_attempted, 
	credits_earned, 
	credits_passed, 
	quality_points, 
	computation_date, 
	freeze_event
FROM 
	sis.frz_gpa_by_term t
WHERE
	t.academic_period = '201510'
	AND t.freeze_event = 'TERM_END'
	-- I = Institution, T - Transfer
	AND t.gpa_type = 'I'
ORDER BY
	1	
;

-- Level GPA (no duplicates with ap, inst type and event filters)
SELECT 
	person_uid, 
	id, 
	gpa_type, 
	gpa_type_desc, 
	academic_study_value, 
	academic_study_value_desc, 
	gpa, 
	gpa_credits, 
	credits_attempted, 
	credits_earned, 
	credits_passed, 
	quality_points, 
	academic_period, 
	academic_period_desc, 
	computation_date, 
	freeze_event
FROM 
	sis.frz_gpa_by_level lvl
WHERE
	lvl.academic_period = '201510'
	AND lvl.freeze_event = 'TERM_END'
	-- I = Institution, T - Transfer, 0 - Overall
	AND lvl.gpa_type = 'I'
ORDER BY
	1
;



SELECT 
	person_uid,
	id, 
	name, 
	profile_academic_year, 
	profile_academic_year_desc, 
 	profile_academic_period, 
	profile_academic_period_desc, 
	profile_level, 
	profile_level_desc, 
	profile_admissions_population, 
	profile_admissions_pop_desc, 
	profile_reporting_campus, 
	profile_reporting_campus_desc, 
	profile_campus_site, 
	profile_campus_site_desc, 
	profile_college, 
	profile_college_desc, 
	profile_major, 
	profile_major_desc, 
	profile_degree, 
	profile_degree_desc, 
	profile_program, 
	profile_program_desc, 
	profile_classification_boap, 
	profile_class_boap_desc, 
	profile_residence, 
	profile_residence_desc, 
	profile_reporting_ethnicity, 
	profile_report_ethnicity_sort, 
	profile_underrep_minority_ind, 
	profile_gender, 
	profile_gender_desc, 
	profile_credit_hours, 
	profile_part_time_full_time, 
	profile_firstime_fulltime_ind, 
	profile_firstime_ind, 
	profile_high_school_percentile, 
	profile_highest_sat_math, 
	profile_highest_sat_writing, 
	profile_highest_sat_crit_read, 
	profile_highest_act_english, 
	profile_highest_act_math, 
	profile_highest_act_reading, 
	profile_highest_act_sci_reason, 
	profile_highest_act_composite, 
	profile_highest_act_englwrit, 
	profile_highest_gre_verbal, 
	profile_highest_gre_quant, 
	profile_highest_gre_analytical
FROM sis.student_profile_pu p
WHERE
	p.profile_academic_period = '201410'
	AND p.profile_level = 'UG'
	AND p.profile_reporting_campus = 'PWL'
	AND profile_firstime_fulltime_ind = 'Y'
ORDER BY 2
;

DROP TABLE IF EXISTS academic_period_seq;
CREATE TEMP TABLE academic_period_seq
AS
(
	SELECT 
		academic_period,
		start_date, 
		end_date,
		lag(academic_period) OVER(ORDER BY academic_period) AS previous_academic_period,
		lead(academic_period) OVER(ORDER BY academic_period) AS next_academic_period,
		lead(academic_period,1) OVER(ORDER BY academic_period) AS next_academic_period_1,
		lead(academic_period,2) OVER(ORDER BY academic_period) AS next_academic_period_2,
		lead(academic_period,3) OVER(ORDER BY academic_period) AS next_academic_period_2
	FROM oir.academic_calendar
)
;

-- GPA: Term and Level
SELECT 
	t.person_uid,
	t.id, 
	p.profile_academic_period,
	p.profile_level,
	
	array_agg(t.academic_period order by t.academic_period) as academic_period, 
	array_agg(t.gpa order by t.academic_period) as term_gpa,
	array_agg(lvl.gpa order by t.academic_period) as level_gpa

FROM 
	sis.frz_gpa_by_term t
INNER JOIN sis.frz_gpa_by_level lvl
ON
	(
	t.person_uid = lvl.person_uid
	AND
	t.academic_period = lvl.academic_period	
	AND t.freeze_event = lvl.freeze_event
	AND 
	t.gpa_type = lvl.gpa_type
	)
INNER JOIN sis.student_profile_pu p
ON
	(
	p.person_uid = t.person_uid
	)
WHERE
	t.freeze_event = 'TERM_END'
	AND t.gpa_type = 'I'
	AND p.profile_academic_period = '201410'
	AND p.profile_level = 'UG'
	AND p.profile_reporting_campus = 'PWL'
	AND profile_firstime_fulltime_ind = 'Y'
	AND t.person_uid = 912050
GROUP BY
	1,2,3,4
ORDER BY
	1,2,3,4
;

SELECT
	*,
	array_upper(term_gpa_lst,1) as term_count,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 0 THEN
		term_gpa_lst[1]
	END as term_gpa_1,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 1 THEN
		term_gpa_lst[2]
	END as term_gpa_2,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 2 THEN
		term_gpa_lst[3]
	END as term_gpa_3,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 3 THEN
		term_gpa_lst[4]
	END as term_gpa_4,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 4 THEN
		term_gpa_lst[5]
	END as term_gpa_5,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 5 THEN
		term_gpa_lst[6]
	END as term_gpa_6,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 6 THEN
		term_gpa_lst[7]
	END as term_gpa_7,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 7 THEN
		term_gpa_lst[8]
	END as term_gpa_8,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 8 THEN
		term_gpa_lst[9]
	END as term_gpa_9,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 9 THEN
		term_gpa_lst[10]
	END as term_gpa_10,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 10 THEN
		term_gpa_lst[11]
	END as term_gpa_11,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 11 THEN
		term_gpa_lst[12]
	END as term_gpa_12,
	CASE
		WHEN array_upper(term_gpa_lst,1) > 12 THEN
		term_gpa_lst[13]
	END as term_gpa_13





	,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 0 THEN
		level_gpa_lst[1]
	END as level_gpa_lst_1,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 1 THEN
		level_gpa_lst[2]
	END as level_gpa_lst_2,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 2 THEN
		level_gpa_lst[3]
	END as level_gpa_lst_3,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 3 THEN
		level_gpa_lst[4]
	END as level_gpa_lst_4,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 4 THEN
		level_gpa_lst[5]
	END as level_gpa_lst_5,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 5 THEN
		level_gpa_lst[6]
	END as level_gpa_lst_6,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 6 THEN
		level_gpa_lst[7]
	END as level_gpa_lst_7,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 7 THEN
		level_gpa_lst[8]
	END as level_gpa_lst_8,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 8 THEN
		level_gpa_lst[9]
	END as level_gpa_lst_9,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 9 THEN
		level_gpa_lst[10]
	END as level_gpa_lst_10,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 10 THEN
		level_gpa_lst[11]
	END as level_gpa_lst_11,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 11 THEN
		level_gpa_lst[12]
	END as level_gpa_lst_12,
	CASE
		WHEN array_upper(level_gpa_lst,1) > 12 THEN
		level_gpa_lst[13]
	END as level_gpa_lst_13
FROM
(
	SELECT 
		t.person_uid,
		t.id, 
		p.profile_academic_period,
		p.profile_level,
		lvl.academic_period,
		t.academic_period,
		t.gpa AS term_gpa,
		lvl.gpa AS level_gpa
		--array_agg(t.academic_period order by t.academic_period) as academic_period_lst, 
		--array_agg(t.gpa order by t.academic_period) as term_gpa_lst,
		--array_agg(lvl.gpa order by t.academic_period) as level_gpa_lst

	FROM 
		sis.frz_gpa_by_level lvl
	INNER JOIN sis.frz_gpa_by_term t
	ON
		t.person_uid = lvl.person_uid
		AND t.academic_period = lvl.academic_period
		AND t.freeze_event = lvl.freeze_event
		AND t.gpa_type = lvl.gpa_type
	INNER JOIN sis.student_profile_pu p
	ON
		(
		p.person_uid = lvl.person_uid
		)
	WHERE
		lvl.freeze_event = 'TERM_END'
		AND lvl.gpa_type = 'I'
		AND p.profile_academic_period = '201410'
		AND p.profile_level = 'UG'
		AND p.profile_reporting_campus = 'PWL'
		AND profile_firstime_fulltime_ind = 'Y'
		AND lvl.person_uid = 912050
	GROUP BY
		1,2,3,4,5
	ORDER BY
		1,2,3,4,5
	)s
;

"{3.756250000,3.594117647,3.617021277,3.627419355}"
select *
from sis.frz_gpa_by_term t
where person_uid = 1240358
and gpa_type = 'I'
and freeze_event = 'TERM_END'
order by academic_period
;





DROP TABLE one_term_out_201010;
CREATE TABLE ke.one_term_out_201010
AS
(
SELECT
	*,
	CASE WHEN term_gpa_2 IS NULL THEN
		TRUE
	END AS DROPPED--,
	--decline + 
FROM
(
	SELECT 
		s2.person_uid,
		s2.id,
		s2.profile_academic_period,
		s2.profile_level,
		max(term_gpa_1) AS term_gpa_1,
		max(term_gpa_2) AS term_gpa_2,
		(max(term_gpa_1) - max(term_gpa_2)) as decline,
		max(level_gpa_1) AS level_gpa_1,
		max(level_gpa_2) AS level_gpa_2
	FROM
	(
		SELECT
			*,
			CASE
				WHEN s.academic_period = '201010' THEN
				s.term_gpa
			END AS term_gpa_1,
			CASE
				WHEN s.academic_period = '201020' THEN
				s.term_gpa
			END AS term_gpa_2,
			CASE
				WHEN s.academic_period = '201010' THEN
				s.level_gpa
			END AS level_gpa_1,
			CASE
				WHEN s.academic_period = '201020' THEN
				s.level_gpa
			END AS level_gpa_2
		FROM
			(
			SELECT 
				t.person_uid,
				t.id, 
				p.profile_academic_period,
				p.profile_level,
				lvl.academic_period,
				t.gpa AS term_gpa,
				lvl.gpa AS level_gpa
			FROM 
				sis.frz_gpa_by_level lvl
			INNER JOIN sis.frz_gpa_by_term t
			ON
				t.person_uid = lvl.person_uid
				AND t.academic_period = lvl.academic_period
				AND t.freeze_event = lvl.freeze_event
				AND t.gpa_type = lvl.gpa_type
			INNER JOIN sis.student_profile_pu p
			ON
				(
				p.person_uid = lvl.person_uid
				)
			WHERE
				lvl.freeze_event = 'TERM_END'
				AND lvl.gpa_type = 'I'
				AND p.profile_academic_period = '201010'
				AND p.profile_level = 'UG'
				AND p.profile_reporting_campus = 'PWL'
				AND profile_firstime_fulltime_ind = 'Y'
			)s
		)s2
	GROUP BY 1,2,3,4
)s3
)DISTRIBUTED BY (PERSON_UID)
;
ALTER TABLE ke.one_term_out_201010
  OWNER TO kjeldrid;
GRANT ALL ON TABLE ke.one_term_out_201010 TO kjeldrid;
GRANT SELECT ON TABLE ke.one_term_out_201010 TO ke_readonly_group;
GRANT UPDATE, INSERT, DELETE ON TABLE ke.one_term_out_201010 TO ke_update_group;
GRANT ALL ON TABLE ke.one_term_out_201010 TO gpadmin;


DROP TABLE one_term_out_201410;
CREATE TABLE ke.one_term_out_201410
AS
(
SELECT
	*,
	CASE WHEN term_gpa_2 IS NULL THEN
		TRUE
	END AS DROPPED--,
	--decline + 
FROM
(
	SELECT 
		s2.person_uid,
		s2.id,
		s2.profile_academic_period,
		s2.profile_level,
		max(term_gpa_1) AS term_gpa_1,
		max(term_gpa_2) AS term_gpa_2,
		(max(term_gpa_1) - max(term_gpa_2)) as decline,
		max(level_gpa_1) AS level_gpa_1,
		max(level_gpa_2) AS level_gpa_2
	FROM
	(
		SELECT
			*,
			CASE
				WHEN s.academic_period = '201410' THEN
				s.term_gpa
			END AS term_gpa_1,
			CASE
				WHEN s.academic_period = '201420' THEN
				s.term_gpa
			END AS term_gpa_2,
			CASE
				WHEN s.academic_period = '201410' THEN
				s.level_gpa
			END AS level_gpa_1,
			CASE
				WHEN s.academic_period = '201420' THEN
				s.level_gpa
			END AS level_gpa_2
		FROM
			(
			SELECT 
				t.person_uid,
				t.id, 
				p.profile_academic_period,
				p.profile_level,
				lvl.academic_period,
				t.gpa AS term_gpa,
				lvl.gpa AS level_gpa
			FROM 
				sis.frz_gpa_by_level lvl
			INNER JOIN sis.frz_gpa_by_term t
			ON
				t.person_uid = lvl.person_uid
				AND t.academic_period = lvl.academic_period
				AND t.freeze_event = lvl.freeze_event
				AND t.gpa_type = lvl.gpa_type
			INNER JOIN sis.student_profile_pu p
			ON
				(
				p.person_uid = lvl.person_uid
				)
			WHERE
				lvl.freeze_event = 'TERM_END'
				AND lvl.gpa_type = 'I'
				AND p.profile_academic_period = '201410'
				AND p.profile_level = 'UG'
				AND p.profile_reporting_campus = 'PWL'
				AND profile_firstime_fulltime_ind = 'Y'
			)s
		)s2
	GROUP BY 1,2,3,4
)s3
)DISTRIBUTED BY (PERSON_UID)
;

DROP TABLE IF EXISTS one_term_out_201410_stats;
CREATE TEMP TABLE one_term_out_201410_stats
AS
	(
	SELECT
		oto.profile_academic_period,
		stddev(decline) AS std,
		avg(decline) AS mean, 
		median(decline) AS median
	FROM
		one_term_out_201410 oto
	GROUP BY oto.profile_academic_period
	)
;

DROP TABLE IF EXISTS ke.gpa_decline_201410;
CREATE TABLE ke.gpa_decline_201410
AS
(
	SELECT
		s.person_uid,
		s.id,
		s.academic_period,
		CASE WHEN s.dropped IS TRUE THEN
			1
		ELSE
			0
		END AS dropped,
		CASE WHEN gt_median_decline IS TRUE THEN
			1
		ELSE
			0
		END AS gt_median_decline,
		CASE WHEN gt_std_decline IS TRUE THEN
			1
		ELSE
			0
		END AS gt_std_decline,
		CASE WHEN gt_2std_decline IS TRUE IS TRUE THEN
			1
		ELSE
			0
		END AS gt_2std_decline
	FROM
		(
		SELECT
			oto.person_uid,
			oto.id,
			oto.profile_academic_period as academic_period,
			oto.dropped IS TRUE AS dropped,
			(decline > median) IS TRUE AS gt_median_decline,
			(decline > median + std) IS TRUE AS gt_std_decline,
			(decline > median + 2*std) IS TRUE AS gt_2std_decline
		FROM
			one_term_out_201410 oto
		INNER JOIN
			one_term_out_201410_stats s
		ON
			(
			oto.profile_academic_period = s.profile_academic_period
			)
		)s
)DISTRIBUTED BY (person_uid)
;




