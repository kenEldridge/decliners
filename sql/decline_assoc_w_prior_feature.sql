﻿DROP TABLE IF EXISTS ke.accociated_sums_by_prior;
CREATE TABLE ke.accociated_sums_by_prior AS
(WITH assoc_prior AS
(SELECT 
	a.academic_period, 
	a.person_uid, 
	a.match_person_uid, 
	CASE WHEN p.prior_purdue_gpa < 3 THEN
		-10
	ELSE
		1
	END as prior,
	a.strength
FROM 
	ke.associated a
LEFT JOIN
	features_grade.prior_purdue_gpa p
ON
	(p.person_uid = a.match_person_uid
	AND p.academic_period = '201620')
)
SELECT
	academic_period,
	person_uid,
	sum(strength * prior) as associates_positivity
FROM
	assoc_prior
GROUP BY 
	1,2
)DISTRIBUTED BY (person_uid)
;


DROP TABLE IF EXISTS ke.colo_sums_by_prior;
CREATE TABLE ke.colo_sums_by_prior AS
(WITH assoc_prior AS
(SELECT 
	a.person_uid, 
	a.match_person_uid, 
	CASE WHEN p.prior_purdue_gpa < 3 THEN
		-5
	ELSE
		1
	END as prior,
	sum(min) as min_count
FROM 
	networklog.colocation a
LEFT JOIN
	features_grade.prior_purdue_gpa p
ON
	(p.person_uid = a.match_person_uid
	AND p.academic_period = '201620')
WHERE
	a.week < '2016-01-01'
GROUP BY 
	1,2,3
)
SELECT
	'201610' as academic_period,
	person_uid,
	sum(min_count * prior) as colo_positivity
FROM
	assoc_prior
GROUP BY 
	1,2
)DISTRIBUTED BY (person_uid)
;

-- select * from ke.accociated_sums_by_prior;