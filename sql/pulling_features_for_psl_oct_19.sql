﻿-- ke.student_features build in export_for_neville.sql
select count(*) from ke.student_features;
select * from ke.student_features limit 100;

-- Persons in training that have
DROP TABLE IF EXISTS stu_m;
CREATE TEMP TABLE stu_m AS
SELECT
	min(prior_term_gpa) as prior_term_gpa_min,
	max(prior_term_gpa) as prior_term_gpa_max,
	median(prior_term_gpa) as prior_term_gpa_median,
	min(aleks) as aleks_min,
	max(aleks) as aleks_max,
	median(aleks) as aleks_median
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
;

-- Prior term GPA train
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND prior_term_gpa >= (SELECT prior_term_gpa_median FROM stu_m)
;

-- Aleks train
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND aleks >= (SELECT aleks_median FROM stu_m)
;

-- Prior term GPA validation
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND prior_term_gpa >= (SELECT prior_term_gpa_median FROM stu_m)
;

-- Aleks validation
SELECT
	s.person_uid
FROM
	ke.student_features s
WHERE
	s.academic_period = '201620'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND aleks >= (SELECT aleks_median FROM stu_m)
;

DROP TABLE IF EXISTS hs_core_gpa_stat;
CREATE TEMP TABLE hs_core_gpa_stat AS
SELECT
	median(hs_core_gpa) as hs_core_gpa_median
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016
;

-- High School Core GPA training
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND hs_core_gpa >= (SELECT hs_core_gpa_median FROM hs_core_gpa_stat)
;

-- High School Core GPA validation
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND hs_core_gpa >= (SELECT hs_core_gpa_median FROM hs_core_gpa_stat)
;

DROP TABLE IF EXISTS perc_attend_stat;
CREATE TEMP TABLE perc_attend_stat AS
SELECT
	median(perc_attendance_all_avg) as perc_attendance_all_avg_median
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016
;
-- select perc_attendance_all_avg from ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 limit 100;
-- perc_attendance_all_avg training
SELECT
	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
	AND perc_attendance_all_avg >= (SELECT perc_attendance_all_avg_median FROM perc_attend_stat)
;

-- perc_attendance_all_avg training
SELECT
	*
-- 	s.person_uid
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s limit 199
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	AND perc_attendance_all_avg >= (SELECT perc_attendance_all_avg_median FROM perc_attend_stat)
;


-- let's do some features in batch
SELECT
	s.person_uid
	, avg_arrival_prompt_rec_avg
	, perc_attendance_all_avg
	, avg_arrival_prompt_all_avg
	, perc_attendance_rec_avg
	, door_access_prob_hr_of_day_19
FROM
	ke.decline_mod_input_table_netlog_card_lms_sis_20_sep_2016 s
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	AND s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
-- limit 199
;


-- let's do a bunch of features in batch
SELECT 
	s.person_uid
	, p.prior_purdue_gpa
	, term_gpa_1
	, dropped
	, gt_median_decline
	, gt_std_decline
	, gt_2std_decline
	, row_number
	, avg_arrival_prompt_all_avg
	, avg_arrival_prompt_lec_avg
	, avg_arrival_prompt_lab_avg
	, avg_arrival_prompt_rec_avg
	, avg_arrival_prompt_sd_avg
	, avg_arrival_prompt_pso_avg
	, avg_arrival_prompt_lbp_avg
	, perc_attendance_all_avg
	, perc_attendance_lec_avg
	, perc_attendance_lab_avg
	, perc_attendance_rec_avg
	, perc_attendance_sd_avg
	, perc_attendance_pso_avg
	, perc_attendance_lbp_avg
	, attendance_4th_quartile
	, bytes_transferred_in_class_person_z_min
	, bytes_transferred_in_class_person_z_max
	, bytes_transferred_in_class_person_z_avg
	, bytes_transferred_in_class_person_z_median
	, bytes_transferred_in_class_person_z_stdd
	, num_transactions_in_class_person_z_min
	, num_transactions_in_class_person_z_max
	, num_transactions_in_class_person_z_avg
	, num_transactions_in_class_person_z_median
	, num_transactions_in_class_person_z_stdd
	, bytes_transferred_in_class_person_z_min_lec
	, bytes_transferred_in_class_person_z_min_lab
	, bytes_transferred_in_class_person_z_min_rec
	, bytes_transferred_in_class_person_z_min_sd
	, bytes_transferred_in_class_person_z_min_pso
	, bytes_transferred_in_class_person_z_min_lbp
	, bytes_transferred_in_class_person_z_max_lec
	, bytes_transferred_in_class_person_z_max_lab
	, bytes_transferred_in_class_person_z_max_rec
	, bytes_transferred_in_class_person_z_max_sd
	, bytes_transferred_in_class_person_z_max_pso
	, bytes_transferred_in_class_person_z_max_lbp
	, bytes_transferred_in_class_person_z_avg_lec
	, bytes_transferred_in_class_person_z_avg_lab
	, bytes_transferred_in_class_person_z_avg_rec
	, bytes_transferred_in_class_person_z_avg_sd
	, bytes_transferred_in_class_person_z_avg_pso
	, bytes_transferred_in_class_person_z_avg_lbp
	, bytes_transferred_in_class_person_z_median_lec
	, bytes_transferred_in_class_person_z_median_lab
	, bytes_transferred_in_class_person_z_median_rec
	, bytes_transferred_in_class_person_z_median_sd
	, bytes_transferred_in_class_person_z_medianpso
	, bytes_transferred_in_class_person_z_median_lbp
	, bytes_transferred_in_class_person_z_stdd_lec
	, bytes_transferred_in_class_person_z_stdd_lab
	, bytes_transferred_in_class_person_z_stdd_rec
	, bytes_transferred_in_class_person_z_stdd_sd
	, bytes_transferred_in_class_person_z_stdd_pso
	, bytes_transferred_in_class_person_z_stdd_lbp
	, num_transactions_in_class_person_z_min_lec
	, num_transactions_in_class_person_z_min_lab
	, num_transactions_in_class_person_z_min_rec
	, num_transactions_in_class_person_z_min_sd
	, num_transactions_in_class_person_z_min_pso
	, num_transactions_in_class_person_z_min_lbp
	, num_transactions_in_class_person_z_max_lec
	, num_transactions_in_class_person_z_max_lab
	, num_transactions_in_class_person_z_max_rec
	, num_transactions_in_class_person_z_max_sd
	, num_transactions_in_class_person_z_max_pso
	, num_transactions_in_class_person_z_max_lbp
	, num_transactions_in_class_person_z_avg_lec
	, num_transactions_in_class_person_z_avg_lab
	, num_transactions_in_class_person_z_avg_rec
	, num_transactions_in_class_person_z_avg_sd
	, num_transactions_in_class_person_z_avg_pso
	, num_transactions_in_class_person_z_avg_lbp
	, num_transactions_in_class_person_z_median_lec
	, num_transactions_in_class_person_z_median_lab
	, num_transactions_in_class_person_z_median_rec
	, num_transactions_in_class_person_z_median_sd
	, num_transactions_in_class_person_z_medianpso
	, num_transactions_in_class_person_z_median_lbp
	, num_transactions_in_class_person_z_stdd_lec
	, num_transactions_in_class_person_z_stdd_lab
	, num_transactions_in_class_person_z_stdd_rec
	, num_transactions_in_class_person_z_stdd_sd
	, num_transactions_in_class_person_z_stdd_pso
	, num_transactions_in_class_person_z_stdd_lbp
	, brd_pln_dining_usage_prob_hr_of_day_21
	, brd_pln_dining_usage_prob_hr_of_day_9
	, brd_pln_dining_usage_prob_hr_of_day_22
	, brd_pln_dining_usage_prob_hr_of_day_23
	, brd_pln_dining_usage_prob_hr_of_day_19
	, brd_pln_dining_usage_prob_hr_of_day_5
	, brd_pln_dining_usage_prob_hr_of_day_8
	, brd_pln_dining_usage_prob_hr_of_day_20
	, brd_pln_dining_usage_prob_hr_of_day_18
	, brd_pln_dining_usage_prob_hr_of_day_12
	, brd_pln_dining_usage_prob_hr_of_day_13
	, brd_pln_dining_usage_prob_hr_of_day_14
	, brd_pln_dining_usage_prob_hr_of_day_11
	, brd_pln_dining_usage_prob_hr_of_day_6
	, brd_pln_dining_usage_prob_hr_of_day_16
	, brd_pln_dining_usage_prob_hr_of_day_15
	, brd_pln_dining_usage_prob_hr_of_day_17
	, brd_pln_dining_usage_prob_hr_of_day_10
	, brd_pln_dining_usage_prob_hr_of_day_7
	, door_access_prob_hr_of_day_8
	, door_access_prob_hr_of_day_20
	, door_access_prob_hr_of_day_22
	, door_access_prob_hr_of_day_21
	, door_access_prob_hr_of_day_9
	, door_access_prob_hr_of_day_7
	, door_access_prob_hr_of_day_3
	, door_access_prob_hr_of_day_18
	, door_access_prob_hr_of_day_5
	, door_access_prob_hr_of_day_15
	, door_access_prob_hr_of_day_17
	, door_access_prob_hr_of_day_13
	, door_access_prob_hr_of_day_23
	, door_access_prob_hr_of_day_12
	, door_access_prob_hr_of_day_16
	, door_access_prob_hr_of_day_10
	, door_access_prob_hr_of_day_6
	, door_access_prob_hr_of_day_0
	, door_access_prob_hr_of_day_14
	, door_access_prob_hr_of_day_2
	, door_access_prob_hr_of_day_11
	, door_access_prob_hr_of_day_1
	, door_access_prob_hr_of_day_4
	, door_access_prob_hr_of_day_19
	, household_num_members
	, rectrac_login_dow_fri
	, rectrac_login_dow_thu
	, rectrac_login_dow_sun
	, rectrac_login_dow_wed
	, rectrac_login_dow_tue
	, rectrac_login_dow_mon
	, rectrac_login_dow_sat
	, avg_weekly_rectrac_swipes
	, strd_val_tran_dining_usage_prob_hr_of_day_22
	, strd_val_tran_dining_usage_prob_hr_of_day_23
	, strd_val_tran_dining_usage_prob_hr_of_day_7
	, strd_val_tran_dining_usage_prob_hr_of_day_17
	, strd_val_tran_dining_usage_prob_hr_of_day_18
	, strd_val_tran_dining_usage_prob_hr_of_day_21
	, strd_val_tran_dining_usage_prob_hr_of_day_9
	, strd_val_tran_dining_usage_prob_hr_of_day_16
	, strd_val_tran_dining_usage_prob_hr_of_day_12
	, strd_val_tran_dining_usage_prob_hr_of_day_6
	, strd_val_tran_dining_usage_prob_hr_of_day_5
	, strd_val_tran_dining_usage_prob_hr_of_day_15
	, strd_val_tran_dining_usage_prob_hr_of_day_13
	, strd_val_tran_dining_usage_prob_hr_of_day_14
	, strd_val_tran_dining_usage_prob_hr_of_day_10
	, strd_val_tran_dining_usage_prob_hr_of_day_11
	, strd_val_tran_dining_usage_prob_hr_of_day_8
	, strd_val_tran_dining_usage_prob_hr_of_day_20
	, strd_val_tran_dining_usage_prob_hr_of_day_19
	, strd_val_tran_laundry_usage_prob_hr_of_day_22
	, strd_val_tran_laundry_usage_prob_hr_of_day_21
	, strd_val_tran_laundry_usage_prob_hr_of_day_9
	, strd_val_tran_laundry_usage_prob_hr_of_day_3
	, strd_val_tran_laundry_usage_prob_hr_of_day_13
	, strd_val_tran_laundry_usage_prob_hr_of_day_7
	, strd_val_tran_laundry_usage_prob_hr_of_day_1
	, strd_val_tran_laundry_usage_prob_hr_of_day_6
	, strd_val_tran_laundry_usage_prob_hr_of_day_10
	, strd_val_tran_laundry_usage_prob_hr_of_day_11
	, strd_val_tran_laundry_usage_prob_hr_of_day_17
	, strd_val_tran_laundry_usage_prob_hr_of_day_12
	, strd_val_tran_laundry_usage_prob_hr_of_day_14
	, strd_val_tran_laundry_usage_prob_hr_of_day_2
	, strd_val_tran_laundry_usage_prob_hr_of_day_5
	, strd_val_tran_laundry_usage_prob_hr_of_day_23
	, strd_val_tran_laundry_usage_prob_hr_of_day_16
	, strd_val_tran_laundry_usage_prob_hr_of_day_15
	, strd_val_tran_laundry_usage_prob_hr_of_day_19
	, strd_val_tran_laundry_usage_prob_hr_of_day_8
	, strd_val_tran_laundry_usage_prob_hr_of_day_20
	, strd_val_tran_laundry_usage_prob_hr_of_day_18
	, strd_val_tran_laundry_usage_prob_hr_of_day_4
	, strd_val_tran_laundry_usage_prob_hr_of_day_0
	, avg_sun_num_sessions
	, avg_mon_num_sessions
	, avg_tue_num_sessions
	, avg_wed_num_sessions
	, avg_thu_num_sessions
	, avg_fri_num_sessions
	, avg_sat_num_sessions
	, stddev_sun_num_sessions
	, stddev_mon_num_sessions
	, stddev_tue_num_sessions
	, stddev_wed_num_sessions
	, stddev_thu_num_sessions
	, stddev_fri_num_sessions
	, stddev_sat_num_sessions
	, avg_session_duration_mins
	, stddev_session_duration_mins
	, avg_num_courses_per_week
	, stddev_num_courses_per_week
	, admin_attr_algs
	, admin_attr_atfc
	, admin_attr_amwg
	, admin_attr_aesl
	, admin_attr_ambb
	, admin_attr_awsc
	, admin_attr_arnc
	, admin_attr_apvt
	, admin_attr_apmd
	, admin_attr_aste
	, admin_attr_awvb
	, admin_attr_awsw
	, admin_attr_amsw
	, o2o3flag
	, not_first_choice_major
	, ap_cnt
	, ap_bio
	, ap_german
	, ap_hum_geo
	, ap_eng_lit
	, ap_phys1
	, ap_span_lang
	, ap_phys2
	, ap_japa
	, ap_compsciab
	, ap_physc_mech
	, ap_usgov
	, ap_calcbc
	, ap_chin
	, ap_compscia
	, ap_envi
	, ap_calcab
	, ap_euro
	, ap_physc_elec
	, ap_chem
	, ap_world_hist
	, ap_compgov
	, ap_psyc
	, ap_span_lit
	, ap_physb
	, ap_ital
	, ap_stat
	, ap_lati
	, ap_ushist
	, ap_engl
	, total_ap_course_credits
	, highest_satr_math
	, highest_satr_ebrw
	, highest_satr_total
	, toefl
	, aleks
	, ap_avg
	, hs_state_in
	, hs_state_il
	, hs_gpa
	, hs_size
	, total_transfer_credits
	, hs_inst_gpa
	, hs_gpa_vs_hs_inst_gpa_diff
	, hs_core_gpa
	, driving_time_nrm
	, population
	, bach25plus
	, mast25plus
	, medianmalebachincome
	, medianfemalebachincome
	, per_capita_income
	, decision_count
	, age
	, cume_ap_credits_earned
	, first_term_num_fails
	, hs_vs_purdue_gpa_diff
	, enroll_college_os
	, enroll_college_ne
	, enroll_college_id
	, enroll_college_hs
	, enroll_college_be
	, enroll_college_el
	, enroll_college_mt
	, enroll_college_ms
	, enroll_college_bc
	, enroll_college_cp
	, enroll_college_ae
	, enroll_college_p
	, enroll_college_at
	, enroll_college_ec
	, enroll_college_us
	, enroll_college_pp
	, enroll_college_v
	, enroll_college_la
	, enroll_college_ie
	, enroll_college_nd
	, enroll_college_ab
	, enroll_college_pe
	, enroll_college_ev
	, enroll_college_hh
	, enroll_college_pc
	, enroll_college_a
	, enroll_college_cf
	, enroll_college_m
	, enroll_college_pi
	, enroll_college_cn
	, enroll_college_e
	, enroll_college_f
	, enroll_college_cm
	, enroll_college_ch
	, enroll_college_nr
	, enroll_college_ce
	, enroll_college_cg
	, enroll_college_me
	, enroll_college_le
	, enroll_college_s
	, enroll_college_it
	, enroll_college_t
	, enroll_college_eu
	, student_classification_boap_08
	, student_classification_boap_04
	, student_classification_boap_05
	, student_classification_boap_02
	, student_classification_boap_07
	, student_classification_boap_03
	, student_classification_boap_06
	, student_classification_boap_01
	, campus_num_changes
	, college_num_changes
	, major_num_changes
	, program_num_changes
	, profile_college_ie
	, profile_college_nd
	, profile_college_la
	, profile_college_me
	, profile_college_ec
	, profile_college_us
	, profile_college_pp
	, profile_college_p
	, profile_college_f
	, profile_college_cn
	, profile_college_e
	, profile_college_hh
	, profile_college_ch
	, profile_college_ab
	, profile_college_ev
	, profile_college_v
	, profile_college_pc
	, profile_college_a
	, profile_college_s
	, profile_college_eu
	, profile_college_ae
	, profile_college_ne
	, profile_college_id
	, profile_college_ms
	, profile_college_be
	, profile_college_m
	, profile_college_ce
	, profile_college_pi
	, profile_admissions_population_rp
	, profile_admissions_population_x
	, profile_admissions_population_none
	, profile_admissions_population_t
	, profile_admissions_population_nh
	, profile_admissions_population_ex
	, profile_admissions_population_nu
	, profile_admissions_population_cc
	, profile_admissions_population_a
	, profile_admissions_population_b
	, profile_admissions_population_r
	, profile_admissions_population_d
	, profile_admissions_population_nx
	, profile_admissions_population_sb
	, profile_admissions_population_ns
	, profile_admissions_population_st
	, profile_classification_boap_05
	, profile_classification_boap_04
	, profile_classification_boap_07
	, profile_classification_boap_02
	, profile_classification_boap_06
	, profile_classification_boap_01
	, profile_classification_boap_03
	, profile_classification_boap_08
	, profile_residence_r
	, profile_residence_f
	, profile_residence_n
	, profile_reporting_ethnicity_asian
	, profile_reporting_ethnicity_native_hawaiian_or_other_pacific_is
	, profile_reporting_ethnicity_unknown
	, profile_reporting_ethnicity_international
	, profile_reporting_ethnicity_black_or_african_american
	, profile_reporting_ethnicity_white
	, profile_reporting_ethnicity_american_indian_or_alaska_native
	, profile_reporting_ethnicity_2_or_more_races
	, profile_reporting_ethnicity_hispanic_latino
	, profile_underrep_minority_ind_y
	, profile_underrep_minority_ind_n
	, profile_gender_f
	, profile_gender_m
	, profile_high_school_percentile
	, profile_highest_act_english
	, profile_highest_act_math
	, profile_highest_act_reading
	, profile_highest_act_sci_reason
	, profile_highest_act_composite
	, profile_highest_act_englwrit
	, lives_on_campus
	, housing_building_code_wily
	, housing_building_code_pvil
	, housing_building_code_hill
	, housing_building_code_erht
	, housing_building_code_mrdh
	, housing_building_code_tark
	, housing_building_code_hltp
	, housing_building_code_wind
	, housing_building_code_tssu
	, housing_building_code_hawk
	, housing_building_code_mcut
	, housing_building_code_cary
	, housing_building_code_frst
	, housing_building_code_none
	, housing_building_code_shrv
	, housing_building_code_harr
	, housing_building_code_owen
	, s_hold_count

	, case when primary_source_web = True then 1 else 0 end as primary_source_web
	, case when primary_source_manual = True then 1 else 0 end as primary_source_manual
	, case when primary_source_ca = True then 1 else 0 end as primary_source_ca
	, case when finaid_applicant_ind_y = True then 1 else 0 end as finaid_applicant_ind_y
	, case when finaid_applicant_ind_n = True then 1 else 0 end as finaid_applicant_ind_n
      
  FROM 
	ke.decline_mod_input_table_netlog_card_lms_sis_09_Nov_2016 s
  LEFT JOIN
	features_grade.prior_purdue_gpa p
  ON	
	(s.person_uid = p.person_uid)
WHERE
	-- These features are built following 201610, so the academic period won't match our 201620 for the dep var
	s.academic_period = '201610'
	and p.academic_period = 201620
	AND (s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_train)
		OR
	     s.person_uid in (SELECT person_uid FROM ke.gpa_decline_201610_validation)
	)
limit 199
;

select * from ke.decline_mod_input_table_netlog_card_lms_sis_19_oct_2016 limit 100
SELECT * FROM ke.gpa_decline_201610_validation limit 100
SELECT *
  FROM features_grade.prior_purdue_gpa f limit 100
--Prior gpa isn't correct in the above table
SELECT f.person_uid, f.prior_purdue_gpa
  FROM features_grade.prior_purdue_gpa f
  where f.person_uid in (SELECT v.person_uid FROM ke.gpa_decline_201610_validation v)
  and f.academic_period = 201620
;

-- Persons in training set
SELECT person_uid FROM ke.gpa_decline_201610_train;

-- Persons in validation set
SELECT person_uid FROM ke.gpa_decline_201610_validation;

SELECT person_uid, case when gt_std_decline = 0 then 1 else 0 end as gt_std_decline FROM ke.gpa_decline_201610_train;
SELECT person_uid, case when gt_std_decline = 0 then 1 else 0 end as gt_std_decline FROM ke.gpa_decline_201610_validation;

SELECT person_uid,gt_std_decline FROM ke.gpa_decline_201610_train;
SELECT person_uid, gt_std_decline FROM ke.gpa_decline_201610_validation;

-- Behavioral Feats by importance score
'avg_arrival_prompt_rec_avg', 
'perc_attendance_all_avg', 
'avg_arrival_prompt_all_avg', 
'perc_attendance_rec_avg', 
'door_access_prob_hr_of_day_19', 
'brd_pln_dining_usage_prob_hr_of_day_16', 
'perc_attendance_lab_avg', 
'perc_attendance_lec_avg', 
'avg_arrival_prompt_sd_avg', 
'avg_arrival_prompt_lec_avg', 
'door_access_prob_hr_of_day_20', 
'door_access_prob_hr_of_day_12', 
'door_access_prob_hr_of_day_14', 
'brd_pln_dining_usage_prob_hr_of_day_12', 
'brd_pln_dining_usage_prob_hr_of_day_11', 
'brd_pln_dining_usage_prob_hr_of_day_20', 
'perc_attendance_pso_avg', 
'door_access_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_17', 
'perc_attendance_sd_avg', 
'strd_val_tran_laundry_usage_prob_hr_of_day_21', 
'strd_val_tran_laundry_usage_prob_hr_of_day_16', 
'door_access_prob_hr_of_day_0', 
'door_access_prob_hr_of_day_11', 
'brd_pln_dining_usage_prob_hr_of_day_19', 
'avg_arrival_prompt_lab_avg', 
'door_access_prob_hr_of_day_8', 
'avg_arrival_prompt_pso_avg', 
'door_access_prob_hr_of_day_4', 
'door_access_prob_hr_of_day_23', 
'door_access_prob_hr_of_day_1', 
'avg_weekly_rectrac_swipes', 
'associates_positivity', 
'strd_val_tran_laundry_usage_prob_hr_of_day_15', 
'strd_val_tran_laundry_usage_prob_hr_of_day_23', 
'strd_val_tran_laundry_usage_prob_hr_of_day_18', 
'colo_positivity', 
'door_access_prob_hr_of_day_10', 
'door_access_prob_hr_of_day_16', 
'strd_val_tran_laundry_usage_prob_hr_of_day_9', 
'door_access_prob_hr_of_day_3', 
'brd_pln_dining_usage_prob_hr_of_day_14', 
'brd_pln_dining_usage_prob_hr_of_day_15', 
'brd_pln_dining_usage_prob_hr_of_day_18', 
'rectrac_login_dow_thu', 
'rectrac_login_dow_tue', 
'rectrac_login_dow_wed', 
'door_access_prob_hr_of_day_9', 
'door_access_prob_hr_of_day_5', 
'door_access_prob_hr_of_day_21', 
'door_access_prob_hr_of_day_2', 
'door_access_prob_hr_of_day_7', 
'strd_val_tran_laundry_usage_prob_hr_of_day_19', 
'brd_pln_dining_usage_prob_hr_of_day_9', 
'brd_pln_dining_usage_prob_hr_of_day_8', 
'door_access_prob_hr_of_day_17', 
'strd_val_tran_laundry_usage_prob_hr_of_day_10', 
'brd_pln_dining_usage_prob_hr_of_day_10', 
'avg_arrival_prompt_lbp_avg', 
'strd_val_tran_dining_usage_prob_hr_of_day_11', 
'strd_val_tran_dining_usage_prob_hr_of_day_10', 
'strd_val_tran_laundry_usage_prob_hr_of_day_22', 
'strd_val_tran_laundry_usage_prob_hr_of_day_14', 
'strd_val_tran_laundry_usage_prob_hr_of_day_13', 
'brd_pln_dining_usage_prob_hr_of_day_7', 
'door_access_prob_hr_of_day_15', 
'door_access_prob_hr_of_day_18', 
'strd_val_tran_dining_usage_prob_hr_of_day_9', 
'strd_val_tran_laundry_usage_prob_hr_of_day_11', 
'strd_val_tran_dining_usage_prob_hr_of_day_18', 
'strd_val_tran_dining_usage_prob_hr_of_day_19', 
'rectrac_login_dow_mon', 
'rectrac_login_dow_fri'
