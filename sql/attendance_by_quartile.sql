-- 266644 rows affected, 25956 ms execution time
drop table if exists ke.attendance_by_quartile;
create table ke.attendance_by_quartile as (
with week_term as (
	select
		academic_period
		, date_trunc('week', activity_date) as week
		, row_number() over(partition by academic_period order by date_trunc('week', activity_date))::numeric wot
	from
		features_network.attendance_by_person_session_corrected
	group by 1,2
	order by 1,2
), ts as (
	select
		academic_period
		, max(w.wot) as m
	from
		week_term w
	group by academic_period
), term_stats as (
	select
		academic_period
		, round(ts.m,0) / 4 as q_size
	from
		ts
), qt as (
select
	w.*
	, case when w.wot > 0 and w.wot <= ts.q_size then
		1
		when w.wot > ts.q_size and w.wot <= 2*ts.q_size then
		2
		when w.wot > 2*ts.q_size and w.wot <= 3*ts.q_size then
		3
		when w.wot > 3*ts.q_size then
		4
		end as quartile
from 
	week_term w
left join
	term_stats ts
on
	w.academic_period = ts.academic_period
-- order by 1,2
), att as (
	select
		*
		, date_trunc('week', activity_date) as week
	from
		features_network.attendance_by_person_session_corrected
), quartile_attend as (
select
	a.person_uid
	, a.academic_period
	, q.quartile
	, sum(a.attended)::numeric as attended
	, sum(a.absent)::numeric as absent
	, (sum(a.attended) + sum(a.absent))::numeric as num_sessions
from
	att a
left join
	qt q
on
	(q.academic_period = a.academic_period
	and a.week = q.week)
group by 1,2,3
-- limit 1000
)
select
	*
	, attended / num_sessions as attendance
from
	quartile_attend
)distributed by (person_uid);

-- 66661 rows affected, 21024 ms execution time
drop table if exists ke.attendance_by_quartile_1;
create table ke.attendance_by_quartile_1 as (select * from ke.attendance_by_quartile where quartile = 1 ) distributed by (person_uid);

-- 66661 rows affected, 21175 ms execution time
drop table if exists ke.attendance_by_quartile_2;
create table ke.attendance_by_quartile_2 as (select * from ke.attendance_by_quartile where quartile = 2 ) distributed by (person_uid);

-- 66661 rows affected, 21437 ms execution time
drop table if exists ke.attendance_by_quartile_3;
create table ke.attendance_by_quartile_3 as (select * from ke.attendance_by_quartile where quartile = 3 ) distributed by (person_uid);

-- 66661 rows affected, 20854 ms execution time
drop table if exists ke.attendance_by_quartile_4;
create table ke.attendance_by_quartile_4 as (select * from ke.attendance_by_quartile where quartile = 4 ) distributed by (person_uid);





networklog.usage_by_person_class_week_nrm



-- 266644 rows affected, 25956 ms execution time
drop table if exists ke.network_use_by_quartile;
create table ke.network_use_by_quartile as (
with week_term as (
	select
		academic_period
		, week
		, row_number() over(partition by academic_period order by week)::numeric wot
	from
		networklog.usage_by_person_class_week_nrm
	group by 1,2
	order by 1,2
), ts as (
	select
		academic_period
		, max(w.wot) as m
	from
		week_term w
	group by academic_period
), term_stats as (
	select
		academic_period
		, round(ts.m,0) / 4 as q_size
	from
		ts
), qt as (
select
	w.*
	, case when w.wot > 0 and w.wot <= ts.q_size then
		1
		when w.wot > ts.q_size and w.wot <= 2*ts.q_size then
		2
		when w.wot > 2*ts.q_size and w.wot <= 3*ts.q_size then
		3
		when w.wot > 3*ts.q_size then
		4
		end as quartile
from 
	week_term w
left join
	term_stats ts
on
	w.academic_period = ts.academic_period
-- order by 1,2
)
select
	a.person_uid
	, a.academic_period
	, q.quartile
	, round(avg(a.bytes_transferred_in_class_person_z),4)::numeric as bytes_avg_z
	, round(avg(a.num_transactions_in_class_person_z),4)::numeric as transactions_avg_z
from
	networklog.usage_by_person_class_week_nrm a
left join
	qt q
on
	(q.academic_period = a.academic_period
	and a.week = q.week)
group by 1,2,3
)distributed by (person_uid);

drop table if exists ke.network_use_by_quartile_4;
create table ke.network_use_by_quartile_4 as (select * from ke.network_use_by_quartile where quartile = 4 ) distributed by (person_uid);
