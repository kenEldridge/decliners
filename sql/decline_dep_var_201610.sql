﻿﻿
DROP TABLE IF EXISTS ke.one_term_out_201610;
CREATE TABLE ke.one_term_out_201610
AS
(
SELECT
	*,
	CASE WHEN term_gpa_2 IS NULL THEN
		TRUE
	END AS DROPPED
FROM
(
	SELECT 
		s2.person_uid,
		s2.id,
		s2.profile_academic_period,
		s2.profile_level,
		max(term_gpa_1) AS term_gpa_1,
		max(term_gpa_2) AS term_gpa_2,
		(max(term_gpa_1) - max(term_gpa_2)) as decline,
		max(level_gpa_1) AS level_gpa_1,
		max(level_gpa_2) AS level_gpa_2
	FROM
	(
		SELECT
			*,
			CASE
				WHEN s.academic_period = '201610' THEN
				s.term_gpa
			END AS term_gpa_1,
			CASE
				WHEN s.academic_period = '201620' THEN
				s.term_gpa
			END AS term_gpa_2,
			CASE
				WHEN s.academic_period = '201610' THEN
				s.level_gpa
			END AS level_gpa_1,
			CASE
				WHEN s.academic_period = '201620' THEN
				s.level_gpa
			END AS level_gpa_2
		FROM
			(
			SELECT 
				t.person_uid,
				t.id, 
				p.profile_academic_period,
				p.profile_level,
				lvl.academic_period,
				t.gpa AS term_gpa,
				lvl.gpa AS level_gpa
			FROM 
				sis.frz_gpa_by_level lvl
			INNER JOIN sis.frz_gpa_by_term t
			ON
				t.person_uid = lvl.person_uid
				AND t.academic_period = lvl.academic_period
				AND t.freeze_event = lvl.freeze_event
				AND t.gpa_type = lvl.gpa_type
			INNER JOIN sis.student_profile_pu p
			ON
				(
				p.person_uid = lvl.person_uid
				)
			WHERE
				lvl.freeze_event = 'TERM_END'
				AND lvl.gpa_type = 'I'
				AND p.profile_academic_period = '201610'
				AND p.profile_level = 'UG'
				AND p.profile_reporting_campus = 'PWL'
				AND profile_firstime_fulltime_ind = 'Y'
			)s
		)s2
	GROUP BY 1,2,3,4
)s3
)DISTRIBUTED BY (PERSON_UID)
;

DROP TABLE IF EXISTS ke.one_term_out_201610_stats;
CREATE TABLE ke.one_term_out_201610_stats
AS
	(
	SELECT
		oto.profile_academic_period,
		stddev(decline) AS std,
		avg(decline) AS mean, 
		median(decline) AS median
	FROM
		ke.one_term_out_201610 oto
	GROUP BY 1
	)
;

DROP TABLE IF EXISTS ke.gpa_decline_201610;
CREATE TABLE ke.gpa_decline_201610
AS
(
	SELECT
		s.person_uid,
		s.id,
		s.academic_period,
		term_gpa_1,
		CASE WHEN s.dropped IS TRUE THEN
			1
		ELSE
			0
		END AS dropped,
		CASE WHEN gt_median_decline IS TRUE THEN
			1
		ELSE
			0
		END AS gt_median_decline,
		CASE WHEN gt_std_decline IS TRUE THEN
			1
		ELSE
			0
		END AS gt_std_decline,
		CASE WHEN gt_2std_decline IS TRUE IS TRUE THEN
			1
		ELSE
			0
		END AS gt_2std_decline,
		sr.row_number
	FROM
		(
		SELECT
			oto.person_uid,
			oto.id,
			oto.profile_academic_period as academic_period,
			oto.dropped IS TRUE AS dropped,
			oto.term_gpa_1,
			(decline > median) IS TRUE AS gt_median_decline,
			(decline > median + std) IS TRUE AS gt_std_decline,
			(decline > median + 2*std) IS TRUE AS gt_2std_decline
		FROM
			ke.one_term_out_201610 oto
		INNER JOIN
			ke.one_term_out_201610_stats s
		ON
			(
			oto.profile_academic_period = s.profile_academic_period
			)
		)s
		INNER JOIN
			ke.student_profile_random_201610 sr
		ON
			(sr.person_uid = s.person_uid)
)DISTRIBUTED BY (person_uid)
;


-- 6853
-- select count(*) from ke.gpa_decline_201610;
-- select * from ke.gpa_decline_201610 limit 100;

-- 4795
DROP TABLE IF EXISTS ke.gpa_decline_201610_train;
CREATE TABLE ke.gpa_decline_201610_train
as
SELECT
	*
FROM
	ke.gpa_decline_201610
WHERE
	row_number < (SELECT count(*) * 0.7 FROM ke.gpa_decline_201610)
distributed by (person_uid)
; -- select count(*) from ke.gpa_decline_201610_train
-- select person_uid, gt_std_decline from ke.gpa_decline_201610_train

-- 1028
DROP TABLE IF EXISTS ke.gpa_decline_201610_validation;
CREATE TABLE ke.gpa_decline_201610_validation
as
SELECT
	*
FROM
	ke.gpa_decline_201610
WHERE
	row_number >= (SELECT count(*) * 0.7 FROM ke.gpa_decline_201610)
	AND row_number < (SELECT count(*) * 0.85 FROM ke.gpa_decline_201610)
distributed by (person_uid)
; -- select count(*) from ke.gpa_decline_201610_validation
-- select person_uid, gt_std_decline from ke.gpa_decline_201610_validation

-- 1030
DROP TABLE IF EXISTS ke.gpa_decline_201610_test;
CREATE TABLE ke.gpa_decline_201610_test
as
SELECT
	*
FROM
	ke.gpa_decline_201610
WHERE
	row_number >= (SELECT count(*) * 0.85 FROM ke.gpa_decline_201610)
distributed by (person_uid)
; -- select count(*) from ke.gpa_decline_201610_test


-- 6853
SELECT (select count(*) from ke.gpa_decline_201610_train) 
	+ (select count(*) from ke.gpa_decline_201610_validation) 
	+ (select count(*) from ke.gpa_decline_201610_test)
;














