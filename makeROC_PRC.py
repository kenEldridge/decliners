import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt

name = sys.argv[1]

d = pd.read_csv('output/' + name + '_scores.csv')
# print d.shape, d.columns

roc_area = sum([x * d.shape[0] for x in d.recall]) / d.shape[0]**2
roc_area = round(roc_area, 3)

g = sns.jointplot(x='false_pos_rate'
             , y='recall'
             , data=d
             , kind='scatter'
             , xlim=[-0.1,1.05]
             , ylim=[-0.1,1.05]
            )
sns.plt.title(name + ' - Area: ' + str(roc_area))
g.savefig('output/' + name + '_roc.png')

prc_area = sum([x * d.shape[0] for x in d.precision]) / d.shape[0]**2
prc_area = round(prc_area, 3)

g = sns.jointplot(x='recall'
             , y='precision'
             , data=d
             , kind='scatter'
             , xlim=[-0.1,1.05]
             , ylim=[-0.1,1.05]
            )
sns.plt.title(name + ' - Area: ' + str(prc_area))
g.savefig('output/' + name + '_prc.png')
