package dec

import edu.umd.cs.psl.groovy.PSLModel


class AddStockRules{

	def m = null
	def st = null
	
	AddStockRules(Object m){
//		this.m = m
//		getPreds(m)
//		addRules(m)
	}
	
	def addRules(Object m){
		m.add rule : ( student(A) & admin_attr_aesl(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_aesl(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_algs(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_algs(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_ambb(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_ambb(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_amsw(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_amsw(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_amwg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_amwg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_apmd(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_apmd(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_apvt(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_apvt(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_arnc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_arnc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_aste(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_aste(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_atfc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_atfc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awsc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awsc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awsw(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awsw(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awvb(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & admin_attr_awvb(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & age(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & age(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & aleks(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & aleks(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_bio(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_bio(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_calcab(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_calcab(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_calcbc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_calcbc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_chem(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_chem(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_chin(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_chin(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_cnt(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_cnt(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compgov(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compgov(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compscia(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compscia(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compsciab(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_compsciab(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_eng_lit(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_eng_lit(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_engl(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_engl(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_envi(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_envi(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_euro(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_euro(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_german(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_german(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_hum_geo(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_hum_geo(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_ital(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_ital(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_japa(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_japa(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_lati(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_lati(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_phys1(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_phys1(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_phys2(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_phys2(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physb(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physb(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physc_elec(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physc_elec(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physc_mech(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_physc_mech(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_psyc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_psyc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_span_lang(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_span_lang(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_span_lit(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_span_lit(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_stat(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_stat(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_usgov(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_usgov(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_ushist(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_ushist(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & ap_world_hist(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & ap_world_hist(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & associates_positivity(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & associates_positivity(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & attendance(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & attendance(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & attendance_all(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & attendance_all(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & attendance_quartile(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & attendance_quartile(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lab_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lab_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lbp_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lbp_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lec_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_lec_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_pso_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_pso_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_sd_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_arrival_prompt_sd_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_fri_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_fri_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_mon_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_mon_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_num_courses_per_week(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_num_courses_per_week(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_sat_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_sat_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_session_duration_mins(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_session_duration_mins(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_sun_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_sun_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_thu_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_thu_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_tue_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_tue_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_wed_num_sessions(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_wed_num_sessions(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & avg_weekly_rectrac_swipes(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & avg_weekly_rectrac_swipes(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & bach25plus(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & bach25plus(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_10(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_11(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_12(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_12(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_13(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_13(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_14(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_14(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_15(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_15(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_16(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_16(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_17(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_17(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_18(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_19(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_20(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_20(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_21(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_21(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_22(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_22(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_23(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_23(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_5(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_5(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_6(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_6(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_7(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_7(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_8(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_8(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_9(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & bytes_avg_z(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & bytes_avg_z(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & bytes_quartile(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & bytes_quartile(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & campus_num_changes(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & campus_num_changes(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & college_num_changes(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & college_num_changes(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & colo_positivity(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & colo_positivity(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & cume_ap_credits_earned(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & cume_ap_credits_earned(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & decision_count(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & decision_count(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_0(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_0(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_1(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_1(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_10(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_11(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_12(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_12(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_13(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_13(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_14(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_14(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_15(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_15(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_16(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_16(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_17(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_17(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_18(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_19(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_2(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_2(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_20(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_20(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_21(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_21(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_22(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_22(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_23(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_23(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_3(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_3(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_4(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_4(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_5(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_5(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_6(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_6(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_7(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_7(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_8(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_8(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & door_access_prob_hr_of_day_9(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & driving_time_nrm(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & driving_time_nrm(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_a(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_a(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ab(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ab(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ae(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ae(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_at(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_at(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_bc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_bc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_be(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_be(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ce(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ce(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cf(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cf(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ch(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ch(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cm(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cm(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cn(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cn(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_cp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_e(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_e(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ec(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ec(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_el(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_el(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_eu(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_eu(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ev(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ev(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_f(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_f(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_hh(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_hh(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_hs(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_hs(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_id(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_id(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ie(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ie(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_it(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_it(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_la(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_la(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_le(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_le(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_m(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_m(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_me(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_me(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ms(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ms(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_mt(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_mt(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_nd(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_nd(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ne(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_ne(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_nr(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_nr(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_os(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_os(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_p(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_p(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pc(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pc(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pe(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pe(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pi(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pi(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_pp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_s(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_s(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_t(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_t(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_us(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_us(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_v(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & enroll_college_v(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & father_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & father_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & finaid_applicant_ind_n(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & finaid_applicant_ind_n(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & finaid_applicant_ind_y(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & finaid_applicant_ind_y(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & first_term_num_fails(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & first_term_num_fails(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_father_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_father_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_mother_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_mother_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_parent_income(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & first_year_parent_income(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & has_need_based_aid(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & has_need_based_aid(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_ebrw(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_ebrw(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_math(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_math(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_total(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & highest_satr_total(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & household_num_members(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & household_num_members(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_cary(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_cary(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_erht(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_erht(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_frst(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_frst(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_harr(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_harr(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hawk(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hawk(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hill(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hill(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hltp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_hltp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_mcut(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_mcut(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_mrdh(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_mrdh(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_none(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_none(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_owen(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_owen(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_pvil(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_pvil(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_shrv(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_shrv(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_tark(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_tark(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_tssu(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_tssu(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_wily(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_wily(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_wind(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & housing_building_code_wind(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_core_gpa(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_core_gpa(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_gpa(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_gpa(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_gpa_vs_hs_inst_gpa_diff(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_gpa_vs_hs_inst_gpa_diff(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_inst_gpa(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_inst_gpa(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_size(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_size(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_state_il(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_state_il(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_state_in(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_state_in(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & hs_vs_purdue_gpa_diff(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & hs_vs_purdue_gpa_diff(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & live_off_camp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & live_off_camp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & live_on_camp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & live_on_camp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & live_with_parents(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & live_with_parents(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & lives_on_campus(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & lives_on_campus(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & major_num_changes(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & major_num_changes(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & mast25plus(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & mast25plus(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & max_father_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & max_father_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & max_mother_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & max_mother_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & max_parent_income(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & max_parent_income(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & medianfemalebachincome(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & medianfemalebachincome(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & medianmalebachincome(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & medianmalebachincome(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & mother_highest_grade(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & mother_highest_grade(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & not_first_choice_major(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & not_first_choice_major(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & o2o3flag(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & o2o3flag(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & parent_income(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & parent_income(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & per_capita_income(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & per_capita_income(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_all_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_all_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_lab_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_lab_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_lec_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_lec_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_pso_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_pso_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_rec_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_rec_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_sd_avg(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_attendance_sd_avg(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_dependency_independ_n(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_dependency_independ_n(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_dependency_independ_y(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_dependency_independ_y(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_camp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_camp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_offcamp(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_offcamp(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_with_par(A) ) >> decline(A) , weight : 10
		m.add rule : ( student(A) & perc_housing_desc_with_par(A) ) >> not_decline(A) , weight : 10
		m.add rule : ( student(A) & perc_need_based_ind_n(A) ) >> decline(A) , weight : 10
		
	}
	
	def getPreds(Object m){
		def preds = [
//						m.admin_attr_aesl, 
//						m.admin_attr_algs, 
//						m.admin_attr_ambb, 
//						m.admin_attr_amsw, 
//						m.admin_attr_amwg, 
//						m.admin_attr_apmd, 
//						m.admin_attr_apvt, 
//						m.admin_attr_arnc, 
//						m.admin_attr_aste, 
//						m.admin_attr_atfc, 
//						m.admin_attr_awsc, 
//						m.admin_attr_awsw, 
//						m.admin_attr_awvb, 
//						m.age, 
//						m.aleks, 
//						m.ap_avg, 
//						m.ap_bio, 
//						m.ap_calcab, 
//						m.ap_calcbc, 
//						m.ap_chem, 
//						m.ap_chin, 
//						m.ap_cnt, 
//						m.ap_compgov, 
//						m.ap_compscia, 
//						m.ap_compsciab, 
//						m.ap_eng_lit, 
//						m.ap_engl, 
//						m.ap_envi, 
//						m.ap_euro, 
//						m.ap_german, 
//						m.ap_hum_geo, 
//						m.ap_ital, 
//						m.ap_japa, 
//						m.ap_lati, 
//						m.ap_phys1, 
//						m.ap_phys2, 
//						m.ap_physb, 
//						m.ap_physc_elec, 
//						m.ap_physc_mech, 
//						m.ap_psyc, 
//						m.ap_span_lang, 
//						m.ap_span_lit, 
//						m.ap_stat, 
//						m.ap_usgov, 
//						m.ap_ushist, 
//						m.ap_world_hist, 
//						m.associates_positivity, 
//						m.attendance, 
						m.attendance_all, 
						m.attendance_quartile, 
//						m.avg_arrival_prompt_all_avg, 
//						m.avg_arrival_prompt_lab_avg, 
//						m.avg_arrival_prompt_lbp_avg, 
//						m.avg_arrival_prompt_lec_avg, 
//						m.avg_arrival_prompt_pso_avg, 
//						m.avg_arrival_prompt_rec_avg, 
//						m.avg_arrival_prompt_sd_avg, 
//						m.avg_fri_num_sessions, 
//						m.avg_mon_num_sessions, 
//						m.avg_num_courses_per_week, 
//						m.avg_sat_num_sessions, 
//						m.avg_session_duration_mins, 
//						m.avg_sun_num_sessions, 
//						m.avg_thu_num_sessions, 
//						m.avg_tue_num_sessions, 
//						m.avg_wed_num_sessions, 
//						m.avg_weekly_rectrac_swipes, 
						m.bach25plus, 
//						m.brd_pln_dining_usage_prob_hr_of_day_10, 
//						m.brd_pln_dining_usage_prob_hr_of_day_11, 
//						m.brd_pln_dining_usage_prob_hr_of_day_12, 
//						m.brd_pln_dining_usage_prob_hr_of_day_13, 
//						m.brd_pln_dining_usage_prob_hr_of_day_14, 
//						m.brd_pln_dining_usage_prob_hr_of_day_15, 
//						m.brd_pln_dining_usage_prob_hr_of_day_16, 
//						m.brd_pln_dining_usage_prob_hr_of_day_17, 
//						m.brd_pln_dining_usage_prob_hr_of_day_18, 
//						m.brd_pln_dining_usage_prob_hr_of_day_19, 
//						m.brd_pln_dining_usage_prob_hr_of_day_20, 
//						m.brd_pln_dining_usage_prob_hr_of_day_21, 
//						m.brd_pln_dining_usage_prob_hr_of_day_22, 
//						m.brd_pln_dining_usage_prob_hr_of_day_23, 
//						m.brd_pln_dining_usage_prob_hr_of_day_5, 
//						m.brd_pln_dining_usage_prob_hr_of_day_6, 
//						m.brd_pln_dining_usage_prob_hr_of_day_7, 
//						m.brd_pln_dining_usage_prob_hr_of_day_8, 
//						m.brd_pln_dining_usage_prob_hr_of_day_9, 
						m.bytes_avg_z, 
//						m.bytes_quartile, 
//						m.campus_num_changes, 
//						m.college_num_changes, 
//						m.colo_positivity, 
//						m.cume_ap_credits_earned, 
//						m.decision_count, 
//						m.door_access_prob_hr_of_day_0, 
//						m.door_access_prob_hr_of_day_1, 
//						m.door_access_prob_hr_of_day_10, 
//						m.door_access_prob_hr_of_day_11, 
//						m.door_access_prob_hr_of_day_12, 
//						m.door_access_prob_hr_of_day_13, 
//						m.door_access_prob_hr_of_day_14, 
//						m.door_access_prob_hr_of_day_15, 
//						m.door_access_prob_hr_of_day_16, 
//						m.door_access_prob_hr_of_day_17, 
//						m.door_access_prob_hr_of_day_18, 
//						m.door_access_prob_hr_of_day_19, 
						m.door_access_prob_hr_of_day_2, 
//						m.door_access_prob_hr_of_day_20, 
//						m.door_access_prob_hr_of_day_21, 
//						m.door_access_prob_hr_of_day_22, 
//						m.door_access_prob_hr_of_day_23, 
						m.door_access_prob_hr_of_day_3, 
						m.door_access_prob_hr_of_day_4, 
//						m.door_access_prob_hr_of_day_5, 
//						m.door_access_prob_hr_of_day_6, 
//						m.door_access_prob_hr_of_day_7, 
//						m.door_access_prob_hr_of_day_8, 
//						m.door_access_prob_hr_of_day_9, 
						m.driving_time_nrm, 
//						m.enroll_college_a, 
//						m.enroll_college_ab, 
//						m.enroll_college_ae, 
//						m.enroll_college_at, 
//						m.enroll_college_bc, 
//						m.enroll_college_be, 
//						m.enroll_college_ce, 
//						m.enroll_college_cf, 
//						m.enroll_college_cg, 
//						m.enroll_college_ch, 
//						m.enroll_college_cm, 
//						m.enroll_college_cn, 
//						m.enroll_college_cp, 
//						m.enroll_college_e, 
//						m.enroll_college_ec, 
//						m.enroll_college_el, 
//						m.enroll_college_eu, 
//						m.enroll_college_ev, 
//						m.enroll_college_f, 
//						m.enroll_college_hh, 
//						m.enroll_college_hs, 
//						m.enroll_college_id, 
//						m.enroll_college_ie, 
//						m.enroll_college_it, 
//						m.enroll_college_la, 
//						m.enroll_college_le, 
//						m.enroll_college_m, 
//						m.enroll_college_me, 
//						m.enroll_college_ms, 
//						m.enroll_college_mt, 
//						m.enroll_college_nd, 
//						m.enroll_college_ne, 
//						m.enroll_college_nr, 
//						m.enroll_college_os, 
//						m.enroll_college_p, 
//						m.enroll_college_pc, 
//						m.enroll_college_pe, 
//						m.enroll_college_pi, 
//						m.enroll_college_pp, 
//						m.enroll_college_s, 
//						m.enroll_college_t, 
//						m.enroll_college_us, 
//						m.enroll_college_v, 
//						m.father_highest_grade, 
						m.finaid_applicant_ind_n, 
						m.finaid_applicant_ind_y, 
//						m.first_term_num_fails, 
//						m.first_year_father_highest_grade, 
//						m.first_year_mother_highest_grade, 
//						m.first_year_parent_income, 
//						m.has_need_based_aid, 
//						m.highest_satr_ebrw, 
//						m.highest_satr_math, 
//						m.highest_satr_total, 
//						m.household_num_members, 
//						m.housing_building_code_cary, 
//						m.housing_building_code_erht, 
//						m.housing_building_code_frst, 
//						m.housing_building_code_harr, 
//						m.housing_building_code_hawk, 
//						m.housing_building_code_hill, 
//						m.housing_building_code_hltp, 
//						m.housing_building_code_mcut, 
//						m.housing_building_code_mrdh, 
//						m.housing_building_code_none, 
//						m.housing_building_code_owen, 
//						m.housing_building_code_pvil, 
//						m.housing_building_code_shrv, 
//						m.housing_building_code_tark, 
//						m.housing_building_code_tssu, 
//						m.housing_building_code_wily, 
//						m.housing_building_code_wind, 
//						m.hs_core_gpa, 
//						m.hs_gpa, 
//						m.hs_gpa_vs_hs_inst_gpa_diff, 
//						m.hs_inst_gpa, 
						m.hs_size, 
//						m.hs_state_il, 
//						m.hs_state_in, 
//						m.hs_vs_purdue_gpa_diff, 
//						m.live_off_camp, 
//						m.live_on_camp, 
//						m.live_with_parents, 
//						m.lives_on_campus, 
//						m.major_num_changes, 
//						m.mast25plus, 
//						m.max_father_highest_grade, 
//						m.max_mother_highest_grade, 
//						m.max_parent_income, 
//						m.medianfemalebachincome, 
//						m.medianmalebachincome, 
//						m.mother_highest_grade, 
//						m.not_first_choice_major, 
//						m.o2o3flag, 
//						m.parent_income, 
//						m.per_capita_income, 
//						m.perc_attendance_all_avg, 
//						m.perc_attendance_lab_avg, 
//						m.perc_attendance_lec_avg, 
//						m.perc_attendance_pso_avg, 
//						m.perc_attendance_rec_avg, 
//						m.perc_attendance_sd_avg, 
//						m.perc_dependency_independ_n, 
//						m.perc_dependency_independ_y, 
//						m.perc_housing_desc_camp, 
//						m.perc_housing_desc_offcamp, 
//						m.perc_housing_desc_with_par, 
//						m.perc_need_based_ind_n, 
//						m.perc_need_based_ind_y, 
//						m.population, 
//						m.primary_source_ca, 
//						m.primary_source_manual, 
//						m.primary_source_web, 
//						m.prior_purdue_gpa, 
						m.prior_term_gpa, 
//						m.profile_admissions_population_a, 
//						m.profile_admissions_population_b, 
//						m.profile_admissions_population_cc, 
//						m.profile_admissions_population_d, 
//						m.profile_admissions_population_ex, 
//						m.profile_admissions_population_nh, 
//						m.profile_admissions_population_none, 
//						m.profile_admissions_population_ns, 
//						m.profile_admissions_population_nu, 
//						m.profile_admissions_population_nx, 
//						m.profile_admissions_population_r, 
//						m.profile_admissions_population_rp, 
//						m.profile_admissions_population_sb, 
//						m.profile_admissions_population_st, 
//						m.profile_admissions_population_t, 
//						m.profile_admissions_population_x, 
//						m.profile_classification_boap_01, 
//						m.profile_classification_boap_02, 
//						m.profile_classification_boap_03, 
//						m.profile_classification_boap_04, 
//						m.profile_classification_boap_05, 
//						m.profile_classification_boap_06, 
//						m.profile_classification_boap_07, 
//						m.profile_classification_boap_08, 
//						m.profile_college_a, 
//						m.profile_college_ab, 
//						m.profile_college_ae, 
//						m.profile_college_be, 
//						m.profile_college_ce, 
//						m.profile_college_ch, 
//						m.profile_college_cn, 
//						m.profile_college_e, 
//						m.profile_college_ec, 
//						m.profile_college_eu, 
//						m.profile_college_ev, 
//						m.profile_college_f, 
//						m.profile_college_hh, 
//						m.profile_college_id, 
//						m.profile_college_ie, 
//						m.profile_college_la, 
//						m.profile_college_m, 
//						m.profile_college_me, 
//						m.profile_college_ms, 
//						m.profile_college_nd, 
//						m.profile_college_ne, 
//						m.profile_college_p, 
//						m.profile_college_pc, 
//						m.profile_college_pi, 
//						m.profile_college_pp, 
//						m.profile_college_s, 
//						m.profile_college_us, 
//						m.profile_college_v, 
//						m.profile_gender_f, 
//						m.profile_gender_m, 
//						m.profile_high_school_percentile, 
//						m.profile_highest_act_composite, 
//						m.profile_highest_act_english, 
//						m.profile_highest_act_englwrit, 
//						m.profile_highest_act_math, 
//						m.profile_highest_act_reading, 
//						m.profile_highest_act_sci_reason, 
//						m.profile_reporting_ethnicity_2_or_more_races, 
//						m.profile_reporting_ethnicity_american_indian_or_alaska_native, 
//						m.profile_reporting_ethnicity_asian, 
//						m.profile_reporting_ethnicity_black_or_african_american, 
//						m.profile_reporting_ethnicity_hispanic_latino, 
//						m.profile_reporting_ethnicity_international, 
//						m.profile_reporting_ethnicity_native_hawaiian_or_other_pacific_is, 
//						m.profile_reporting_ethnicity_unknown, 
//						m.profile_reporting_ethnicity_white, 
//						m.profile_residence_f, 
//						m.profile_residence_n, 
//						m.profile_residence_r, 
						m.profile_underrep_minority_ind_n, 
						m.profile_underrep_minority_ind_y, 
//						m.program_num_changes, 
//						m.rectrac_login_dow_fri, 
//						m.rectrac_login_dow_mon, 
//						m.rectrac_login_dow_sat, 
//						m.rectrac_login_dow_sun, 
//						m.rectrac_login_dow_thu, 
//						m.rectrac_login_dow_tue, 
//						m.rectrac_login_dow_wed, 
//						m.s_hold_count, 
//						m.stddev_fri_num_sessions, 
//						m.stddev_mon_num_sessions, 
//						m.stddev_num_courses_per_week, 
//						m.stddev_sat_num_sessions, 
//						m.stddev_session_duration_mins, 
//						m.stddev_sun_num_sessions, 
//						m.stddev_thu_num_sessions, 
//						m.stddev_tue_num_sessions, 
//						m.stddev_wed_num_sessions, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_10, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_11, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_12, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_13, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_14, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_15, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_16, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_17, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_18, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_19, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_20, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_21, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_22, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_23, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_5, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_6, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_7, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_8, 
//						m.strd_val_tran_dining_usage_prob_hr_of_day_9, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_0, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_1, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_10, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_11, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_12, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_13, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_14, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_15, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_16, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_17, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_18, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_19, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_2, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_20, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_21, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_22, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_23, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_3, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_4, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_5, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_6, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_7, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_8, 
//						m.strd_val_tran_laundry_usage_prob_hr_of_day_9, 
//						m.term_gpa_1, 
//						m.toefl, 
//						m.total_ap_course_credits, 
//						m.total_fm_unmet_need, 
						m.total_transfer_credits,
						m.student
			]
//		for (r in rules){
//			m.add rule : ( m.student(m.A) & r(m.A) ) >> m.decline(m.A) , weight : 3
////			m.add rule : ( m.student(this.st) & r(this.st) ) >> m.not_decline(this.st) , weight : 10
//		}
		return preds
	}
		
}


