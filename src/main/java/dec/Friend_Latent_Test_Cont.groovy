package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.LazyMPEInference
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * The first thing we need to do is initialize a ConfigBundle and a DataStore
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("friend_simple")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "friend")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)


PSLModel m = new PSLModel(this, data)


m.add predicate: "colo",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID, ArgumentType.Double]
m.add predicate: "min_count",	types: [ArgumentType.UniqueID]
m.add predicate: "coswipe",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]
m.add predicate: "friend",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

m.add rule : colo(A, B, X) >> friend(A, B) , weight : 10
m.add rule : ~colo(A, B, Y) >> ~friend(A, B) , weight : 10
//m.add rule : coswipe(A, B) >> friend(A, B) , weight : 10
//m.add rule : (colo(A, B, Y) & coswipe(A, B)) >> friend(A, B) , weight : 10
//m.add rule : ( ~coswipe(A, B)) >> ~friend(A, B) , weight : 10
//m.add rule : ( ~colo(A, B) ) >> ~friend(A, B) , weight : 10

println m;


def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'raw'+java.io.File.separator+'friend_latent_test'+java.io.File.separator;

for (Predicate p : [colo, coswipe])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_cont.txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [colo, coswipe] as Set, evidencePartition);


LazyMPEInference inferenceApp = new LazyMPEInference(m, db, config);
inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's see the results
 */
println "Inference results with hand-defined weights:"
DecimalFormat formatter = new DecimalFormat("#.##");
for (GroundAtom atom : Queries.getAllAtoms(db, friend))
	println atom.toString() + "\t" + formatter.format(atom.getValue());


println 'done.'























