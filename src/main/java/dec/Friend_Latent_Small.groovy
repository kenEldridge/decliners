package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.LazyMPEInference
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * The first thing we need to do is initialize a ConfigBundle and a DataStore
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("friend_small")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "friend")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)


PSLModel m = new PSLModel(this, data)

// Location features
m.add predicate: "colo",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]
m.add predicate: "coswipe",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Homophily features
//m.add predicate: "residence",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Latent Variables
m.add predicate: "associated",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Resident Ground Terms
//GroundTerm resident = data.getUniqueID('R');
//GroundTerm forgien = data.getUniqueID('F');
//GroundTerm nonresident = data.getUniqueID('N');

// Location Rules to Build loc_assoc
m.add rule : colo(A, B) >> associated(A, B) , weight : 10
m.add rule : coswipe(A, B) >> associated(A, B) , weight : 10
m.add rule : ~colo(A, B) >> ~associated(A, B) , weight : 10
m.add rule : (colo(A, B) & coswipe(A, B)) >> associated(A, B) , weight : 10
m.add rule : (~colo(A, B) & coswipe(A, B)) >> ~associated(A, B) , weight : 10
m.add rule : (~colo(A, B) & ~coswipe(A, B)) >> ~associated(A, B) , weight : 10

println m;


def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'raw'+java.io.File.separator+'friend_latent_small'+java.io.File.separator;

for (Predicate p : [colo, coswipe])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+".txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [colo, coswipe] as Set, evidencePartition);


LazyMPEInference inferenceApp = new LazyMPEInference(m, db, config);
inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's see the results
 */
println "Inference results with learned weights:"
DecimalFormat formatter = new DecimalFormat("#.##");
def output_file = 'output' + java.io.File.separator + 'associated_latent.csv';
boolean fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
f.append("academic_period" + "," + "person_uid" + "," + "match_person_uid" + "," + "strength" + "\r\n") 

for (GroundAtom atom : Queries.getAllAtoms(db, associated))
{
	f.append("201610," + atom.getArguments()[0].toString() + "," + atom.getArguments()[1].toString() + "," + formatter.format(atom.getValue()) + "\r\n");
}

println 'done.'























