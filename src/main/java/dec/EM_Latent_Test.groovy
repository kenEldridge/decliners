package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.em.HardEM
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * Build an latent variable using expectation maximization and tie it to 
 * the dependent variable.  One model.
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("em_latent_test")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "em_latent")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)

// Instantiate a model
PSLModel m = new PSLModel(this, data)

// Base features predicates
m.add predicate: "student",		types: [ArgumentType.UniqueID]
m.add predicate: "aleks",		types: [ArgumentType.UniqueID]
m.add predicate: "prior_term_gpa",		types: [ArgumentType.UniqueID]
m.add predicate: "hs_core_gpa",		types: [ArgumentType.UniqueID]
m.add predicate: "attendance_all",		types: [ArgumentType.UniqueID]

// Groups
GroundTerm g0 = data.getUniqueID(2);
GroundTerm g1 = data.getUniqueID(3);
// Latent Variable predicate
m.add predicate: "group",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Dependent Variable
m.add predicate: "decline",		types: [ArgumentType.UniqueID]


// Logic Rules

// Group 0 - below - We expect these to be decliners
//m.add rule : aleks(A) >> group(A, g0) , weight : 5
//m.add rule : prior_term_gpa(A) >> group(A, g0) , weight : 5
//m.add rule : hs_core_gpa(A) >> group(A, g0) , weight : 5
//m.add rule : student(A) >> group(A, g0) , weight : 5
m.add rule : ( student(A) & ~aleks(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~prior_term_gpa(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~hs_core_gpa(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~attendance_all(A) ) >> group(A, g0) , weight : 10
//m.add rule : ( student(A) & ~aleks(A)  & ~prior_term_gpa(A) & ~hs_core_gpa(A)) >> decline(A) , weight : 10

// Group 1 - above
m.add rule : ( student(A) & aleks(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & prior_term_gpa(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & hs_core_gpa(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & aleks(A) & prior_term_gpa(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & aleks(A) &  hs_core_gpa(A)) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & prior_term_gpa(A) & hs_core_gpa(A)) >> group(A, g1) , weight : 10
//m.add rule : student(A) >> group(A, g1) , weight : 10

// Promote mutual exclusivity of latent var
m.add rule : group(A, g0) >> ~group(A, g1), weight : 10
m.add rule : group(A, g1) >> ~group(A, g0), weight : 10

// Add dependent variable
m.add rule : group(A, g0) >> decline(A), weight : 10
m.add rule : group(A, g1) >> ~decline(A), weight : 10

// Promote mutual exclusivity of dep var
m.add rule : decline(A) >> ~decline(A), weight : 10

// Manually defined model
println 'Manually defined model: \n' + m;

def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'em_latent_test'+java.io.File.separator;

for (Predicate p : [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all] as Set, evidencePartition);

Set<GroundTerm> group_vals = new HashSet<GroundTerm>();
group_vals.add(g0);
group_vals.add(g1);

def person_uid_list = []
new File( dir + "student_train.txt" ).eachLine { line ->
	person_uid_list << line
}

def decliner_train = []
new File( dir + "decline_train.txt" ).eachLine { line ->
	decliner_train << line.split('\t')[1]
}

Set<GroundTerm> people = new HashSet<GroundTerm>();
for (id in person_uid_list)
	people.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(db);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
popMap.put(new Variable("Groups"), group_vals);
dbPop.populate((decline(People)).getFormula(), popMap);
dbPop.populate((group(People, Groups)).getFormula(), popMap);

Partition trueDataPartition = new Partition(3);
insert = data.getInserter(decline, trueDataPartition)
InserterUtils.loadDelimitedDataTruth(insert, dir + "decline_train.txt");
	
Database trueDataDB = data.getDatabase(trueDataPartition, [decline] as Set);
dbPop = new DatabasePopulator(trueDataDB);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);

HardEM hardEM = new HardEM(m, db, trueDataDB, config);
hardEM.learn();

println m;

/*
 * Let's see the results
 */
DecimalFormat formatter = new DecimalFormat("#.##");
def output_file = 'output' + java.io.File.separator + 'em_latent.csv';
boolean fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
f.append("person_uid, predictin, ground\r\n") 

def i = 0
for (GroundAtom atom : Queries.getAllAtoms(db, decline))
{
	f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + decliner_train[i] + "\r\n");
	i++;
}
println "Inference results with learned weights: em_latent.csv"

/*
 * Use learned model on validation data
 */
def evidencePartitionValidation = new Partition(4);

for (Predicate p : [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all])
{
	insert = data.getInserter(p, evidencePartitionValidation)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
}

def targetPartitionValidation = new Partition(5);
Database dbValidation = data.getDatabase(targetPartitionValidation, [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all] as Set, evidencePartitionValidation);

def person_uid_list_validation = []
new File( dir + "student_validation.txt" ).eachLine { line ->
	person_uid_list_validation << line
}

def decliner_validation = []
new File( dir + "decline_validation.txt" ).eachLine { line ->
	decliner_validation << line.split('\t')[1]
}

Set<GroundTerm> peopleValidation = new HashSet<GroundTerm>();
for (id in person_uid_list_validation)
	peopleValidation.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(dbValidation);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), peopleValidation);
popMap.put(new Variable("Groups"), group_vals);
dbPop.populate((decline(People)).getFormula(), popMap);
dbPop.populate((group(People, Groups)).getFormula(), popMap);

/*
 * Performs inference
 */
inferenceApp = new MPEInference(m, dbValidation, config);
result = inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's see the results on validation data
 */
def output_file_validation = 'output' + java.io.File.separator + 'em_latent_validation.csv';
fileSuccessfullyDeleted =  new File(output_file_validation).delete()
f = new File(output_file_validation)
f.append("person_uid, predictin, ground\r\n")

def tp = 0.0
def tn = 0.0
def fp = 0.0
def fn = 0.0
def scoring_error = 0.0
def truth = False
def prediction = False

// Find unique values for result probabilities
def uniqueProbas = new HashSet<Double>();
uniqueProbas.add(0) //baseline
for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
{
	uniqueProbas.add(atom.getValue())
}

def uniqueProbasList = uniqueProbas.toArray().sort()

println ( '\n' + uniqueProbasList + '\n' ) 

for ( proba in uniqueProbasList ){
	println ( 'When predicted probability is greater than ' + proba + ' is declared positive: ')
	tp = 0.0
	tn = 0.0
	fp = 0.0
	fn = 0.0
	scoring_error = 0.0
	truth = False
	prediction = False
	i = 0
	for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
	{
		truth = decliner_validation[i].toInteger()
		prediction = atom.getValue()
		if ( ( truth == 1 ) && ( prediction > proba ) ) tp++
		else if ( ( truth == 0 ) && ( prediction <= proba ) ) tn++
		else if ( ( truth == 0 ) && ( prediction > proba ) ) fp++
		else if ( ( truth == 1 ) && ( prediction <= proba ) ) fn++
		else scoring_error++
	//	f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + decliner_validation[i] + "\r\n");
		i++;
	}
	println ('tp = ' + tp)
	println ('tn = ' + tn)
	println ('fp = ' + fp)
	println ('fn = ' + fn)
	println ('scoring_error = ' + scoring_error )
	
	def precision = tp / ( tp + fp + 0.00000001)
	def recall = tp / ( tp + fn + 0.00000001)
	def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
	println ('precision = ' + precision)
	println ('recall = ' + recall)
	println ('fscore = ' + fscore + '\n')
}

//println "Inference results on validation data with learned weights: em_latent_validation.csv"

println 'done.'























