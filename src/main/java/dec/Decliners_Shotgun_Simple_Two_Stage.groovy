package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.em.BernoulliMeanFieldEM
import edu.umd.cs.psl.application.learning.weight.em.HardEM
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * Build an latent variable using expectation maximization.  Make predictions
 * on the validation data for latent var.  Use latent var and others to create
 * a new model and train with MPI to make final dec predictions.
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("Decliners")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "decline")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)

// Instantiate a model
PSLModel m = new PSLModel(this, data)

// Base features predicates
m.add predicate: "student",		types: [ArgumentType.UniqueID]
//m.add predicate: "aleks",		types: [ArgumentType.UniqueID]
//m.add predicate: "prior_term_gpa",		types: [ArgumentType.UniqueID]
//m.add predicate: "hs_core_gpa",		types: [ArgumentType.UniqueID]
//m.add predicate: "attendance_all",		types: [ArgumentType.UniqueID]

//	negative is bad?  so someone on the list is prompt
m.add predicate: "avg_arrival_prompt_rec_avg",		types: [ArgumentType.UniqueID]
//	again, higher is better on the list is good
m.add predicate: "perc_attendance_all_avg",		types: [ArgumentType.UniqueID]
//	same, on list is good
m.add predicate: "avg_arrival_prompt_all_avg",		types: [ArgumentType.UniqueID]
// list is good
m.add predicate: "perc_attendance_rec_avg",		types: [ArgumentType.UniqueID]
// who know, maybe good
m.add predicate: "door_access_prob_hr_of_day_19",		types: [ArgumentType.UniqueID]

// Groups
GroundTerm g0 = data.getUniqueID(2);
GroundTerm g1 = data.getUniqueID(3);
// Latent Variable predicate
m.add predicate: "group",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Logic Rules

// Group 0 - below - We expect these to be decliners
m.add rule : ( student(A) & ~avg_arrival_prompt_rec_avg(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~perc_attendance_all_avg(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~avg_arrival_prompt_all_avg(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~perc_attendance_rec_avg(A) ) >> group(A, g0) , weight : 10
m.add rule : ( student(A) & ~door_access_prob_hr_of_day_19(A) ) >> group(A, g0) , weight : 10

// Group 1 - above
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & perc_attendance_rec_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_19(A) ) >> group(A, g1) , weight : 10

m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) & perc_attendance_all_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) & avg_arrival_prompt_all_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) & perc_attendance_rec_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) & door_access_prob_hr_of_day_19(A) ) >> group(A, g1) , weight : 10

m.add rule : ( student(A) & perc_attendance_all_avg(A) & avg_arrival_prompt_rec_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) & avg_arrival_prompt_all_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) & perc_attendance_rec_avg(A) ) >> group(A, g1) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) & door_access_prob_hr_of_day_19(A) ) >> group(A, g1) , weight : 10

// Promote mutual exclusivity of latent var
m.add rule : group(A, g0) >> ~group(A, g1), weight : 10
m.add rule : group(A, g1) >> ~group(A, g0), weight : 10

// Manually defined model
println 'Manually defined model: \n' + m;

def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'decline'+java.io.File.separator;

for (Predicate p : [student, avg_arrival_prompt_rec_avg, perc_attendance_all_avg, avg_arrival_prompt_all_avg, perc_attendance_rec_avg, door_access_prob_hr_of_day_19])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [student, avg_arrival_prompt_rec_avg, perc_attendance_all_avg, avg_arrival_prompt_all_avg, perc_attendance_rec_avg, door_access_prob_hr_of_day_19] as Set, evidencePartition);

Set<GroundTerm> group_vals = new HashSet<GroundTerm>();
group_vals.add(g0);
group_vals.add(g1);

def person_uid_list = []
new File( dir + "student_train.txt" ).eachLine { line ->
	person_uid_list << line
}

Set<GroundTerm> people = new HashSet<GroundTerm>();
for (id in person_uid_list)
	people.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(db);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
popMap.put(new Variable("Groups"), group_vals);
dbPop.populate((group(People, Groups)).getFormula(), popMap);

Partition trueDataPartition = new Partition(3);
	
Database trueDataDB = data.getDatabase(trueDataPartition, [group] as Set);
dbPop = new DatabasePopulator(trueDataDB);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);

/*
 *  Unsupervised training: Use EM to determine a Group for the training data.
 *  
 *  The assigned Groups will be used in a supervised fashion in a second model.
 *  
 *  The resultant model will later be used on training data to make Group 
 *  assignments.  
 *  
 */
HardEM em = new HardEM(m, db, trueDataDB, config);
//BernouliMeanFieldEM em = new BernoulliMeanFieldEM(m, db, trueDataDB, config);
em.learn();

println m;

/*
 * Use the learned model from unsupervised EM and make a Group assignment on
 * the training data
 */
inferenceApp = new MPEInference(m, db, config);
result = inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's make a file with a list of students who belong to g0 (2)
 */
DecimalFormat formatter = new DecimalFormat("#.##");
def output_file = 'data/decline' + java.io.File.separator + 'assigned_group_train.txt';
boolean fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
def i = 0
for (GroundAtom atom : Queries.getAllAtoms(db, group))
{
	if ( atom.getValue() >= 0.5 && atom.getArguments()[1].toString() == "2" ){
		f.append(atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\r\n");
	}else if ( atom.getValue() >= 0.5 && atom.getArguments()[1].toString() == "3" ){
		f.append(atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\r\n");
	}	
//	else{
//		print atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\t" + atom.getValue() + "\r\n"
//	}
	i++;
}
println "Latent variable predictions of training data based on learned weights from EM: assigned_group_train.txt"

/*
 *  Now that we have the Group latent variable we will 
 *  start fresh.  Build a new model with new rules that
 *  include Group. 
 */

// Instantiate a model
PSLModel m2 = new PSLModel(this, data)

// Base features predicates
m2.add predicate: "student",		types: [ArgumentType.UniqueID]
m2.add predicate: "aleks",		types: [ArgumentType.UniqueID]
m2.add predicate: "prior_term_gpa",		types: [ArgumentType.UniqueID]
m2.add predicate: "hs_core_gpa",		types: [ArgumentType.UniqueID]
m2.add predicate: "attendance_all",		types: [ArgumentType.UniqueID]

// Dependent Variable
m2.add predicate: "decline",		types: [ArgumentType.UniqueID]

// NO LONGER Latent Variable predicate
m2.add predicate: "assigned_group",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]


// Logic Rules

// Group 0 - below - We expect these to be decliners
m2.add rule : ( student(A) & aleks(A) & assigned_group(A, g0) ) >> decline(A) , weight : 10
m2.add rule : ( student(A) & hs_core_gpa(A) & assigned_group(A, g0) ) >> decline(A) , weight : 10
m2.add rule : ( student(A) & ~attendance_all(A) & assigned_group(A, g0) ) >> decline(A) , weight : 10
m2.add rule : ( student(A) & prior_term_gpa(A) & assigned_group(A, g0) ) >> decline(A) , weight : 10

// Group 1 - above
m2.add rule : ( student(A) & aleks(A) ) >> ~decline(A) , weight : 10
m2.add rule : ( student(A) & hs_core_gpa(A) ) >> ~decline(A) , weight : 10
m2.add rule : ( student(A) & aleks(A) &  hs_core_gpa(A)) >> ~decline(A) , weight : 10
m2.add rule : ( student(A) & prior_term_gpa(A) & assigned_group(A, g1) ) >> ~decline(A) , weight : 10


m2.add rule : ( student(A) & prior_term_gpa(A) ) >> decline(A) , weight : 10
m2.add rule : ( assigned_group(A, g0) & ~attendance_all(A) ) >> decline(A) , weight : 10
m2.add rule : ( assigned_group(A, g1) & attendance_all(A) ) >> ~decline(A) , weight : 10
//m2.add rule : assigned_group(A, g0) >> decline(A), weight : 10
//m2.add rule : assigned_group(A, g1) >> ~decline(A), weight : 20

// Promote mutual exclusivity of dep var
m2.add rule : decline(A) >> ~decline(A), weight : 10

def evidencePartition2 = new Partition(4);

for (Predicate p : [student, aleks, prior_term_gpa, hs_core_gpa, attendance_all, assigned_group])
{
	insert = data.getInserter(p, evidencePartition2)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

insert = data.getInserter(decline, evidencePartition2)
InserterUtils.loadDelimitedData(insert, dir+ "decliner_only_train.txt");

def targetPartition2 = new Partition(5);
Database db2 = data.getDatabase(targetPartition2, [student, aleks, prior_term_gpa, hs_core_gpa,  attendance_all, assigned_group, decline] as Set, evidencePartition);

dbPop = new DatabasePopulator(db2);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);

/*
 * Performs inference
*/
MPEInference inferenceLatent = new MPEInference(m2, db2, config);
result = inferenceLatent.mpeInference();
inferenceLatent.close();

println ( m2 )

/*
 * Use learned model on validation data
 */
def evidencePartitionValidation = new Partition(7);

for (Predicate p : [student, avg_arrival_prompt_rec_avg, perc_attendance_all_avg, avg_arrival_prompt_all_avg, perc_attendance_rec_avg, door_access_prob_hr_of_day_19])
{
	insert = data.getInserter(p, evidencePartitionValidation)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
}

def targetPartitionValidation = new Partition(8);
Database dbValidation = data.getDatabase(targetPartitionValidation, [student, avg_arrival_prompt_rec_avg, perc_attendance_all_avg, avg_arrival_prompt_all_avg, perc_attendance_rec_avg, door_access_prob_hr_of_day_19] as Set, evidencePartitionValidation);

def person_uid_list_validation = []
new File( dir + "student_validation.txt" ).eachLine { line ->
	person_uid_list_validation << line
}

Set<GroundTerm> peopleValidation = new HashSet<GroundTerm>();
for (id in person_uid_list_validation)
	peopleValidation.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(dbValidation);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), peopleValidation);
popMap.put(new Variable("Groups"), group_vals);
dbPop.populate((group(People, Groups)).getFormula(), popMap);

/*
 * Performs inference: predict Group on validation data
 */
MPEInference inferenceLatent2 = new MPEInference(m, dbValidation, config);
result = inferenceLatent2.mpeInference();
inferenceLatent2.close();

/*
 * Let's make a file with a list of students who belong to g0 (2) for the validation data
 */
output_file = 'data/decline' + java.io.File.separator + 'assigned_group_validation.txt';
fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
i = 0
for (GroundAtom atom : Queries.getAllAtoms(dbValidation, group))
{
	if ( atom.getValue() >= 0.5 && atom.getArguments()[1].toString() == "2" ){
		f.append(atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\r\n");
	}else if ( atom.getValue() >= 0.5 && atom.getArguments()[1].toString() == "3" ){
		f.append(atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\r\n");
	}	
//	print atom.getArguments()[0].toString() + "\t" + atom.getArguments()[1].toString() + "\t" + atom.getValue() + "\r\n"
	i++;
}
println "Latent variable predictions of validation data based on learned weights from EM: assigned_group_validation.txt"


/*
 * Use learned model on validation data
 * Use m2 to make dec predictions
 */
def evidencePartitionValidation2 = new Partition(9);

for (Predicate p : [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all, assigned_group])
{
	insert = data.getInserter(p, evidencePartitionValidation2)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
}

def targetPartitionValidation2 = new Partition(10);
Database dbValidation2 = data.getDatabase(targetPartitionValidation2, [aleks, prior_term_gpa, hs_core_gpa, student, attendance_all, assigned_group] as Set, evidencePartitionValidation2);

def decliner_validation = []
new File( dir + "decline_validation.txt" ).eachLine { line ->
	decliner_validation << line.split('\t')[1]
}

dbPop = new DatabasePopulator(dbValidation2);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), peopleValidation);
popMap.put(new Variable("Groups"), group_vals);
dbPop.populate((decline(People)).getFormula(), popMap);
dbPop.populate((group(People, Groups)).getFormula(), popMap);

/*
 * Performs inference
 */
MPEInference inferenceApp2 = new MPEInference(m2, dbValidation2, config);
result = inferenceApp2.mpeInference();
inferenceApp2.close();

/*
 * Let's see the results on validation data
 */
def output_file_validation = 'output' + java.io.File.separator + 'em_latent_validation.csv';
fileSuccessfullyDeleted =  new File(output_file_validation).delete()
f = new File(output_file_validation)
f.append("person_uid, predictin, ground\r\n")

def tp = 0.0
def tn = 0.0
def fp = 0.0
def fn = 0.0
def scoring_error = 0.0
def truth = False
def prediction = False

// Find unique values for result probabilities
def uniqueProbas = new HashSet<Double>();
uniqueProbas.add(0) //baseline
for (GroundAtom atom : Queries.getAllAtoms(dbValidation2, decline))
{
	uniqueProbas.add(atom.getValue())
}

def uniqueProbasList = uniqueProbas.toArray().sort()

println ( '\n' + uniqueProbasList + '\n' ) 

for ( proba in uniqueProbasList ){
	println ( 'When predicted probability is greater than ' + proba + ' is declared positive: ')
	tp = 0.0
	tn = 0.0
	fp = 0.0
	fn = 0.0
	scoring_error = 0.0
	truth = False
	prediction = False
	i = 0
	for (GroundAtom atom : Queries.getAllAtoms(dbValidation2, decline))
	{
		truth = decliner_validation[i].toInteger()
		prediction = atom.getValue()
		if ( ( truth == 1 ) && ( prediction > proba ) ) tp++
		else if ( ( truth == 0 ) && ( prediction <= proba ) ) tn++
		else if ( ( truth == 0 ) && ( prediction > proba ) ) fp++
		else if ( ( truth == 1 ) && ( prediction <= proba ) ) fn++
		else scoring_error++
//		f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + decliner_validation[i] + "\r\n");
		i++;
	}
	println ('tp = ' + tp)
	println ('tn = ' + tn)
	println ('fp = ' + fp)
	println ('fn = ' + fn)
	println ('scoring_error = ' + scoring_error )
	
	def precision = tp / ( tp + fp + 0.00000001)
	def recall = tp / ( tp + fn + 0.00000001)
	def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
	println ('precision = ' + precision)
	println ('recall = ' + recall)
	println ('fscore = ' + fscore + '\n')
}

println "Inference results on validation data with learned weights: em_latent_validation.csv"

println 'done.'























