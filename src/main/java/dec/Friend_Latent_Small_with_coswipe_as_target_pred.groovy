package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.LazyMPEInference
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * The first thing we need to do is initialize a ConfigBundle and a DataStore
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("friend_small")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "friend")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)


PSLModel m = new PSLModel(this, data)

// Location features
m.add predicate: "colo",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]
m.add predicate: "coswipe",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Homophily features
//m.add predicate: "residence",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Latent Variables
//m.add predicate: "loc_assoc",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]
m.add predicate: "associated",		types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

// Resident Ground Terms
//GroundTerm resident = data.getUniqueID('R');
//GroundTerm forgien = data.getUniqueID('F');
//GroundTerm nonresident = data.getUniqueID('N');

// Location Rules to Build loc_assoc
m.add rule : colo(A, B) >> associated(A, B) , weight : 10
m.add rule : coswipe(A, B) >> associated(A, B) , weight : 10
m.add rule : ~colo(A, B) >> ~associated(A, B) , weight : 10
//m.add rule : ~coswipe(A, B) >> ~friend(A, B) , weight : 10
m.add rule : (colo(A, B) & coswipe(A, B)) >> associated(A, B) , weight : 10
m.add rule : (colo(A, B) & ~coswipe(A, B)) >> ~associated(A, B) , weight : 10
m.add rule : (~colo(A, B) & coswipe(A, B)) >> ~associated(A, B) , weight : 10
m.add rule : (~colo(A, B) & ~coswipe(A, B)) >> ~associated(A, B) , weight : 10
//m.add rule : loc_assoc(A, B) >> friend(A, B) , weight : 20

// Residency Rules
//m.add rule : ( loc_assoc(A, B) & residence(A, resident) & residence(B, resident)) >> friend(A, B) , weight : 5
//m.add rule : ( loc_assoc(A, B) & residence(A, nonresident) & residence(B, nonresident)) >> friend(A, B) , weight : 5
//m.add rule : ( loc_assoc(A, B) & residence(A, forgien) & residence(B, forgien)) >> friend(A, B) , weight : 5
//m.add rule : ( loc_assoc(A, B) & residence(A, resident) &  ~residence(B, resident)) >> ~friend(A, B) , weight : 5
//m.add rule : ( loc_assoc(A, B) & residence(A, nonresident) &  ~residence(B, nonresident)) >> ~friend(A, B) , weight : 5
//m.add rule : ( loc_assoc(A, B) & residence(A, forgien) &  ~residence(B, forgien)) >> ~friend(A, B) , weight : 5


println m;


def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'raw'+java.io.File.separator+'friend_latent_small'+java.io.File.separator;

for (Predicate p : [colo, coswipe]) //, residence])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [colo, coswipe] as Set, evidencePartition);

/*
 * Next, we want to learn the weights from data. For that, we need to have some
 * evidence data from which we can learn. In our example, that means we need to
 * specify the 'true' alignment, which we now load into another partition.
 */
Partition trueDataPartition = new Partition(2);
insert = data.getInserter(friend, trueDataPartition)
/*
 *  .txt needs three columns!  It's like it wants a label in addition to the data it already has
 *  but the data is actually the label to begin with!
 */
// These are person_uids present in the evidence

InserterUtils.loadDelimitedDataTruth(insert, dir + "friend_truth_from_coswipe.txt");

def person_uid_list = []
new File( dir + "person_uid_train.txt" ).eachLine { line ->
	person_uid_list << line
}

Set<GroundTerm> students = new HashSet<GroundTerm>();
for (id in person_uid_list)
	students.add(data.getUniqueID(id));

Map<Variable, Set<GroundTerm>> popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("Students"), students)

DatabasePopulator dbPop = new DatabasePopulator(db);
dbPop.populate((friend(Students, Students)).getFormula(), popMap);


/*
 * Now, we can learn the weights.
 *
 * We first open a database which contains all the target atoms as observations.
 * We then combine this database with the original database to learn.
 */
Database trueDataDB = data.getDatabase(trueDataPartition, [friend] as Set);
MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(m, db, trueDataDB, config);
weightLearning.learn();
weightLearning.close();

/*
 * Let's have a look at the newly learned weights.
 */
println ""
println "Learned model:"
println m


def testPartition = new Partition(3);

for (Predicate p : [colo, coswipe]) //, residence])
{
	insert = data.getInserter(p, testPartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_test.txt");
}

def testTargetPartition = new Partition(4);
Database db2 = data.getDatabase(testTargetPartition, [colo, coswipe] as Set, testPartition);


LazyMPEInference inferenceApp = new LazyMPEInference(m, db2, config);
inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's see the results
 */
println "Inference results with learned weights:"
DecimalFormat formatter = new DecimalFormat("#.##");
def output_file = 'output' + java.io.File.separator + 'friend_latent.csv';
boolean fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
f.append("A" + "," + "B" + "," + "Friendship Strength" + "\r\n") 

for (GroundAtom atom : Queries.getAllAtoms(db2, friend))
{
	f.append(atom.getArguments()[0].toString() + "," + atom.getArguments()[1].toString() + "," + formatter.format(atom.getValue()) + "\r\n");
}

println 'done.'























