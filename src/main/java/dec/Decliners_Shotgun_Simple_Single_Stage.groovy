package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * Build an latent variable using expectation maximization and tie it to 
 * the dependent variable.  One model.
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("decline")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "declince")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)

// Instantiate a model
PSLModel m = new PSLModel(this, data)

// Base features predicates
m.add predicate: "aleks",        types: [ArgumentType.UniqueID]
m.add predicate: "associates_positivity",        types: [ArgumentType.UniqueID]
m.add predicate: "attendance_all",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_all_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_lab_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_lbp_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_lec_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_pso_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_rec_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_arrival_prompt_sd_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "avg_weekly_rectrac_swipes",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_10",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_11",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_12",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_13",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_14",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_15",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_16",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_17",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_18",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_19",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_20",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_7",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_8",        types: [ArgumentType.UniqueID]
m.add predicate: "brd_pln_dining_usage_prob_hr_of_day_9",        types: [ArgumentType.UniqueID]
m.add predicate: "colo_positivity",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_0",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_1",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_10",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_11",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_12",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_13",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_14",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_15",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_16",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_17",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_18",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_19",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_2",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_20",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_21",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_23",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_3",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_4",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_5",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_7",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_8",        types: [ArgumentType.UniqueID]
m.add predicate: "door_access_prob_hr_of_day_9",        types: [ArgumentType.UniqueID]
m.add predicate: "hs_core_gpa",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_all_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_lab_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_lec_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_pso_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_rec_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "perc_attendance_sd_avg",        types: [ArgumentType.UniqueID]
m.add predicate: "prior_term_gpa",        types: [ArgumentType.UniqueID]
m.add predicate: "rectrac_login_dow_fri",        types: [ArgumentType.UniqueID]
m.add predicate: "rectrac_login_dow_mon",        types: [ArgumentType.UniqueID]
m.add predicate: "rectrac_login_dow_thu",        types: [ArgumentType.UniqueID]
m.add predicate: "rectrac_login_dow_tue",        types: [ArgumentType.UniqueID]
m.add predicate: "rectrac_login_dow_wed",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_dining_usage_prob_hr_of_day_10",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_dining_usage_prob_hr_of_day_11",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_dining_usage_prob_hr_of_day_18",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_dining_usage_prob_hr_of_day_19",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_dining_usage_prob_hr_of_day_9",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_10",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_11",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_13",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_14",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_15",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_16",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_18",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_19",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_21",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_22",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_23",        types: [ArgumentType.UniqueID]
m.add predicate: "strd_val_tran_laundry_usage_prob_hr_of_day_9",        types: [ArgumentType.UniqueID]

// Dependent Variable
m.add predicate: "decline",		types: [ArgumentType.UniqueID]

// Is a student
m.add predicate: "student",        types: [ArgumentType.UniqueID]

// Logic Rules
m.add rule : ( student(A) & aleks(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & aleks(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & associates_positivity(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & associates_positivity(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & attendance_all(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & attendance_all(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lab_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lab_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lbp_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lbp_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lec_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_lec_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_pso_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_pso_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_rec_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_sd_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_arrival_prompt_sd_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & avg_weekly_rectrac_swipes(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & avg_weekly_rectrac_swipes(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_10(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_11(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_12(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_12(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_13(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_13(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_14(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_14(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_15(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_15(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_16(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_16(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_17(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_17(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_18(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_19(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_20(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_20(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_7(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_7(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_8(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_8(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & brd_pln_dining_usage_prob_hr_of_day_9(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & colo_positivity(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & colo_positivity(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_0(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_0(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_1(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_1(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_10(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_11(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_12(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_12(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_13(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_13(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_14(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_14(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_15(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_15(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_16(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_16(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_17(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_17(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_18(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_19(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_2(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_2(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_20(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_20(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_21(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_21(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_23(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_23(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_3(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_3(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_4(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_4(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_5(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_5(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_7(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_7(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_8(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_8(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & door_access_prob_hr_of_day_9(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & hs_core_gpa(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & hs_core_gpa(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_all_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_lab_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_lab_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_lec_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_lec_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_pso_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_pso_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_rec_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_rec_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_sd_avg(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & perc_attendance_sd_avg(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & prior_term_gpa(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & prior_term_gpa(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_fri(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_fri(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_mon(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_mon(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_thu(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_thu(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_tue(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_tue(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_wed(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & rectrac_login_dow_wed(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_10(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_11(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_18(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_19(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_dining_usage_prob_hr_of_day_9(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_10(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_10(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_11(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_11(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_13(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_13(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_14(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_14(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_15(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_15(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_16(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_16(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_18(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_18(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_19(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_19(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_21(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_21(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_22(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_22(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_23(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_23(A) ) >> ~decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_9(A) ) >> decline(A) , weight : 10
m.add rule : ( student(A) & strd_val_tran_laundry_usage_prob_hr_of_day_9(A) ) >> ~decline(A) , weight : 10

// Promote mutual exclusivity of dep var
m.add rule : decline(A) >> ~decline(A), constraint: true
m.add rule : ( student(A) & ~decline(A) ) >> decline(A), constraint: true
m.add rule : student(A) >> ~decline(A), weight : 10

// Manually defined model
println 'Manually defined model: \n' + m;

def evidencePartition = new Partition(0);
def dir = 'data'+java.io.File.separator+'decline'+java.io.File.separator;

for (Predicate p : [aleks, 
associates_positivity, 
attendance_all, 
avg_arrival_prompt_all_avg, 
avg_arrival_prompt_lab_avg, 
avg_arrival_prompt_lbp_avg, 
avg_arrival_prompt_lec_avg, 
avg_arrival_prompt_pso_avg, 
avg_arrival_prompt_rec_avg, 
avg_arrival_prompt_sd_avg, 
avg_weekly_rectrac_swipes, 
brd_pln_dining_usage_prob_hr_of_day_10, 
brd_pln_dining_usage_prob_hr_of_day_11, 
brd_pln_dining_usage_prob_hr_of_day_12, 
brd_pln_dining_usage_prob_hr_of_day_13, 
brd_pln_dining_usage_prob_hr_of_day_14, 
brd_pln_dining_usage_prob_hr_of_day_15, 
brd_pln_dining_usage_prob_hr_of_day_16, 
brd_pln_dining_usage_prob_hr_of_day_17, 
brd_pln_dining_usage_prob_hr_of_day_18, 
brd_pln_dining_usage_prob_hr_of_day_19, 
brd_pln_dining_usage_prob_hr_of_day_20, 
brd_pln_dining_usage_prob_hr_of_day_7, 
brd_pln_dining_usage_prob_hr_of_day_8, 
brd_pln_dining_usage_prob_hr_of_day_9, 
colo_positivity, 
door_access_prob_hr_of_day_0, 
door_access_prob_hr_of_day_1, 
door_access_prob_hr_of_day_10, 
door_access_prob_hr_of_day_11, 
door_access_prob_hr_of_day_12, 
door_access_prob_hr_of_day_13, 
door_access_prob_hr_of_day_14, 
door_access_prob_hr_of_day_15, 
door_access_prob_hr_of_day_16, 
door_access_prob_hr_of_day_17, 
door_access_prob_hr_of_day_18, 
door_access_prob_hr_of_day_19, 
door_access_prob_hr_of_day_2, 
door_access_prob_hr_of_day_20, 
door_access_prob_hr_of_day_21, 
door_access_prob_hr_of_day_23, 
door_access_prob_hr_of_day_3, 
door_access_prob_hr_of_day_4, 
door_access_prob_hr_of_day_5, 
door_access_prob_hr_of_day_7, 
door_access_prob_hr_of_day_8, 
door_access_prob_hr_of_day_9, 
hs_core_gpa, 
perc_attendance_all_avg, 
perc_attendance_lab_avg, 
perc_attendance_lec_avg, 
perc_attendance_pso_avg, 
perc_attendance_rec_avg, 
perc_attendance_sd_avg, 
prior_term_gpa, 
rectrac_login_dow_fri, 
rectrac_login_dow_mon, 
rectrac_login_dow_thu, 
rectrac_login_dow_tue, 
rectrac_login_dow_wed, 
strd_val_tran_dining_usage_prob_hr_of_day_10, 
strd_val_tran_dining_usage_prob_hr_of_day_11, 
strd_val_tran_dining_usage_prob_hr_of_day_18, 
strd_val_tran_dining_usage_prob_hr_of_day_19, 
strd_val_tran_dining_usage_prob_hr_of_day_9, 
strd_val_tran_laundry_usage_prob_hr_of_day_10, 
strd_val_tran_laundry_usage_prob_hr_of_day_11, 
strd_val_tran_laundry_usage_prob_hr_of_day_13, 
strd_val_tran_laundry_usage_prob_hr_of_day_14, 
strd_val_tran_laundry_usage_prob_hr_of_day_15, 
strd_val_tran_laundry_usage_prob_hr_of_day_16, 
strd_val_tran_laundry_usage_prob_hr_of_day_18, 
strd_val_tran_laundry_usage_prob_hr_of_day_19, 
strd_val_tran_laundry_usage_prob_hr_of_day_21, 
strd_val_tran_laundry_usage_prob_hr_of_day_22, 
strd_val_tran_laundry_usage_prob_hr_of_day_23, 
strd_val_tran_laundry_usage_prob_hr_of_day_9,
student])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [aleks, 
associates_positivity, 
attendance_all, 
avg_arrival_prompt_all_avg, 
avg_arrival_prompt_lab_avg, 
avg_arrival_prompt_lbp_avg, 
avg_arrival_prompt_lec_avg, 
avg_arrival_prompt_pso_avg, 
avg_arrival_prompt_rec_avg, 
avg_arrival_prompt_sd_avg, 
avg_weekly_rectrac_swipes, 
brd_pln_dining_usage_prob_hr_of_day_10, 
brd_pln_dining_usage_prob_hr_of_day_11, 
brd_pln_dining_usage_prob_hr_of_day_12, 
brd_pln_dining_usage_prob_hr_of_day_13, 
brd_pln_dining_usage_prob_hr_of_day_14, 
brd_pln_dining_usage_prob_hr_of_day_15, 
brd_pln_dining_usage_prob_hr_of_day_16, 
brd_pln_dining_usage_prob_hr_of_day_17, 
brd_pln_dining_usage_prob_hr_of_day_18, 
brd_pln_dining_usage_prob_hr_of_day_19, 
brd_pln_dining_usage_prob_hr_of_day_20, 
brd_pln_dining_usage_prob_hr_of_day_7, 
brd_pln_dining_usage_prob_hr_of_day_8, 
brd_pln_dining_usage_prob_hr_of_day_9, 
colo_positivity, 
door_access_prob_hr_of_day_0, 
door_access_prob_hr_of_day_1, 
door_access_prob_hr_of_day_10, 
door_access_prob_hr_of_day_11, 
door_access_prob_hr_of_day_12, 
door_access_prob_hr_of_day_13, 
door_access_prob_hr_of_day_14, 
door_access_prob_hr_of_day_15, 
door_access_prob_hr_of_day_16, 
door_access_prob_hr_of_day_17, 
door_access_prob_hr_of_day_18, 
door_access_prob_hr_of_day_19, 
door_access_prob_hr_of_day_2, 
door_access_prob_hr_of_day_20, 
door_access_prob_hr_of_day_21, 
door_access_prob_hr_of_day_23, 
door_access_prob_hr_of_day_3, 
door_access_prob_hr_of_day_4, 
door_access_prob_hr_of_day_5, 
door_access_prob_hr_of_day_7, 
door_access_prob_hr_of_day_8, 
door_access_prob_hr_of_day_9, 
hs_core_gpa, 
perc_attendance_all_avg, 
perc_attendance_lab_avg, 
perc_attendance_lec_avg, 
perc_attendance_pso_avg, 
perc_attendance_rec_avg, 
perc_attendance_sd_avg, 
prior_term_gpa, 
rectrac_login_dow_fri, 
rectrac_login_dow_mon, 
rectrac_login_dow_thu, 
rectrac_login_dow_tue, 
rectrac_login_dow_wed, 
strd_val_tran_dining_usage_prob_hr_of_day_10, 
strd_val_tran_dining_usage_prob_hr_of_day_11, 
strd_val_tran_dining_usage_prob_hr_of_day_18, 
strd_val_tran_dining_usage_prob_hr_of_day_19, 
strd_val_tran_dining_usage_prob_hr_of_day_9, 
strd_val_tran_laundry_usage_prob_hr_of_day_10, 
strd_val_tran_laundry_usage_prob_hr_of_day_11, 
strd_val_tran_laundry_usage_prob_hr_of_day_13, 
strd_val_tran_laundry_usage_prob_hr_of_day_14, 
strd_val_tran_laundry_usage_prob_hr_of_day_15, 
strd_val_tran_laundry_usage_prob_hr_of_day_16, 
strd_val_tran_laundry_usage_prob_hr_of_day_18, 
strd_val_tran_laundry_usage_prob_hr_of_day_19, 
strd_val_tran_laundry_usage_prob_hr_of_day_21, 
strd_val_tran_laundry_usage_prob_hr_of_day_22, 
strd_val_tran_laundry_usage_prob_hr_of_day_23, 
strd_val_tran_laundry_usage_prob_hr_of_day_9,
student] as Set, evidencePartition);

def person_uid_list = []   
new File( dir + "student_train.txt" ).eachLine { line ->
	person_uid_list << line
}

Set<GroundTerm> people = new HashSet<GroundTerm>();
for (id in person_uid_list)
	people.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(db);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);
	

Partition trueDataPartition = new Partition(3);
insert = data.getInserter(decline, trueDataPartition)
//InserterUtils.loadDelimitedDataTruth(insert, dir + "decline_train.txt");
InserterUtils.loadDelimitedData(insert, dir + "decliner_only_train.txt");

Database trueDataDB = data.getDatabase(trueDataPartition, [decline] as Set);
dbPop = new DatabasePopulator(trueDataDB);
popMap = new HashMap<Variable, Set<GroundTerm>>();	
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);

MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(m, db, trueDataDB, config);
weightLearning.learn();
weightLearning.close();

println m;

/*
 * Let's see the results
 */
DecimalFormat formatter = new DecimalFormat("#.##");
def output_file = 'output' + java.io.File.separator + 'shotgun_simple_single.csv';
boolean fileSuccessfullyDeleted =  new File(output_file).delete()
f = new File(output_file)
f.append("person_uid, predictin, ground\r\n") 

def decliner_train = []
new File( dir + "decline_train.txt" ).eachLine { line ->
	decliner_train << line.split('\t')[1]
}

def i = 0
for (GroundAtom atom : Queries.getAllAtoms(db, decline))
{
	f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + decliner_train[i] + "\r\n");
	i++;
}
println "Inference results with learned weights: shotgun_simple_single_validation.csv"

/*
 * Use learned model on validation data
 */
def evidencePartitionValidation = new Partition(4);

for (Predicate p : [aleks, 
associates_positivity, 
attendance_all, 
avg_arrival_prompt_all_avg, 
avg_arrival_prompt_lab_avg, 
avg_arrival_prompt_lbp_avg, 
avg_arrival_prompt_lec_avg, 
avg_arrival_prompt_pso_avg, 
avg_arrival_prompt_rec_avg, 
avg_arrival_prompt_sd_avg, 
avg_weekly_rectrac_swipes, 
brd_pln_dining_usage_prob_hr_of_day_10, 
brd_pln_dining_usage_prob_hr_of_day_11, 
brd_pln_dining_usage_prob_hr_of_day_12, 
brd_pln_dining_usage_prob_hr_of_day_13, 
brd_pln_dining_usage_prob_hr_of_day_14, 
brd_pln_dining_usage_prob_hr_of_day_15, 
brd_pln_dining_usage_prob_hr_of_day_16, 
brd_pln_dining_usage_prob_hr_of_day_17, 
brd_pln_dining_usage_prob_hr_of_day_18, 
brd_pln_dining_usage_prob_hr_of_day_19, 
brd_pln_dining_usage_prob_hr_of_day_20, 
brd_pln_dining_usage_prob_hr_of_day_7, 
brd_pln_dining_usage_prob_hr_of_day_8, 
brd_pln_dining_usage_prob_hr_of_day_9, 
colo_positivity, 
door_access_prob_hr_of_day_0, 
door_access_prob_hr_of_day_1, 
door_access_prob_hr_of_day_10, 
door_access_prob_hr_of_day_11, 
door_access_prob_hr_of_day_12, 
door_access_prob_hr_of_day_13, 
door_access_prob_hr_of_day_14, 
door_access_prob_hr_of_day_15, 
door_access_prob_hr_of_day_16, 
door_access_prob_hr_of_day_17, 
door_access_prob_hr_of_day_18, 
door_access_prob_hr_of_day_19, 
door_access_prob_hr_of_day_2, 
door_access_prob_hr_of_day_20, 
door_access_prob_hr_of_day_21, 
door_access_prob_hr_of_day_23, 
door_access_prob_hr_of_day_3, 
door_access_prob_hr_of_day_4, 
door_access_prob_hr_of_day_5, 
door_access_prob_hr_of_day_7, 
door_access_prob_hr_of_day_8, 
door_access_prob_hr_of_day_9, 
hs_core_gpa, 
perc_attendance_all_avg, 
perc_attendance_lab_avg, 
perc_attendance_lec_avg, 
perc_attendance_pso_avg, 
perc_attendance_rec_avg, 
perc_attendance_sd_avg, 
prior_term_gpa, 
rectrac_login_dow_fri, 
rectrac_login_dow_mon, 
rectrac_login_dow_thu, 
rectrac_login_dow_tue, 
rectrac_login_dow_wed, 
strd_val_tran_dining_usage_prob_hr_of_day_10, 
strd_val_tran_dining_usage_prob_hr_of_day_11, 
strd_val_tran_dining_usage_prob_hr_of_day_18, 
strd_val_tran_dining_usage_prob_hr_of_day_19, 
strd_val_tran_dining_usage_prob_hr_of_day_9, 
strd_val_tran_laundry_usage_prob_hr_of_day_10, 
strd_val_tran_laundry_usage_prob_hr_of_day_11, 
strd_val_tran_laundry_usage_prob_hr_of_day_13, 
strd_val_tran_laundry_usage_prob_hr_of_day_14, 
strd_val_tran_laundry_usage_prob_hr_of_day_15, 
strd_val_tran_laundry_usage_prob_hr_of_day_16, 
strd_val_tran_laundry_usage_prob_hr_of_day_18, 
strd_val_tran_laundry_usage_prob_hr_of_day_19, 
strd_val_tran_laundry_usage_prob_hr_of_day_21, 
strd_val_tran_laundry_usage_prob_hr_of_day_22, 
strd_val_tran_laundry_usage_prob_hr_of_day_23, 
strd_val_tran_laundry_usage_prob_hr_of_day_9,
student])
{
	insert = data.getInserter(p, evidencePartitionValidation)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
}

def targetPartitionValidation = new Partition(5);
Database dbValidation = data.getDatabase(targetPartitionValidation, [aleks, 
associates_positivity, 
attendance_all, 
avg_arrival_prompt_all_avg, 
avg_arrival_prompt_lab_avg, 
avg_arrival_prompt_lbp_avg, 
avg_arrival_prompt_lec_avg, 
avg_arrival_prompt_pso_avg, 
avg_arrival_prompt_rec_avg, 
avg_arrival_prompt_sd_avg, 
avg_weekly_rectrac_swipes, 
brd_pln_dining_usage_prob_hr_of_day_10, 
brd_pln_dining_usage_prob_hr_of_day_11, 
brd_pln_dining_usage_prob_hr_of_day_12, 
brd_pln_dining_usage_prob_hr_of_day_13, 
brd_pln_dining_usage_prob_hr_of_day_14, 
brd_pln_dining_usage_prob_hr_of_day_15, 
brd_pln_dining_usage_prob_hr_of_day_16, 
brd_pln_dining_usage_prob_hr_of_day_17, 
brd_pln_dining_usage_prob_hr_of_day_18, 
brd_pln_dining_usage_prob_hr_of_day_19, 
brd_pln_dining_usage_prob_hr_of_day_20, 
brd_pln_dining_usage_prob_hr_of_day_7, 
brd_pln_dining_usage_prob_hr_of_day_8, 
brd_pln_dining_usage_prob_hr_of_day_9, 
colo_positivity, 
door_access_prob_hr_of_day_0, 
door_access_prob_hr_of_day_1, 
door_access_prob_hr_of_day_10, 
door_access_prob_hr_of_day_11, 
door_access_prob_hr_of_day_12, 
door_access_prob_hr_of_day_13, 
door_access_prob_hr_of_day_14, 
door_access_prob_hr_of_day_15, 
door_access_prob_hr_of_day_16, 
door_access_prob_hr_of_day_17, 
door_access_prob_hr_of_day_18, 
door_access_prob_hr_of_day_19, 
door_access_prob_hr_of_day_2, 
door_access_prob_hr_of_day_20, 
door_access_prob_hr_of_day_21, 
door_access_prob_hr_of_day_23, 
door_access_prob_hr_of_day_3, 
door_access_prob_hr_of_day_4, 
door_access_prob_hr_of_day_5, 
door_access_prob_hr_of_day_7, 
door_access_prob_hr_of_day_8, 
door_access_prob_hr_of_day_9, 
hs_core_gpa, 
perc_attendance_all_avg, 
perc_attendance_lab_avg, 
perc_attendance_lec_avg, 
perc_attendance_pso_avg, 
perc_attendance_rec_avg, 
perc_attendance_sd_avg, 
prior_term_gpa, 
rectrac_login_dow_fri, 
rectrac_login_dow_mon, 
rectrac_login_dow_thu, 
rectrac_login_dow_tue, 
rectrac_login_dow_wed, 
strd_val_tran_dining_usage_prob_hr_of_day_10, 
strd_val_tran_dining_usage_prob_hr_of_day_11, 
strd_val_tran_dining_usage_prob_hr_of_day_18, 
strd_val_tran_dining_usage_prob_hr_of_day_19, 
strd_val_tran_dining_usage_prob_hr_of_day_9, 
strd_val_tran_laundry_usage_prob_hr_of_day_10, 
strd_val_tran_laundry_usage_prob_hr_of_day_11, 
strd_val_tran_laundry_usage_prob_hr_of_day_13, 
strd_val_tran_laundry_usage_prob_hr_of_day_14, 
strd_val_tran_laundry_usage_prob_hr_of_day_15, 
strd_val_tran_laundry_usage_prob_hr_of_day_16, 
strd_val_tran_laundry_usage_prob_hr_of_day_18, 
strd_val_tran_laundry_usage_prob_hr_of_day_19, 
strd_val_tran_laundry_usage_prob_hr_of_day_21, 
strd_val_tran_laundry_usage_prob_hr_of_day_22, 
strd_val_tran_laundry_usage_prob_hr_of_day_23, 
strd_val_tran_laundry_usage_prob_hr_of_day_9,
student] as Set, evidencePartitionValidation);

def person_uid_list_validation = []
new File( dir + "student_validation.txt" ).eachLine { line ->
	person_uid_list_validation << line
}

def decliner_validation = []
new File( dir + "decline_validation.txt" ).eachLine { line ->
	decliner_validation << line.split('\t')[1]
}

Set<GroundTerm> peopleValidation = new HashSet<GroundTerm>();
for (id in person_uid_list_validation)
	peopleValidation.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(dbValidation);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), peopleValidation);
dbPop.populate((decline(People)).getFormula(), popMap);

/*
 * Performs inference
 */
MPEInference inferenceApp2 = new MPEInference(m, dbValidation, config);
result = inferenceApp2.mpeInference();
inferenceApp2.close();

/*
 * Let's see the results on validation data
 */
def output_file_validation = 'output' + java.io.File.separator + 'shotgun_simple_single_validation.csv';
fileSuccessfullyDeleted =  new File(output_file_validation).delete()
f = new File(output_file_validation)
f.append("person_uid, predictin, ground\r\n")

def tp = 0.0
def tn = 0.0
def fp = 0.0
def fn = 0.0
def scoring_error = 0.0
def truth = False
def prediction = False

// Find unique values for result probabilities
def uniqueProbas = new HashSet<Double>();
uniqueProbas.add(0) //baseline
for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
{
	uniqueProbas.add(atom.getValue())
}

def uniqueProbasList = uniqueProbas.toArray().sort()

println ( '\n' + uniqueProbasList + '\n' ) 

for ( proba in uniqueProbasList ){
	println ( 'When predicted probability is greater than ' + proba + ' is declared positive: ')
	tp = 0.0
	tn = 0.0
	fp = 0.0
	fn = 0.0
	scoring_error = 0.0
	truth = False
	prediction = False
	i = 0
	for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
	{
		truth = decliner_validation[i].toInteger()
		prediction = atom.getValue()
		if ( ( truth == 1 ) && ( prediction > proba ) ) tp++
		else if ( ( truth == 0 ) && ( prediction <= proba ) ) tn++
		else if ( ( truth == 0 ) && ( prediction > proba ) ) fp++
		else if ( ( truth == 1 ) && ( prediction <= proba ) ) fn++
		else scoring_error++
		f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + decliner_validation[i] + "\r\n");
		i++;
	}
	println ('tp = ' + tp)
	println ('tn = ' + tn)
	println ('fp = ' + fp)
	println ('fn = ' + fn)
	println ('scoring_error = ' + scoring_error )
	
	def precision = tp / ( tp + fp + 0.00000001)
	def recall = tp / ( tp + fn + 0.00000001)
	def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
	println ('precision = ' + precision)
	println ('recall = ' + recall)
	println ('fscore = ' + fscore + '\n')
}

//println "Inference results on validation data with learned weights: em_latent_validation.csv"

println 'done.'























