package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/*
 * Build an latent variable using expectation maximization and tie it to 
 * the dependent variable.  One model.
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("decline")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "decline")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)

// Instantiate a model
PSLModel m = new PSLModel(this, data)

// Base features predicates
m.add predicate: "cheating",        types: [ArgumentType.UniqueID]

// Dependent Variable
m.add predicate: "decline",		types: [ArgumentType.UniqueID]
m.add predicate: "not_decline",		types: [ArgumentType.UniqueID]

// Is a student
m.add predicate: "student",        types: [ArgumentType.UniqueID]

// Logic Rules

// Cheating
m.add rule : ( student(A) & cheating(A) ) >> decline(A) , weight : 10
//m.add rule : ( student(A) & ~cheating(A) ) >> not_decline(A) , weight : 10

// Prior 
m.add rule : student(A) >> ~decline(A), weight : 20
//m.add rule : student(A) >> ~decline(A), weight : 10

// Promote mutual exclusivity of dep var
//m.add rule : ( student(A) & ~decline(A) ) >> not_decline(A), constraint: true
//m.add rule : ( student(A) & not_decline(A) ) >> ~decline(A), constraint: true

// Manually defined model
println 'Manually defined model: \n' + m;

def trainObservations = new Partition(0);
def dir = 'data'+java.io.File.separator+'decline'+java.io.File.separator;

for (Predicate p : [
					cheating
					, student
					])
{
	insert = data.getInserter(p, trainObservations)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
}

def trainPredictions = new Partition(1);
Database db = data.getDatabase(trainPredictions, [
												cheating
												, student
												] as Set, trainObservations);

def person_uid_list = []   
new File( dir + "student_train.txt" ).eachLine { line ->
	person_uid_list << line
}

Set<GroundTerm> people = new HashSet<GroundTerm>();
for (id in person_uid_list)
	people.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(db);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);
//dbPop.populate((not_decline(People)).getFormula(), popMap);


Partition truth = new Partition(3);
insert = data.getInserter(decline, truth)
InserterUtils.loadDelimitedDataTruth(insert, dir + "decline_train.txt");
//insert = data.getInserter(not_decline, truth)
//InserterUtils.loadDelimitedDataTruth(insert, dir + "not_decline_train_just_p.txt");

Database truthDB = data.getDatabase(truth, [decline] as Set);
dbPop = new DatabasePopulator(truthDB);
//popMap = new HashMap<Variable, Set<GroundTerm>>();	
//popMap.put(new Variable("People"), people);
//dbPop.populate((decline(People)).getFormula(), popMap);

//HardEM weightLearning = new HardEM(m, db, trueDataDB, config);
//weightLearning.learn();
//weightLearning.close();

MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(m, db, truthDB, config);
weightLearning.learn();
weightLearning.close();

println m;

/*
 * Let's see the results
 */
DecimalFormat formatter = new DecimalFormat("#.##");

def decliner_train_map = [:]
new File( dir + "decline_train.txt" ).eachLine { line ->
	decliner_train_map[line.split('\t')[0]] = line.split('\t')[1]
}

println "Inference results with learned weights: shotgun_simple_single_training.csv"

/*
 * Let's see the results on validation data
 */
def output_file_validation = 'output' + java.io.File.separator + 'shotgun_simple_single_training.csv';
fileSuccessfullyDeleted =  new File(output_file_validation).delete()
f = new File(output_file_validation)
f.append("person_uid, predictin, ground\r\n")

def tp = 0.0
def tn = 0.0
def fp = 0.0
def fn = 0.0
def scoring_error = 0.0
def actual = False
def prediction = False

// Find unique values for result probabilities
def uniqueProbas = new HashSet<Double>();
uniqueProbas.add(0) //baseline
for (GroundAtom atom : Queries.getAllAtoms(db, decline))
{
	uniqueProbas.add(atom.getValue())
}

def uniqueProbasList = uniqueProbas.toArray().sort()

println ( '\n' + uniqueProbasList + '\n' ) 

for ( proba in uniqueProbasList ){
	println ( 'Training Data: When predicted probability is greater than ' + proba + ' is declared positive: ')
	tp = 0.0
	tn = 0.0
	fp = 0.0
	fn = 0.0
	scoring_error = 0.0
	actual = False
	prediction = False
	i = 0
	for (GroundAtom atom : Queries.getAllAtoms(db, decline))
	{
		actual = decliner_train_map.get(atom.getArguments()[0].toString()).toString().toInteger();
		prediction = atom.getValue()
		if ( ( actual == 1 ) && ( prediction >= proba ) ) tp++
		else if ( ( actual == 0 ) && ( prediction < proba ) ) tn++
		else if ( ( actual == 0 ) && ( prediction >= proba ) ) fp++
		else if ( ( actual == 1 ) && ( prediction < proba ) ) fn++
		else scoring_error++
		f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + actual + "\r\n");
		i++;
	}
	println ('tp = ' + tp)
	println ('tn = ' + tn)
	println ('fp = ' + fp)
	println ('fn = ' + fn)
	println ('scoring_error = ' + scoring_error )
	
	def precision = tp / ( tp + fp + 0.00000001)
	def recall = tp / ( tp + fn + 0.00000001)
	def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
	println ('precision = ' + precision)
	println ('recall = ' + recall)
	println ('fscore = ' + fscore + '\n')
}



println "Inference results with learned weights: shotgun_simple_single_validation.csv"
/*
 * Use learned model on validation data
 */
def validationObservations = new Partition(4);

for (Predicate p : [
					cheating
					, student
					])
{
	insert = data.getInserter(p, validationObservations)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
}

def validationPredictions = new Partition(5);
Database dbValidation = data.getDatabase(validationPredictions, [
												cheating
												, student
												] as Set, validationObservations);

person_uid_list = []
new File( dir + "student_validation.txt" ).eachLine { line ->
	person_uid_list << line
}

people = new HashSet<GroundTerm>();
for (id in person_uid_list)
	people.add(data.getUniqueID(id));

dbPop = new DatabasePopulator(dbValidation);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("People"), people);
dbPop.populate((decline(People)).getFormula(), popMap);

/*
 * Performs inference
 */
MPEInference inferenceApp2 = new MPEInference(m, dbValidation, config);
result = inferenceApp2.mpeInference();
inferenceApp2.close();

/*
 * Let's see the results on validation data
 */

def decliner_validation_map = [:]
new File( dir + "decline_validation.txt" ).eachLine { line ->
	decliner_validation_map[line.split('\t')[0]] = line.split('\t')[1]
}

output_file_validation = 'output' + java.io.File.separator + 'shotgun_simple_single_validation.csv';
fileSuccessfullyDeleted =  new File(output_file_validation).delete()
f = new File(output_file_validation)
f.append("person_uid, predictin, ground\r\n")

tp = 0.0
tn = 0.0
fp = 0.0
fn = 0.0
scoring_error = 0.0
actual = False
prediction = False

// Find unique values for result probabilities
uniqueProbas = new HashSet<Double>();
uniqueProbas.add(0) //baseline
for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
{
	uniqueProbas.add(atom.getValue())
}

uniqueProbasList = uniqueProbas.toArray().sort()

println ( '\n' + uniqueProbasList + '\n' )

for ( proba in uniqueProbasList ){
	println ( 'Validation Data: When predicted probability is greater than ' + proba + ' is declared positive: ')
	tp = 0.0
	tn = 0.0
	fp = 0.0
	fn = 0.0
	scoring_error = 0.0
	actual = False
	prediction = False
	i = 0
	for (GroundAtom atom : Queries.getAllAtoms(dbValidation, decline))
	{
		actual = decliner_validation_map.get(atom.getArguments()[0].toString()).toString().toInteger();
		prediction = atom.getValue()
		if ( ( actual == 1 ) && ( prediction > proba ) ) tp++
		else if ( ( actual == 0 ) && ( prediction <= proba ) ) tn++
		else if ( ( actual == 0 ) && ( prediction > proba ) ) fp++
		else if ( ( actual == 1 ) && ( prediction <= proba ) ) fn++
		else scoring_error++
		f.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + actual + "\r\n");
		i++;
	}
	println ('tp = ' + tp)
	println ('tn = ' + tn)
	println ('fp = ' + fp)
	println ('fn = ' + fn)
	println ('scoring_error = ' + scoring_error )
	
	def precision = tp / ( tp + fp + 0.00000001)
	def recall = tp / ( tp + fn + 0.00000001)
	def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
	println ('precision = ' + precision)
	println ('recall = ' + recall)
	println ('fscore = ' + fscore + '\n')
}

println 'done.'























