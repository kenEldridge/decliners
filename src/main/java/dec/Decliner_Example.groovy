package dec

import java.text.DecimalFormat

import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.LazyMaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.atom.GroundAtom
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

/* 
 * The first thing we need to do is initialize a ConfigBundle and a DataStore
 */

/*
 * A ConfigBundle is a set of key-value pairs containing configuration options.
 * One place these can be defined is in psl-example/src/main/resources/psl.properties
 */
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("decliner_simple")

/* Uses H2 as a DataStore and stores it in a temp. directory by default */
def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "decliner")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)

/*
 * Now we can initialize a PSLModel, which is the core component of PSL.
 * The first constructor argument is the context in which the PSLModel is defined.
 * The second argument is the DataStore we will be using.
 */
PSLModel m = new PSLModel(this, data)

///*
// * In this example program, the task is to align two social networks, by
// * identifying which pairs of users are the same across networks.
// */
//
///* 
// * We create four predicates in the model, giving their names and list of argument types
// */
m.add predicate: "dec_attr1",		types: [ArgumentType.UniqueID, ArgumentType.Double]
m.add predicate: "dec_attr2",		types: [ArgumentType.UniqueID, ArgumentType.Double]
m.add predicate: "dec_dep", 	types: [ArgumentType.UniqueID, ArgumentType.UniqueID]

/*
 * Now, we define a string similarity function bound to a predicate.
 * Note that we can use any implementation of ExternalFunction that acts on two strings!
 */
//m.add function: "SameName" , implementation: new LevenshteinSimilarity()
/* Also, try: new MyStringSimilarity(), see end of file */

/* 
 * Having added all the predicates we need to represent our problem, we finally
 * add some rules into the model. Rules are defined using a logical syntax.
 * 
 * Uppercase letters are variables and the predicates used in the rules below
 * are those defined above. The character '&' denotes a conjunction where '>>'
 * denotes an implication.
 * 
 * Each rule is given a weight that is either the weight used for inference or
 * an initial guess for the starting point of weight learning.
 */

/*
 * We also create constants to refer to each social network.
 */
GroundTerm decliner = data.getUniqueID(1);
GroundTerm not_decliner = data.getUniqueID(1);

/*
 * Our first rule says that users with similar names are likely the same person
 */
//m.add rule : ( Network(A, snA) & Network(B, snB) & Name(A,X) & Name(B,Y)
//	& SameName(X,Y) ) >> SamePerson(A,B),  weight : 5

/* 
 * In this rule, we use the social network to propagate SamePerson information.
 */
m.add rule : ( dec_attr1(A, X) ) >> dec_dep(A, decliner) , weight : 10
m.add rule : ( dec_attr1(B, Y) ) >> dec_dep(B, decliner) , weight : 10
m.add rule : ( dec_dep(E, decliner) ) >> ~dec_dep(F, not_decliner) , weight : 10

/* 
 * Next, we define some constraints for our model. In this case, we restrict that
 * each person can be aligned to at most one other person in the other social network.
 * To do so, we define two partial functional constraints where the latter is on
 * the inverse. We also say that samePerson must be symmetric,
 * i.e., samePerson(p1, p2) == samePerson(p2, p1).
 */
//m.add PredicateConstraint.PartialFunctional, on : SamePerson
//m.add PredicateConstraint.PartialInverseFunctional, on : SamePerson
//m.add PredicateConstraint.Symmetric, on : SamePerson

/*
 * Finally, we define a prior on the inference predicate samePerson. It says that
 * we should assume two people are not the samePerson with some weight. This can
 * be overridden with evidence as defined in the previous rules.
 */
//m.add rule: ~SamePerson(A,B), weight: 1

/*
 * Let's see what our model looks like.
 */
println m;

/* 
 * We now insert data into our DataStore. All data is stored in a partition.
 * We put all the observations into their own partition.
 * 
 * We can use insertion helpers for a specified predicate. Here we show how one
 * can manually insert data or use the insertion helpers to easily implement
 * custom data loaders.
 */
def evidencePartition = new Partition(0);
//def insert = data.getInserter(name, evidencePartition);

///* Social Network A */
//insert.insert(1, "John Braker");
//insert.insert(2, "Mr. Jack Ressing");
//insert.insert(3, "Peter Larry Smith");
//insert.insert(4, "Tim Barosso");
//insert.insert(5, "Jessica Pannillo");
//insert.insert(6, "Peter Smithsonian");
//insert.insert(7, "Miranda Parker");

///* Social Network B */
//insert.insert(11, "Johny Braker");
//insert.insert(12, "Jack Ressing");
//insert.insert(13, "PL S.");
//insert.insert(14, "Tim Barosso");
//insert.insert(15, "J. Panelo");
//insert.insert(16, "Gustav Heinrich Gans");
//////insert.insert(17, "Otto v. Lautern");

/*
 * Of course, we can also load data directly from tab delimited data files.
 */
def dir = 'data'+java.io.File.separator;

//insert = data.getInserter(attr1, evidencePartition)
//InserterUtils.loadDelimitedData(insert, dir+"dec_attr1.txt");
//
//insert = data.getInserter(attr2, evidencePartition)
//InserterUtils.loadDelimitedData(insert, dir+"dec_attr2.txt");

for (Predicate p : [dec_attr1, dec_attr2, dec_dep])
{
	insert = data.getInserter(p, evidencePartition)
	InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+".txt");
}

/*
 * After having loaded the data, we are ready to run some inference and see what
 * kind of alignment our model produces. Note that for now, we are using the
 * predefined weights.
 * 
 * We first create a second partition and open it as the write partition of
 * a Database from the DataStore. We also include the evidence partition as a
 * read partition.
 * 
 * We close the predicates Name and Knows since we want to treat those atoms as
 * observed, and leave the predicate
 * SamePerson open to infer its atoms' values.
 */
def targetPartition = new Partition(1);
Database db = data.getDatabase(targetPartition, [dec_attr1, dec_attr2] as Set, evidencePartition);

/*
 * Before running inference, we have to add the target atoms to the database.
 * If inference (or learning) attempts to access an atom that is not in the database,
 * it will throw an exception.
 * 
 * The below code builds a set of all users, then uses a utility class
 * (DatabasePopulator) to create all possible SamePerson atoms between users of
 * each network.
 */
Set<GroundTerm> users = new HashSet<GroundTerm>();
Set<GroundTerm> dep_vals = new HashSet<GroundTerm>();
for (int i = 1; i <= 10; i++)
	users.add(data.getUniqueID(i));
//for (int i = 11; i <= 10; i++)
dep_vals.add(data.getUniqueID(0));
dep_vals.add(data.getUniqueID(1));

Map<Variable, Set<GroundTerm>> popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("User"), users)
popMap.put(new Variable("DepVal"), dep_vals)

DatabasePopulator dbPop = new DatabasePopulator(db);
dbPop.populate((dec_dep(User, DepVal)).getFormula(), popMap);
//dbPop.populate((dec_dep(UserB, UserA)).getFormula(), popMap);

/*
 * Now we can run inference
 * The LazyMPEInference rather that MPEInference was because I didn't add all permutations of ....
 */
MPEInference inferenceApp = new MPEInference(m, db, config);
//LazyMPEInference inferenceApp = new LazyMPEInference(m, db, config);
inferenceApp.mpeInference();
inferenceApp.close();

/*
 * Let's see the results
 */
println "Inference results with hand-defined weights:"
DecimalFormat formatter = new DecimalFormat("#.##");
for (GroundAtom atom : Queries.getAllAtoms(db, dec_dep))
	println atom.toString() + "\t" + formatter.format(atom.getValue());

/* 
 * Next, we want to learn the weights from data. For that, we need to have some
 * evidence data from which we can learn. In our example, that means we need to
 * specify the 'true' alignment, which we now load into another partition.
 */
Partition trueDataPartition = new Partition(2);
insert = data.getInserter(dec_dep, trueDataPartition)
/*
 *  .txt needs three columns!  It's like it wants a label in addition to the data it already has
 *  but the data is actually the label to begin with!
 */
InserterUtils.loadDelimitedDataTruth(insert, dir + "dec_dep_all.txt");

/* 
 * Now, we can learn the weights.
 * 
 * We first open a database which contains all the target atoms as observations.
 * We then combine this database with the original database to learn.
 */
Database trueDataDB = data.getDatabase(trueDataPartition, [dec_dep] as Set);
MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(m, db, trueDataDB, config);
weightLearning.learn();
weightLearning.close();

/*
 * Let's have a look at the newly learned weights.
 */
println ""
println "Learned model:"
println m

/*
 * Now, we apply the learned model to a different social network alignment data set.
 * We load the data set as before (into new partitions) and run inference.
 * Finally, we print the results.
 */

/*
 * Loads evidence
 */
Partition evidencePartition2 = new Partition(3);

insert = data.getInserter(dec_attr1, evidencePartition2)
InserterUtils.loadDelimitedData(insert, dir+"dec_attr1_2.txt");

insert = data.getInserter(dec_attr2, evidencePartition2);
InserterUtils.loadDelimitedData(insert, dir+"dec_attr2_2.txt");

insert = data.getInserter(dec_dep, evidencePartition2);
InserterUtils.loadDelimitedData(insert, dir+"dec_dep_2.txt");

/*
 * Populates targets
 */
def targetPartition2 = new Partition(4);
Database db2 = data.getDatabase(targetPartition2, [dec_attr1, dec_attr2] as Set, evidencePartition2);

users.clear();
for (int i = 11; i <= 20; i++)
	users.add(data.getUniqueID(i))

dep_vals.clear();
dep_vals.add(data.getUniqueID(0));
dep_vals.add(data.getUniqueID(1));

dbPop = new DatabasePopulator(db2);
popMap = new HashMap<Variable, Set<GroundTerm>>();
popMap.put(new Variable("User"), users)
popMap.put(new Variable("DepVal"), dep_vals)
dbPop.populate((dec_dep(User, DepVal)).getFormula(), popMap);


/*
 * Performs inference
 */
inferenceApp = new MPEInference(m, db2, config);
result = inferenceApp.mpeInference();
inferenceApp.close();

println "Inference results on second social network with learned weights:"
for (GroundAtom atom : Queries.getAllAtoms(db2, dec_dep))
	println atom.toString() + "\t" + formatter.format(atom.getValue()) + "\t" + formatter.format(atom.getConfidenceValue());

//result.
	
/*
 * We close the Databases to flush writes
 */
db.close();
trueDataDB.close();
db2.close();

///**
// * This class implements the ExternalFunction interface so that it can be used
// * as an attribute similarity function within PSL.
// *
// * This simple implementation checks whether two strings are identical, in which case it returns 1.0
// * or different (returning 0.0).
// *
// * The package edu.umd.cs.psl.ui.functions.textsimilarity contains additional and
// * more sophisticated string similarity functions.
// */
//class MyStringSimilarity implements ExternalFunction {
//	
//	@Override
//	public int getArity() {
//		return 2;
//	}
//
//	@Override
//	public ArgumentType[] getArgumentTypes() {
//		return [ArgumentType.String, ArgumentType.String].toArray();
//	}
//	
//	@Override
//	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
//		return args[0].toString().equals(args[1].toString()) ? 1.0 : 0.0;
//	}
//	
//}

println 'done.'