package dec

import java.text.DecimalFormat

import util.Score
import edu.umd.cs.psl.application.inference.MPEInference
import edu.umd.cs.psl.application.learning.weight.em.HardEM
import edu.umd.cs.psl.application.learning.weight.em.LatentObjectiveComputer
import edu.umd.cs.psl.application.learning.weight.em.PairedDualLearner
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE
import edu.umd.cs.psl.config.*
import edu.umd.cs.psl.database.DataStore
import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.database.DatabasePopulator
import edu.umd.cs.psl.database.Partition
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.argument.ArgumentType
import edu.umd.cs.psl.model.argument.GroundTerm
import edu.umd.cs.psl.model.argument.Variable
import edu.umd.cs.psl.model.predicate.Predicate
import edu.umd.cs.psl.ui.functions.textsimilarity.*
import edu.umd.cs.psl.ui.loading.InserterUtils
import edu.umd.cs.psl.util.database.Queries

dep_variable = 'gt_std_decline'
//dep_variable = 'gt_2std_decline'
//dep_variable = 'gt_median_decline'

k = 10

def score_validation = new Score('single_stage_validation')

k.times{
	
	def dir = "data/folds/fold_${it}/"

	/*
	 * Build an latent variable using expectation maximization and tie it to 
	 * the dependent variable.  One model.
	 */
	
	/*
	 * A ConfigBundle is a set of key-value pairs containing configuration options.
	 * One place these can be defined is in psl-example/src/main/resources/psl.properties
	 */
	ConfigManager cm = ConfigManager.getManager()
	ConfigBundle config = cm.getBundle("decline")
	
	/* Uses H2 as a DataStore and stores it in a temp. directory by default */
	def defaultPath = System.getProperty("java.io.tmpdir")
	String dbpath = config.getString("dbpath", defaultPath + File.separator + "decline")
	DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)
	
	// Instantiate a model
	PSLModel m = new PSLModel(this, data)
	
	
	// Base features predicates
	addPreds = new AddPredicates(m, this);
	m = addPreds.addPredicates();
	
	
	// Dependent Variable
	m.add predicate: "decline",		types: [ArgumentType.UniqueID]
	m.add predicate: "not_decline",		types: [ArgumentType.UniqueID]
	
	// Is a student
	m.add predicate: "student",        types: [ArgumentType.UniqueID]
	
	// Latent Variable
	m.add predicate: "engagement",        types: [ArgumentType.UniqueID]
	
	// Logic Rules
	
	// Prior
	m.add rule : student(A) >> ~decline(A), weight : 40
	m.add rule : ( student(A) & prior_purdue_gpa(A) ) >> decline(A) , weight : 10
	
	// COLO
	m.add rule : ( student(A) & degree_centrality_colo_201620_bh_q0_minus_colo_201610_bh_q0(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & degree_centrality_colo_201620_nbh_q0_minus_colo_201610_nbh_q0(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & farness_centrality_colo_201620_bh_q0_minus_colo_201610_bh_q0(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & farness_centrality_colo_201620_nbh_q0_minus_colo_201610_nbh_q0(A) ) >> decline(A) , weight : 10

	m.add rule : ( student(A) & ~attendance_4th_quartile(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bach25plus(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & avg_arrival_prompt_all_avg(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & enroll_college_e(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & hs_core_gpa(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & profile_classification_boap_01(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & ~highest_satr_math(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & ~aleks(A) ) >> decline(A) , weight : 10
	// Explored most of the door probas.  These seem to matter.
	m.add rule : ( student(A) & door_access_prob_hr_of_day_2(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & door_access_prob_hr_of_day_3(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & door_access_prob_hr_of_day_4(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & door_access_prob_hr_of_day_5(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & profile_highest_act_math(A) ) >> decline(A) , weight : 10


	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_min_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_max_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_avg_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_medianpso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_median_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & bytes_transferred_in_class_person_z_stdd_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_min_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_max_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_avg_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_medianpso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_median_lbp(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_lec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_lab(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_rec(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_sd(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_pso(A) ) >> decline(A) , weight : 10
	m.add rule : ( student(A) & num_transactions_in_class_person_z_stdd_lbp(A) ) >> decline(A) , weight : 10
	
	// Manually defined model
	println 'Manually defined model: \n' + m;
	
	def trainObservations = new Partition(0);
	
	//thePreds = new AddStockRules().getPreds(this)
	thePreds = addPreds.getPreds();
	for (Predicate p : thePreds)
	{
		insert = data.getInserter(p, trainObservations)
		InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_train.txt");
	}
	
	def trainPredictions = new Partition(1);
	Database db = data.getDatabase(trainPredictions, thePreds as Set, trainObservations);
	
	def person_uid_list = []   
	new File( dir + "student_train.txt" ).eachLine { line ->
		person_uid_list << line
	}
	
	Set<GroundTerm> people = new HashSet<GroundTerm>();
	for (id in person_uid_list)
		people.add(data.getUniqueID(id));
	
	dbPop = new DatabasePopulator(db);
	popMap = new HashMap<Variable, Set<GroundTerm>>();
	popMap.put(new Variable("People"), people);
	dbPop.populate((decline(People)).getFormula(), popMap);
	dbPop.populate((engagement(People)).getFormula(), popMap);
	
	Partition truth = new Partition(3);
	insert = data.getInserter(decline, truth)
	InserterUtils.loadDelimitedDataTruth(insert, dir + dep_variable + "_train.txt");
	
	Database truthDB = data.getDatabase(truth, [decline, not_decline] as Set);
	dbPop = new DatabasePopulator(truthDB);
	
	HardEM weightLearning = new HardEM(m, db, truthDB, config);
	weightLearning.learn();
	weightLearning.close();
	
	//LatentObjectiveComputer weightLearning = new LatentObjectiveComputer(m, db, truthDB, config);
	//weightLearning.learn();
	//weightLearning.close();
	
	//PairedDualLearner weightLearning = new PairedDualLearner(m, db, truthDB, config);
	//weightLearning.learn();
	//weightLearning.close();
	
	
	//PairedDualLearner
	
	//MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(m, db, truthDB, config);
	//weightLearning.learn();
	//weightLearning.close();
	
	println m;
	
	/*
	 * Performs inference
	 */
	MPEInference inferenceApp2 = new MPEInference(m, db, config);
	result = inferenceApp2.mpeInference();
	inferenceApp2.close();
	
	DecimalFormat formatter = new DecimalFormat("#.##");
	/*
	 * Let's see the results on training data
	 */
	
	def decliner_train_map = [:]
	new File( dir + dep_variable + "_train.txt" ).eachLine { line ->
		decliner_train_map[line.split('\t')[0]] = line.split('\t')[1]
	}
	
	/*
	 * Use learned model on validation data
	 */
	def validationObservations = new Partition(4);
	
	for (Predicate p : thePreds)
	{
		insert = data.getInserter(p, validationObservations)
		InserterUtils.loadDelimitedData(insert, dir+p.getName().toLowerCase()+"_validation.txt");
	}
	
	def validationPredictions = new Partition(5);
	Database dbValidation = data.getDatabase(validationPredictions, thePreds as Set, validationObservations);
	
	person_uid_list = []
	new File( dir + "student_validation.txt" ).eachLine { line ->
		person_uid_list << line
	}
	
	people = new HashSet<GroundTerm>();
	for (id in person_uid_list)
		people.add(data.getUniqueID(id));
	
	dbPop = new DatabasePopulator(dbValidation);
	popMap = new HashMap<Variable, Set<GroundTerm>>();
	popMap.put(new Variable("People"), people);
	dbPop.populate((decline(People)).getFormula(), popMap);
	dbPop.populate((not_decline(People)).getFormula(), popMap);
	dbPop.populate((engagement(People)).getFormula(), popMap);
	
	/*
	 * Performs inference
	 */
	MPEInference inferenceApp3 = new MPEInference(m, dbValidation, config);
	result = inferenceApp3.mpeInference();
	inferenceApp3.close();
	
	/*
	 * Let's see the results on validation data
	 */
	
	def decliner_validation_map = [:]
	new File( dir + dep_variable + "_validation.txt" ).eachLine { line ->
		decliner_validation_map[line.split('\t')[0]] = line.split('\t')[1]
	}
	
	score_validation.addPredictions(dbValidation
									, decliner_validation_map
									, Queries.getAllAtoms(dbValidation, decline)
									, m)
}

score_validation.score()
println 'done.'

