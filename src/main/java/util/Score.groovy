package util

import java.text.DecimalFormat
import java.text.SimpleDateFormat

import edu.umd.cs.psl.database.Database
import edu.umd.cs.psl.groovy.PSLModel
import edu.umd.cs.psl.model.atom.GroundAtom

class Score {

	def db = null
	def truthMap = null
	def ground_atom = null
	def ts = null
	def name = null
	def output_file_train = null
	def m = null
	def predictions = null
	def scores_f = null
	def scores = []
	def formatter = new DecimalFormat("#.##");
	def tp = 0.0
	def tn = 0.0
	def fp = 0.0
	def fn = 0.0
	def scoring_error = 0.0
	def actual = -1
	def prediction = -1
	def maxFscore = 0
	
	Score(String name){
		Date now = new Date()
		SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.ts = timestamp.format(now).replaceAll(' ', '_').replaceAll(':', '-').toString();
		this.name = name + ts
		this.output_file_train = 'output' + java.io.File.separator + this.name + '.csv';
		def fileSuccessfullyDeleted =  new File(output_file_train).delete()
		this.predictions = new File(output_file_train)
		this.predictions.append("person_uid,prediction,ground\r\n")
	}

	
	def addPredictions(Database db, Map truthMap, HashSet ground_atom, PSLModel m){
		
		this.db = db
		this.truthMap = truthMap
		this.ground_atom = ground_atom
		this.name = name
		this.m = m
		
		for (GroundAtom atom : ground_atom)
		{
			actual = truthMap.get(atom.getArguments()[0].toString()).toString().toInteger();
			prediction = atom.getValue()
			this.predictions.append(atom.getArguments()[0].toString() + "," + formatter.format(atom.getValue()) + "," + actual + "\r\n");
			this.scores << [atom.getArguments()[0].toString(), formatter.format(atom.getValue()), actual]
		}
		
	}
	
	def score(){
		def output_file_scores = 'output' + java.io.File.separator + this.name + '_scores.csv';
		def fileSuccessfullyDeleted2 =  new File(output_file_scores).delete()
		this.scores_f = new File(output_file_scores)
		this.scores_f.append('threshold,tp,tn,fp,fn,precision,false_pos_rate,recall,fscore\r\n')
		
		// Find unique values for result probabilities
		def uniqueProbas = new HashSet<Double>();
		uniqueProbas.add(0) //baseline
		for (l in this.scores)
		{
			uniqueProbas.add(Float.parseFloat(l[1]))
		}
		
		def uniqueProbasList = uniqueProbas.toArray().sort()
		
		for ( proba in uniqueProbasList ){
			tp = 0.0
			tn = 0.0
			fp = 0.0
			fn = 0.0
			scoring_error = 0.0
			actual = -1
			prediction = -1
			def i = 0
			for (l in this.scores)
			{
				try{
				prediction =  Float.parseFloat( l[1] )
				actual = l[2]
				if ( ( actual == 1 ) && ( prediction >= proba ) ) tp++
				else if ( ( actual == 0 ) && ( prediction < proba ) ) tn++
				else if ( ( actual == 0 ) && ( prediction >= proba ) ) fp++
				else if ( ( actual == 1 ) && ( prediction < proba ) ) fn++
				else scoring_error++
				i++;
				}catch(all){
					println 'stop.'
				}
			}
						

			def precision = tp / ( tp + fp + 0.00000001)
			def recall = tp / ( tp + fn + 0.00000001)
			def fscore = 2 * precision * recall / ( precision + recall + 0.00000001)
			def false_pos_rate = 1 - (tn / (tn + fp + 0.00000001))

			this.scores_f.append(proba + ',' + tp + ',' + tn + ',' + fp + ',' + fn + ',' + precision + ',' + false_pos_rate
				+ ',' + recall + ',' + fscore + "\r\n");
	
			if ( fscore > maxFscore ){
				maxFscore = fscore
			}
		}
		
		this.scores_f.append(m)
		makeGraphics()
		println 'predictions: ' + output_file_train;
		println 'scores: ' + output_file_scores;
		println 'max fscore: ' + maxFscore
		
	}
	
	def makeGraphics(){
		def proc = ("python /home/ken/workspace/eclipse/dec/makeROC_PRC.py " + this.name).execute();
		def outputStream = new StringBuffer();
		proc.waitForProcessOutput(outputStream, System.err);
		println(outputStream .toString());
	}
	
	def setDb(Database db){
		this.db = db
	}
	
}


