import pandas as pd
import numpy as np
import seaborn as sns
import math, os, shutil

# df = pd.read_csv('data/folds/colo_features.csv')
df = pd.read_csv('data/folds/train_validation_combo.csv')
df = df.sort_values(by='row_number')
df = df.reset_index()
del df['index']
del df['row_number']

print df.shape, df.columns

def fold_it(df, k):
    fold_size = math.ceil( float( df.shape[0] ) / k )
    r = [0] * k
    r = [x[0] * fold_size for x in enumerate(r)]
    r = [int(x) for x in r]
    r = [[r[x[0]],r[x[0]+1]] if x[0] < len(r) - 1 else [r[x[0]],df.shape[0] + 1] for x in enumerate(r)]
    return [df.iloc[x[0]:x[1],:] for x in r]
    
k = 10
dfs = fold_it(df, k)

for i, f in enumerate(dfs):
    directory = 'data/folds/fold_' + str(i) + '/'
#     if os.path.exists(directory):
#         shutil.rmtree(directory)
#     os.makedirs(directory)
    
    # Concatenate k-1 DFs into one training set
    train = [0] * len(dfs)
    train = [x[1] if x[0] != i else None for x in enumerate(dfs)]
    train = pd.concat(train)
    (train.person_uid).to_csv( directory + 'student' + '_train.txt', sep='\t', index=False, header=False )
    
    print train.shape
    for c in train:
        print str(i) + ') training... ' + c 
        if c not in ['person_uid', 'row_number', 'gt_median_decline', 'gt_std_decline', 'gt_2std_decline']:
            vals = train[c].unique()
            vals.sort()
            if np.array_equal(vals,np.array([0,1])):
                train[c + '_gtm'] = train[c].apply(lambda x: x >= train[c].mean())
            else:
                train[c + '_gtm'] = train[c].apply(lambda x: x >= train[c].median())
            d = train[train[c + '_gtm'] == True]
            (d.person_uid).to_csv( directory + c + '_train.txt', sep='\t', index=False, header=False )
            del train[c + '_gtm']
        if c in ['gt_median_decline', 'gt_std_decline', 'gt_2std_decline']:
            dep = train[['person_uid', c]]
            dep.to_csv( directory + c + '_train.txt', sep='\t', index=False, header=False )
          
    validation = dfs[i]
    (validation.person_uid).to_csv( directory + 'student' + '_validation.txt', sep='\t', index=False, header=False )
    print validation.shape
    for c in validation:
        print str(i) + ') validation... ' + c
        if c not in ['person_uid', 'row_number', 'gt_median_decline', 'gt_std_decline', 'gt_2std_decline']:
            vals = train[c].unique()
            vals.sort()
            if np.array_equal(vals,np.array([0,1])):
                validation[c + '_gtm'] = validation[c].apply(lambda x: x >= train[c].mean())
            else:
                validation[c + '_gtm'] = validation[c].apply(lambda x: x >= train[c].median())
            d = validation[validation[c + '_gtm'] == True]
            del validation[c + '_gtm']
            (d.person_uid).to_csv( directory + c + '_validation.txt', sep='\t', index=False, header=False )

        if c in ['gt_median_decline', 'gt_std_decline', 'gt_2std_decline']:
            dep = validation[['person_uid', c]]
            dep.to_csv( directory + c + '_validation.txt', sep='\t', index=False, header=False )
    

















# df.to_csv('data/decline/decline_train.txt', sep='\t', index=False, header=False)