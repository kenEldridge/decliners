# Process the colocation data, build feats. Join to /home/ken/workspace/eclipse/dec/data/folds/train_validation_combo.csv.
# Then run /home/ken/workspace/eclipse/dec/mk_folds.py

import pandas as pd
from snap import *
import snap as sp
from os import listdir
from os.path import isfile, join
from subprocess import call

class Raw_Data():
    
    data = {}
    
    def load_data(self):
        path = 'data/colo'
        files = [f for f in listdir(path) if isfile(join(path, f))]
        print files
        for f in files:
            key = f.split('.')[0]
            self.data[key] = sp.LoadEdgeList(sp.PUNGraph, path + '/' + f, 0, 1)


def build_degree_centrality(data):
    for key in data:
        degree_cent_df = pd.DataFrame(columns=('person_uid', 'degree_cent'))
        for index,NI in enumerate(data[key].Nodes()):
            DegCentr = sp.GetDegreeCentr(data[key], NI.GetId())
            degree_cent_df.loc[index] = [NI.GetId(), DegCentr]
        degree_cent_df.to_csv('data/colo/degree_centrality/' + key + '.csv', index=False)
        
def build_betweenness_centrality(data):
    for key in data:
        Nodes = sp.TIntFltH()
        Edges = sp.TIntPrFltH()
        sp.GetBetweennessCentr(data[key], Nodes, Edges, 1.0)
        
        betweenness_cent_df = pd.DataFrame(columns=('person_uid', 'betweenness_cent'))
        for index,node in enumerate(Nodes):
            betweenness_cent_df.loc[index] = [node, Nodes[node]]

        print 'writing /betweenness_centrality/', key
        betweenness_cent_df.to_csv('data/colo/betweenness_centrality/' + key + '.csv', index=False)     

def build_farness_centrality(data):
    for key in data:
        
        farness_cent_df = pd.DataFrame(columns=('person_uid', 'farness_cent'))
        for index, NI in enumerate(data[key].Nodes()):
            FarCentr = sp.GetFarnessCentr(data[key], NI.GetId())
            farness_cent_df.loc[index] = [NI.GetId(), FarCentr]

        print 'writing /farness_centrality/', key
        farness_cent_df.to_csv('data/colo/farness_centrality/' + key + '.csv', index=False)     
        
def build_pagerank(data):
    for key in data:
        
        PRankH = sp.TIntFltH()
        sp.GetPageRank(data[key], PRankH)
        
        pagerank_df = pd.DataFrame(columns=('person_uid', 'pagerank'))
        for index,item in enumerate(PRankH):
            pagerank_df.loc[index] = [item, PRankH[item]]

        print 'writing /pagerank/', key
        pagerank_df.to_csv('data/colo/pagerank/' + key + '.csv', index=False) 
    
def build_eccentricity(data):
    for key in data:
        
        eccentricity_df = pd.DataFrame(columns=('person_uid', 'eccentricity'))
        for index,NI in enumerate(data[key].Nodes()):
            eccentricity_df.loc[index] = [NI.GetId(), sp.GetNodeEcc(data[key], NI.GetId(), False)]

        print 'writing /eccentricity/', key
        eccentricity_df.to_csv('data/colo/eccentricity/' + key + '.csv', index=False) 
        
def build_eigenvector_centality(data):
    for key in data:
        
        NIdEigenH = sp.TIntFltH()
        sp.GetEigenVectorCentr(data[key], NIdEigenH)
        eigenvector_centrality_df = pd.DataFrame(columns=('person_uid', 'eigenvector_centrality'))
        for index,item in enumerate(NIdEigenH):
            eigenvector_centrality_df.loc[index] = [item, NIdEigenH[item]]
            
        print 'writing /eigenvector_centrality/', key
        eigenvector_centrality_df.to_csv('data/colo/eigenvector_centrality/' + key + '.csv', index=False) 
        
def build_triads(data):
    for key in data:
        
        triad_count_df = pd.DataFrame(columns=('person_uid', 'triad_count'))
        for index,NI in enumerate(data[key].Nodes()):
            triad_count_df.loc[index] = [NI.GetId(), sp.GetNodeTriads(data[key], NI.GetId())]
            
        print 'writing /triads/', key
        triad_count_df.to_csv('data/colo/triads/' + key + '.csv', index=False)       

def mk_diff(feat, f1, f2):

    # Pull both files
    path = 'data/colo/' + feat + '/'
    f1_df = pd.read_csv(path + f1 + '.csv')
    f2_df = pd.read_csv(path + f2 + '.csv')
    
    # Make difference f2 - f1
    r = pd.merge(f1_df, f2_df, on='person_uid')
    
    col_name = feat + '_' + f2 + '_minus_' + f1
    r[col_name] = r[r.columns[2]] - r[r.columns[1]]
    
    # Add to feature_files
    
    r[['person_uid', col_name]].to_csv('data/colo/feature_files/' + col_name + '.csv', index=False)
   
def clean_name_cp_to_stage(feat):

    path = 'data/colo/' + feat + '/'
    files = [f for f in listdir(path) if isfile(join(path, f))]
    for f in files:
        key = f.split('.')[0] 
        df = pd.read_csv(path + f)
        df.columns = ['person_uid', feat + '_' + f[0:len(f) - 4]]
        df.to_csv('data/colo/feature_files/' + feat + '_' + f[0:len(f) - 4], index=False)
    
def merge_features(master):
    
    m = pd.read_csv(master)[['person_uid', 'row_number']]
    
    path = 'data/colo/feature_files/'
    feat_files = [f for f in listdir(path) if isfile(join(path, f))]
    
    for f in feat_files:
        f_df = pd.read_csv(path + f)
        m = pd.merge(m, f_df, on='person_uid', how='left')
        
    m.to_csv('data/folds/colo_features.csv', index=False)
   
if __name__ == "__main__":
#     d = Raw_Data()
#     d.load_data()
    
    # done. shouldn't need to run unless you update the data
#     build_degree_centrality(d.data)
#     build_betweenness_centrality(d.data)
#     build_farness_centrality(d.data)
#     build_pagerank(d.data)
#     build_eccentricity(d.data)
#     build_eigenvector_centality(d.data)
#     build_triads(d.data)
    
    clean_name_cp_to_stage('degree_centrality')
    clean_name_cp_to_stage('betweenness_centrality')
    clean_name_cp_to_stage('eccentricity')
    clean_name_cp_to_stage('eigenvector_centrality')
    clean_name_cp_to_stage('farness_centrality')
    clean_name_cp_to_stage('pagerank')
    clean_name_cp_to_stage('triads')
    
    
    ##### Degree Centrality #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('degree_centrality', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('degree_centrality', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('degree_centrality', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('degree_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('degree_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('degree_centrality', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('degree_centrality', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('degree_centrality', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('degree_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('degree_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Farness Centrality #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('farness_centrality', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('farness_centrality', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('farness_centrality', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('farness_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('farness_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('farness_centrality', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('farness_centrality', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('farness_centrality', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('farness_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('farness_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Betweenness Centrality #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('betweenness_centrality', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('betweenness_centrality', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('betweenness_centrality', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('betweenness_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('betweenness_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('betweenness_centrality', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('betweenness_centrality', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('betweenness_centrality', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('betweenness_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('betweenness_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Eccentricity #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('eccentricity', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('eccentricity', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('eccentricity', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('eccentricity', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('eccentricity', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('eccentricity', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('eccentricity', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('eccentricity', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('eccentricity', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('eccentricity', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Eigenvector Centrality #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('eigenvector_centrality', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('eigenvector_centrality', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('eigenvector_centrality', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('eigenvector_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('eigenvector_centrality', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('eigenvector_centrality', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('eigenvector_centrality', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('eigenvector_centrality', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('eigenvector_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('eigenvector_centrality', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Pagerank #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('pagerank', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('pagerank', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('pagerank', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('pagerank', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('pagerank', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('pagerank', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('pagerank', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('pagerank', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('pagerank', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('pagerank', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')
      
  
    ##### Triads #####
    #############################
      
    ### business hours ###
    # q0:q0
    mk_diff('triads', 'colo_201610_bh_q0', 'colo_201620_bh_q0')
    # q1:q1
    mk_diff('triads', 'colo_201610_bh_q1', 'colo_201620_bh_q1')
    # q2:q2
    mk_diff('triads', 'colo_201610_bh_q2', 'colo_201620_bh_q2')
    # q3:q3
    mk_diff('triads', 'colo_201610_bh_q3', 'colo_201620_bh_q3')
    # q3:q0
    mk_diff('triads', 'colo_201610_bh_q3', 'colo_201620_bh_q0')
      
    ### non-business hours ###
    # q0:q0
    mk_diff('triads', 'colo_201610_nbh_q0', 'colo_201620_nbh_q0')
    # q1:q1
    mk_diff('triads', 'colo_201610_nbh_q1', 'colo_201620_nbh_q1')
    # q2:q2
    mk_diff('triads', 'colo_201610_nbh_q2', 'colo_201620_nbh_q2')
    # q3:q3
    mk_diff('triads', 'colo_201610_nbh_q3', 'colo_201620_nbh_q3')
    # q3:q0
    mk_diff('triads', 'colo_201610_nbh_q3', 'colo_201620_nbh_q0')

    merge_features('data/folds/train_validation_combo.csv')

    print 'done.'






