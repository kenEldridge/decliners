import pandas as pd
import numpy as np

df = pd.read_csv('data/decline/decline_train.csv')
df.to_csv('data/decline/decline_train.txt', sep='\t', index=False, header=False)

df = pd.read_csv('data/decline/decline_validation.csv')
df.to_csv('data/decline/decline_validation.txt', sep='\t', index=False, header=False)

df = pd.read_csv('data/decline/not_decline_train.csv')
df.to_csv('data/decline/not_decline_train.txt', sep='\t', index=False, header=False)

df = pd.read_csv('data/decline/not_decline_validation.csv')
df.to_csv('data/decline/not_decline_validation.txt', sep='\t', index=False, header=False)

# aleks = pd.read_csv('data/em_latent_test/aleks_train.csv')
# aleks['aleks_bin'] = [0 if np.isnan(x) else x for x in aleks['aleks_bin']]
# aleks['aleks_bin'] = aleks['aleks_bin'].astype(int)
# aleks.to_csv('data/em_latent_test/aleks_train.txt', sep='\t', index=False, header=False)

# person_uids = prior_term_gpa['person_uid']
# person_uids.to_csv('data/em_latent_test/person_ids.txt', sep='\t', index=False, header=False)

# colo = pd.read_csv('data/raw/friend_latent_small/colo_201610.csv')
# colo.to_csv('data/raw/friend_latent_small/colo.txt', sep='\t', index=False, header=False)
# 
# coswipe_test = pd.read_csv('data/raw/friend_latent_small/coswipe_201610.csv')
# coswipe_test.to_csv('data/raw/friend_latent_small/coswipe.txt', sep='\t', index=False, header=False)

# colo_test = pd.read_csv('data/raw/friend_latent_small/colo_test.csv')
# colo_test.to_csv('data/raw/friend_latent_small/colo_test.txt', sep='\t', index=False, header=False)
# 
# colo_train = pd.read_csv('data/raw/friend_latent_small/colo_train.csv')
# colo_train.to_csv('data/raw/friend_latent_small/colo_train.txt', sep='\t', index=False, header=False)
# 
# coswipe_train = pd.read_csv('data/raw/friend_latent_small/coswipe_train.csv')
# coswipe_train.to_csv('data/raw/friend_latent_small/coswipe_train.txt', sep='\t', index=False, header=False)
# 
# coswipe = pd.read_csv('data/raw/friend_latent_small/stu_profile.csv')
# coswipe = coswipe[['person_uid', 'profile_residence']]
# coswipe.to_csv('data/raw/friend_latent_small/residence.txt', sep='\t', index=False, header=False)
# 
# friend_truth = pd.read_csv('data/raw/friend_latent_small/friend_truth_from_coswipe.csv')
# friend_truth['truth'] = 1
# friend_truth.to_csv('data/raw/friend_latent_small/friend_truth_from_coswipe.txt', sep='\t', index=False, header=False)
# 
# person_uid_train = pd.read_csv('data/raw/friend_latent_small/person_uid_train.csv')
# person_uid_train.to_csv('data/raw/friend_latent_small/person_uid_train.txt', sep='\t', index=False, header=False)

print 'done.'