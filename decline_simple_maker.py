import pandas as pd
import numpy as np

df = pd.read_csv('data/decline/decline_train.csv')
df = df[df.gt_std_decline == 1]
(df.person_uid).to_csv('data/decline/decliner_only_train.txt', sep='\t', index=False, header=False)

df = pd.read_csv('data/decline/decline_validation.csv')
df = df[df.gt_std_decline == 1]
(df.person_uid).to_csv('data/decline/decliner_only_validation.txt', sep='\t', index=False, header=False)
